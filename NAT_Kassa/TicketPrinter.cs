﻿using NAT_Kassa.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.epson.pos.driver;
using System.Drawing.Printing;
using System.Windows;
using System.Drawing;
using System.IO;
using System.Windows.Shapes;
using NAT_Kassa.Properties;
using System.Net.Http.Headers;
using System.ComponentModel.Design;

namespace NAT_Kassa
{
    public class TicketPrinter
    {
        #region Fields
        //State Vars
        private bool _isAvailable;
        private bool _isFinished;
        private bool _isCanceled;
        private bool _isPrinting;
        private bool _isErrorActive;
        private bool _onHold;
        private bool _wasErrorActive;
        private ASB _asbErrorCode;
 
        //Printable Objects
        private Transaction _transaction;
        private string _text;
        private InventoryTransaction _inventoryTransaction;
        private List<InventoryProduct> _inventoryProducts;
        //System Print Classes
        private PrintDocument _receiptDoc;
        private PrintDocument _textDoc;
        private PrintDocument _orderDoc;
        //System Drawing Classes
        private Font _regFont = new Font(FontFamily.GenericMonospace, 9, System.Drawing.FontStyle.Bold);
        private Font _empFont = new Font(FontFamily.GenericMonospace, 9, System.Drawing.FontStyle.Bold);

        //Printer Status
        private StatusAPI _printerStatus;
        //private const string PRINTER_NAME = "Microsoft Print to PDF";
        private const string PRINTER_NAME = "EPSON TM-T20III Receipt";
        #endregion
        #region Constructor
        public TicketPrinter()
        {
            _printerStatus = new StatusAPI();
            _receiptDoc = new PrintDocument();
            _orderDoc = new PrintDocument();
            _textDoc = new PrintDocument();
            _receiptDoc.PrintController = new StandardPrintController();
            _orderDoc.PrintController = new StandardPrintController();
            _textDoc.PrintController = new StandardPrintController();
            _receiptDoc.PrintPage += Receipt_PrintPage;
            _orderDoc.PrintPage += Order_PrintPage;
            _textDoc.PrintPage += Text_PrintPage;
            _receiptDoc.PrinterSettings.PrinterName = PRINTER_NAME;
            _orderDoc.PrinterSettings.PrinterName = PRINTER_NAME;
            _textDoc.PrinterSettings.PrinterName = PRINTER_NAME;
            try
            {
                if (_textDoc.PrinterSettings.IsValid && _printerStatus.OpenMonPrinter(OpenType.TYPE_PRINTER, PRINTER_NAME) == ErrorCode.SUCCESS)
                {
                    _printerStatus.StatusCallback += PrinterStatusChanged;
                    _printerStatus.SetStatusBack();
                   // if (_printerStatus.Status.HasFlag(ASB.ASB_OFF_LINE | ASB.ASB_NO_RESPONSE)) { _isAvailable = false; AvailabilityChanged(EventArgs.Empty); }
                   // else { _isAvailable = true; AvailabilityChanged(EventArgs.Empty); }
                }
                else
                {
                    MessageBox.Show("Failed to open printer status monitor. Check if printer is connected and turned on. Also check if printer name = " + PRINTER_NAME, "Printer Error");
                    _isAvailable = false;
                    AvailabilityChanged(EventArgs.Empty);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Exception");
                _isAvailable = false;
                AvailabilityChanged(EventArgs.Empty);
            }
        }
        #endregion
        #region Properties
        public bool IsAvailable { get => _isAvailable; }
        public bool IsFinished { get => _isFinished; }
        public bool IsCanceled { get => _isCanceled; }
        public bool IsPrinting { get => _isPrinting; }
        public bool IsErrorActive { get => _isErrorActive; }
        public ASB ErrorCodeASB { get => _asbErrorCode; }
        #endregion
        #region EventHandeling

        //Handles the notifications of the printer state changed
        private void PrinterStatusChanged(ASB asb)
        {
            Console.WriteLine(asb);

            // Notify printing completion.
            if (asb.HasFlag(ASB.ASB_PRINT_SUCCESS))
            {
                _isAvailable = true;
                _isFinished = true;
                _isCanceled = false;
                _isPrinting = false;
                _isErrorActive = false;
                _asbErrorCode = asb;
                Finished(EventArgs.Empty);
            }
            else
            {
                // Notify any errors that occur.
                // requires mainten
                if (asb.HasFlag(ASB.ASB_AUTOCUTTER_ERR) | asb.HasFlag(ASB.ASB_MECHANICAL_ERR))
                {
                    _onHold = _isPrinting;
                    _isAvailable = false;
                    _isFinished = false;
                    _isCanceled = true;
                    _isPrinting = false;
                    _isErrorActive = true;
                    _asbErrorCode = asb;
                    UnrecoverError(EventArgs.Empty);
                    AvailabilityChanged(EventArgs.Empty);
                    //clear printer queue by resetting
                }
                // printer will auto recover from these errors and reprint active file and others in queue
                else if (asb.HasFlag(ASB.ASB_COVER_OPEN) | asb.HasFlag(ASB.ASB_PAPER_JAM) | asb.HasFlag(ASB.ASB_PAPER_END) |asb.HasFlag(ASB.ASB_RECEIPT_END)| asb.HasFlag(ASB.ASB_HEAD_TEMPERATURE_ERR))
                {
                    _onHold = _isPrinting;
                    _isAvailable = false;
                    _isFinished = false;
                    _isCanceled = false;
                    _isPrinting = false;
                    _isErrorActive = true;
                    _asbErrorCode = asb;
                    RecoverError(EventArgs.Empty);
                    AvailabilityChanged(EventArgs.Empty);
                }
                // warning
                else if (asb.HasFlag(ASB.ASB_RECEIPT_NEAR_END) | asb.HasFlag(ASB.ASB_RECEIPT_NEAR_END_FIRST) | asb.HasFlag(ASB.ASB_RECEIPT_NEAR_END_SECOND) |asb.HasFlag(ASB.ASB_BLACKMARK_ERR))
                {
                    _isAvailable = true;
                    _isErrorActive = false;
                    _asbErrorCode = asb;
                    Warning(EventArgs.Empty);
                }
                // no communication
                else if (asb.HasFlag(ASB.ASB_NO_RESPONSE) | asb.HasFlag(ASB.ASB_OFF_LINE))
                {
                    _onHold = _isPrinting;
                    _isAvailable = false;
                    _isFinished = false;
                    _isCanceled = false;
                    _isPrinting = false;
                    _isErrorActive = false;
                    _asbErrorCode = asb;
                    AvailabilityChanged(EventArgs.Empty);
                }
                // normal operation
                else
                {
                    _wasErrorActive = _isErrorActive;
                    _isAvailable = true;
                    _isFinished = false;
                    _isCanceled = false;
                    _isPrinting = _onHold;
                    _isErrorActive = false;
                    _asbErrorCode = asb;
                    if (_wasErrorActive) { _wasErrorActive = false; ClearedError(EventArgs.Empty); }
                    AvailabilityChanged(EventArgs.Empty);
                }
            }
        }

        private void Text_PrintPage(object sender, PrintPageEventArgs e)
        {
            SizeF textsize = e.Graphics.MeasureString(_text, _regFont, e.PageBounds.Right);
            e.Graphics.DrawString(_text, _regFont, Brushes.Black, new RectangleF(new PointF(0, 0), textsize));
        }

        private void Order_PrintPage(object sender, PrintPageEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Receipt_PrintPage(object sender, PrintPageEventArgs e)
        {
            //prepare colomns for layout
            //0 = left margin
            //1 = product start
            //2 = count start
            //3 = unit price start
            //4 = line price start
            //5 = line price end
            //6 = total header start
            //7 = total header end
            float rightedge = e.PageBounds.Right - 35;
            float leftedge = e.PageBounds.Left;
            PointF[] cols = { new PointF(leftedge, 0), new PointF(5, 0), new PointF(200, 0), new PointF(180, 0), new PointF(200, 0), new PointF(rightedge, 0), new PointF(160, 0), new PointF(rightedge - 60, 0), new PointF(160, 0), new PointF(290, 0) };
            float lineOffset = _regFont.GetHeight(e.Graphics);
            float lineOffsetEmp = _empFont.GetHeight(e.Graphics);

            //print logo's
            Bitmap logo = Resources.Nautic_Logo;
            e.Graphics.DrawImage(logo, cols[0].X,cols[0].Y, rightedge / 3, rightedge / 3);
            logo = Resources.Flac_Logo;
            e.Graphics.DrawImage(logo, cols[0].X + rightedge / 3, cols[0].Y + rightedge / 6, rightedge / 3, rightedge / 6);
            logo = Resources.Tribes_Logo_kleur;
            e.Graphics.DrawImage(logo, cols[0].X + 2 * rightedge / 3, cols[0].Y, rightedge / 3, rightedge / 3);
            NewLine(cols, rightedge / 3 + 5);

            //print ticket header
            e.Graphics.DrawString("Transactie #" + _transaction.Id.ToString(), _regFont, Brushes.Black, cols[0]);
            NewLine(cols, lineOffset);
            e.Graphics.DrawString("Bediend door " + _transaction.Openedby.Username, _regFont, Brushes.Black, cols[0]);
            NewLine(cols, lineOffset);
            //e.Graphics.DrawString(_transaction.OpenedTimestamp.ToString(), _regFont, Brushes.Black, cols[0]);
            e.Graphics.DrawString(DateTime.Now.ToString(), _regFont, Brushes.Black, cols[0]);
            NewLine(cols, lineOffset * 2);
            //print product header
            e.Graphics.DrawString("Producten", _regFont, Brushes.Black, cols[1]);
            float rightAlign = cols[2].X - e.Graphics.MeasureString("Aantal", _regFont).Width;
            e.Graphics.DrawString("Aantal", _regFont, Brushes.Black,rightAlign ,cols[2].Y);
            rightAlign = cols[5].X - e.Graphics.MeasureString("Prijs(€)", _regFont).Width;
            e.Graphics.DrawString("Prijs(€)", _regFont, Brushes.Black, rightAlign + 2,cols[5].Y);
            NewLine(cols, lineOffset + 2);
            e.Graphics.DrawLine(Pens.Black, cols[0], cols[9]);
            NewLine(cols, 2);
            //print product lines
            foreach (Product p in _transaction.Products)
            {
                e.Graphics.DrawString(p.Name, _regFont, Brushes.Black, cols[1]);
                rightAlign = cols[2].X - e.Graphics.MeasureString("x" + p.Quantity.ToString(), _regFont).Width;
                e.Graphics.DrawString("x" + p.Quantity.ToString(), _regFont, Brushes.Black, rightAlign, cols[2].Y);
                //e.Graphics.DrawString(p.Price.ToString(), _regFont, Brushes.Black, cols[3]);
                string lineamount = (p.Price * p.Quantity).ToString("F1");
                rightAlign = cols[5].X - e.Graphics.MeasureString(lineamount, _regFont).Width;
                e.Graphics.DrawString(lineamount, _regFont, Brushes.Black, rightAlign, cols[5].Y);
                NewLine(cols, lineOffset);
            }
            //print total header
            e.Graphics.DrawLine(Pens.Black, cols[0], cols[9]);
            NewLine(cols, 2);
            e.Graphics.DrawLine(Pens.Black, cols[0], cols[9]);
            NewLine(cols, 2);
            //print total
            rightAlign = cols[2].X - e.Graphics.MeasureString("Totaal", _empFont).Width; 
            e.Graphics.DrawString("Totaal", _empFont, Brushes.Black, rightAlign,cols[2].Y);
            string total = "€" + _transaction.Amount.ToString("F1");
            rightAlign = cols[5].X - e.Graphics.MeasureString(total,_empFont).Width;
            e.Graphics.DrawString(total, _empFont, Brushes.Black, rightAlign, cols[5].Y);
            NewLine(cols, lineOffset * 2);
            //print payments
            foreach (Payment p in _transaction.Payments)
            {
                e.Graphics.DrawString(p.ToString(), _regFont, Brushes.Black, cols[1]);
                NewLine(cols, lineOffset);
            }
            rightAlign = cols[2].X - e.Graphics.MeasureString("Betaal", _empFont).Width;
            e.Graphics.DrawString("Betaald", _empFont, Brushes.Black, rightAlign, cols[2].Y);
            total = "€" + _transaction.AmountPaid.ToString("F1");
            rightAlign = cols[5].X - e.Graphics.MeasureString(total, _empFont).Width;
            e.Graphics.DrawString(total, _empFont, Brushes.Black, rightAlign, cols[5].Y);
            NewLine(cols, lineOffset * 2);
            // print footer
            e.Graphics.DrawString("***************************************************************", _regFont,Brushes.Black, cols[0]);
           
        }
        #endregion
        #region Methods
        /// <summary>
        /// Prints a custom text.
        /// </summary>
        /// <param name="text">string to print</param>
        public void Print(string text)
        {
            _text = text;
            _isPrinting = true;
            _textDoc.Print();
        }
        /// <summary>
        /// Prints a receipt of the transaction.
        /// </summary>
        /// <param name="t">Transaction to print</param>
        public void Print(Transaction transaction)
        {
            _transaction = transaction;
            _isPrinting = true;
            _receiptDoc.Print(); 
        }
        /// <summary>
        /// Prints a receipt of an inventory transaction.
        /// </summary>
        /// <param name="t"> inventory transcation to print</param>
        /// <param name="products"> products in the inventory transaction</param>
        public void Print(InventoryTransaction transaction,List<InventoryProduct> products)
        {
            _inventoryProducts = products;
            _inventoryTransaction = transaction;
            _isPrinting = true;
            _orderDoc.Print();
        }
        /// <summary>
        /// Call on closing of the application
        /// </summary>
        public void Shutdown()
        {
            
            if (_printerStatus.CancelStatusBack() == ErrorCode.SUCCESS && _printerStatus.CloseMonPrinter() == ErrorCode.SUCCESS)
            {
                _orderDoc.Dispose();
                _textDoc.Dispose();
                _receiptDoc.Dispose();
            }
            else
            {
                MessageBox.Show("Something went wrong, failed to close printer status monitor. Unable to dispose of resources.", "Shutdown Error");
            }
        }
        #endregion
        #region Events
        /// <summary>
        /// Event that fires when printer's availibilty has changed. Get IsAvailable to check state.
        /// </summary>
        public event EventHandler OnAvailabilityChanged;
        protected virtual void AvailabilityChanged(EventArgs e)
        {
            EventHandler handler = OnAvailabilityChanged;
            handler?.Invoke(this, e);
        }
        /// <summary>
        /// Event fires on a recoverable error before availability changes. When error is cleared the printer will reprint the last document. Get ErrorCode for more info on error that occured.
        /// </summary>
        public event EventHandler OnRecoverError;
        protected virtual void RecoverError(EventArgs e)
        {
            EventHandler handler = OnRecoverError;
            handler?.Invoke(this, e);
        }
        /// <summary>
        /// Event fires on a warning eg paper low or bad quality. Printer continues printing. Get ErrorCode for more info on warning.
        /// </summary>
        public event EventHandler OnWarning;
        protected virtual void Warning(EventArgs e)
        {
            EventHandler handler = OnWarning;
            handler?.Invoke(this, e);
        }
        /// <summary>
        /// Event fires on a unrecoverable error before availability changes. All pending documents are aborted. Maintenance is required! Get ErrorCode for more info on error that occured.
        /// </summary>
        public event EventHandler OnUnrecoverError;
        protected virtual void UnrecoverError(EventArgs e)
        {
            EventHandler handler = OnUnrecoverError;
            handler?.Invoke(this, e);
        }
        /// <summary>
        /// Event fires when state has changed and no errors are active. Idle or Printing state. Get Printing to check if printer is busy.
        /// </summary>
        public event EventHandler OnClearedError;
        protected virtual void ClearedError(EventArgs e)
        {
            EventHandler handler = OnClearedError;
            handler?.Invoke(this, e);
        }
        /// <summary>
        /// Fires when a document is printed. Get IsPrinting to check printing state.
        /// </summary>
        public event EventHandler OnFinished;
        protected virtual void Finished(EventArgs e)
        {
            EventHandler handler = OnFinished;
            handler?.Invoke(this, e);
        }


        #endregion
        #region Helpers
        //Set all columns to a new line for printing
        private void NewLine(PointF[] columns, float offset)
        {
            for (int i = 0; i < columns.Length; i++)
            {
                columns[i].Y += offset;
            }
        }
        #endregion
    }
}
