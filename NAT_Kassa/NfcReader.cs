﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PCSC;
using PCSC.Monitoring;
using PCSC.Iso7816;
namespace NAT_Kassa
{
    public class NfcReader
    {
        #region Fields
        //State Vars
        private bool _isAvailable;
        private bool _isErrorActive;
        private bool _isCardConnected;
        private ReaderStatus _state;
        private string _error = "";
        //Data adapter for logging
        DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        System.Timers.Timer _bgworker = new System.Timers.Timer(2000);
        //PCSC Device = NFC Reader
        private const string READER_TYPE_NAME = "ACR122";
        private IDeviceMonitorFactory _deviceMonitorFactory = DeviceMonitorFactory.Instance;
        private SCardMonitor _cardMonitor;
        private IsoReader _isoReader;
        private ICardReader _cardReader;
        private IContextFactory _contextFacory;
        private ISCardContext _cardContext;
        private string _readerName = "";
        //SmartCard Data
        private Card _connectedCard;
        //Application Constants, use for Mifare protocol
        private const byte BLOCK_NAME = 0x08;
        private const byte BLOCK_SURNAME = 0x09;
        private const byte SURNAME_LENGHT = 0x02;
        private const byte BLOCK_NAME_TRAILER = 0x0B;
        private const byte BLOCK_CREDIT = 0x0C;
        private const byte BLOCK_HASH = 0x0D;
        private const byte HASH_LENGHT = 0x02;
        private const byte BLOCK_CREDIT_TRAILER = 0x0F;
        private byte[] KEY_A = { 0xAB,0xD1,0x26,0x4C,0x98,0xAA};
        private byte[] KEY_B = { 0xDD, 0xAC, 0x58, 0x22, 0xF1, 0xBB};
        private byte[] KEY_FACTORY = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
        // Block read -> A en B, Block write -> B, Trailer acces -> A and A always secret (transport config)
        private byte[] ACCES_TYPE_B_WRITE = { 0xF8, 0x77, 0x80, 0xFF };
        // Default configuration (all = transport config) Block read/write -> A en B, Trailer Acces A en A hidden
        private byte[] ACCES_TYPE_STD = { 0xFF, 0x07, 0x80, 0xFF };
        private const string HASH_PREFIX = "ffdWsjkh9SoYm5PsGb";
        private bool _useValidation = true;
        #endregion

        #region Constructor
        public NfcReader(bool UseValidation)
        {
            _useValidation = UseValidation;
            _bgworker.Elapsed += Check_ConnectedReaders;
            ConnectReader();
        }
        private void Check_ConnectedReaders(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Console.WriteLine("Check ReaderFactory" + e.SignalTime.ToString() + sender.ToString());
            ConnectReader();
        }
        #endregion

        #region Properties
        public bool IsAvailable { get => _isAvailable; }
        public bool IsErrorActive { get => _isErrorActive; }
        public bool IsCardConnected { get => _isCardConnected; }
        public Card ConnectedCard { get => _connectedCard; }
        public string Error { get => _error; }
        public bool UseValidation { get => _useValidation; set => _useValidation = value; }
        #endregion

        #region EventHandeling
        private void MonitorException(object sender, PCSC.Exceptions.PCSCException exception)
        {
            _isAvailable = false;
            _error = "Lost Connection";
            _isErrorActive = true;
            if (!_bgworker.Enabled) 
            {
                System.Windows.MessageBox.Show(exception.ToString(), "Lost connection to reader. Check USB"); 
                _bgworker.Start(); 
            }
        }
        private void MonitorInit(object sender, CardStatusEventArgs e)
        {
            _connectedCard = GetCard();
            _isCardConnected = (_connectedCard != null);
        }
        private void CardInsert(object sender, CardStatusEventArgs e)
        {
            _connectedCard = GetCard();
            _isCardConnected = (_connectedCard != null);
            AvailabilityChanged(EventArgs.Empty);
        }
        private void CardRemoved(object sender, CardStatusEventArgs e)
        {
            _isCardConnected = false;
            _connectedCard = null;
            _error = "";
            _isErrorActive = false;
            AvailabilityChanged(EventArgs.Empty);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Call on closing of the application
        /// </summary>
        public void Shutdown()
        {
            if (_isoReader != null) { _isoReader.Dispose(); }
            if (_cardReader != null) { _cardReader.Dispose(); }
            _isCardConnected = false;
            _connectedCard = null;
            _error = "";
            _isErrorActive = false;
            _isAvailable = false;
            if (_cardMonitor != null) { _cardMonitor.Dispose(); }
            _bgworker.Dispose();
        }
        /// <summary>
        /// Formats a Mifare card to enable use with this application. Formatting only done when the used sectors are free. This is checked by using factory authentication.
        /// </summary>
        /// <param name="name">Given name (voornaam) of the owner</param>
        /// <param name="surname">Name (achternaam) of the owner </param>
        /// <returns>IsSuccessful</returns>
        public bool InitCard(string name, string surname)
        {
            if (!_isAvailable) { _error = "No reader available"; return false; }
            if (_connectedCard == null) { _error = "No card connected to initialize"; return false; }
            if (_connectedCard.IsValid) { _error = "Card already initialized and card is valid. Can't initialize a valid card."; BlinkBuzzerFeedback(false,1); return false; }
            try
            {
                //Check Connected Card
                if (_connectedCard.CardType == null)
                {
                    _isErrorActive = true;
                    _error = "Card not supported";
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                //Get UID
                _connectedCard.UID = GetUID();
                if (_connectedCard.UID == null)
                {
                    _isErrorActive = true;
                    _error = "UID was not readable";
                    _connectedCard.Message = _error;
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                //Authentication Setup
                if (!SetReaderKeys())
                {
                    _isErrorActive = true;
                    _error = "Reader error: Unable to set reader authentication keys";
                    BlinkBuzzerFeedback(false,1); 
                    return false;
                }
                //Write Authentication Keys
                if (!SetAuthenticationKeys(BLOCK_NAME_TRAILER) || !SetAuthenticationKeys(BLOCK_CREDIT_TRAILER))
                {
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                // WRITE Initial Data
                if (!WriteData(BLOCK_NAME, Encoding.ASCII.GetBytes(name), 0x01) || !WriteData(BLOCK_SURNAME, Encoding.ASCII.GetBytes(surname), SURNAME_LENGHT) || !WriteData(BLOCK_CREDIT, BitConverter.GetBytes(-1.5), 0x01))
                {
                    _isErrorActive = true;
                    _error = "Unable to write data";
                    _connectedCard.Message = _error;
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                _connectedCard.Name = name;
                _connectedCard.SurName = surname;
                _connectedCard.Credit = -1.5;
                _connectedCard.CalcHash(HASH_PREFIX);
                //Write Validation Hash
                if (!WriteData(BLOCK_HASH, _connectedCard.Hash, HASH_LENGHT))
                {
                    _isErrorActive = true;
                    _error = "Unable to write Hash";
                    _connectedCard.Message = _error;
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                _connectedCard.IsValid = true;
                BlinkBuzzerFeedback(true,3);
                return true;
            }
            catch (Exception ex)
            {
                _isErrorActive = true;
                _error = "No card detected or error during detection. Remove card and retry.";
                BlinkBuzzerFeedback(false,1);
                return false;
            }
        }
        /// <summary>
        /// Pay with the connected card
        /// </summary>
        /// <param name="amount">amount to subtract from the card credit. If credit lower than amount then action is not done.</param>
        /// <returns>IsSuccessful</returns>
        public bool Pay(double amount)
        {
            if (!_isAvailable) { _error = "No reader available"; return false; }
            if (_connectedCard == null || !_connectedCard.IsValid) { _error = "Invalid card"; BlinkBuzzerFeedback(false, 1); return false; }
            double rest = _connectedCard.Credit - amount;
            if (rest < 0) { _error = "Saldo ontoereikend"; BlinkBuzzerFeedback(false, 1); return false; }
            //write credit
            if (!WriteData(BLOCK_CREDIT, BitConverter.GetBytes(rest), 0x01))
            {
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            _connectedCard.Credit = rest;
            _connectedCard.CalcHash(HASH_PREFIX);
            //Write Validation Hash
            if (!WriteData(BLOCK_HASH, _connectedCard.Hash, HASH_LENGHT))
            {
                _isErrorActive = true;
                _error = "Unable to write Hash";
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            BlinkBuzzerFeedback(true);
            return true;
        }
        /// <summary>
        /// Recharge the connected card
        /// </summary>
        /// <param name="amount">amount to add to the card credit</param>
        /// <returns>IsSuccessful</returns>
        public bool Recharge(double amount)
        {
            if (!_isAvailable) { _error = "No reader available"; return false; }
            if (_connectedCard == null || !_connectedCard.IsValid) { _error = "Invalid card"; BlinkBuzzerFeedback(false, 1); return false; }
            double rest = _connectedCard.Credit + amount;
            //write credit
            if (!WriteData(BLOCK_CREDIT, BitConverter.GetBytes(rest), 0x01))
            {
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            _connectedCard.Credit = rest;
            _connectedCard.CalcHash(HASH_PREFIX);
            //Write Validation Hash
            if (!WriteData(BLOCK_HASH, _connectedCard.Hash, HASH_LENGHT))
            {
                _isErrorActive = true;
                _error = "Unable to write Hash";
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            BlinkBuzzerFeedback(true);
            return true;
        }
        /// <summary>
        /// Change the name and surname of the owner of the connected card.
        /// </summary>
        /// <param name="name">name (voornaam) of the owner</param>
        /// <param name="surname">surname (achternaam) of the owner</param>
        /// <returns>IsSuccessful</returns>
        public bool ChangeOwner(string name, string surname)
        {
            if (!_isAvailable) { _error = "No reader available"; return false; }
            if (_connectedCard == null || !_connectedCard.IsValid) { _error = "Invalid card"; BlinkBuzzerFeedback(false, 1); return false; }
            //write name and surname
            if (!WriteData(BLOCK_NAME, Encoding.ASCII.GetBytes(name), 0x01))
            {
                _isErrorActive = true;
                _error = "Unable to change owner name";
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            _connectedCard.Name = name;
            if (!WriteData(BLOCK_SURNAME, Encoding.ASCII.GetBytes(surname), SURNAME_LENGHT))
            {
                _isErrorActive = true;
                _error = "Unable to change owner surname";
                _connectedCard.Message = _error;
                BlinkBuzzerFeedback(false,1);
                return false;
            }
            _connectedCard.SurName = surname;
            BlinkBuzzerFeedback(true,1);
            return true;
        }
        /// <summary>
        /// Clears all data from the connected card application sectors
        /// </summary>
        /// <returns>IsSuccessful</returns>
        public bool Reset()
        {
            if (!_isAvailable) { _error = "No reader available"; return false; }
            if (_connectedCard == null) { _error = "No card connected to reset"; return false; }
            try
            {
                //Authentication Setup
                if (!SetReaderKeys())
                {
                    _isErrorActive = true;
                    _error = "Reader error: Unable to set reader authentication keys";
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                // WRITE Reset Data
                if (!WriteData(BLOCK_NAME, new byte[16], 0x01) || !WriteData(BLOCK_SURNAME, new byte[32], SURNAME_LENGHT) || !WriteData(BLOCK_CREDIT, BitConverter.GetBytes(0.0), 0x01))
                {
                    _isErrorActive = true;
                    _error = "Unable to reset data";
                    _connectedCard.Message = _error;
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                _connectedCard.Name = "";
                _connectedCard.SurName = "";
                _connectedCard.Credit = 0.0;
                _connectedCard.CalcHash("NA");
                //Write Empty Hash
                if (!WriteData(BLOCK_HASH, new byte[32], HASH_LENGHT))
                {
                    _isErrorActive = true;
                    _error = "Unable to write Hash";
                    _connectedCard.Message = _error;
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                //Reset Factory Authentication Keys
                if (!ResetAuthenticationKeys(BLOCK_NAME_TRAILER) || !ResetAuthenticationKeys(BLOCK_CREDIT_TRAILER))
                {
                    BlinkBuzzerFeedback(false,1);
                    return false;
                }
                _connectedCard.IsValid = false;
                BlinkBuzzerFeedback(true,3);
                return true;
            }
            catch (Exception ex)
            {
                _isErrorActive = true;
                _error = "No card detected or error during detection. Remove card and retry.";
                BlinkBuzzerFeedback(false,1);
                return false;
            }
        }
        /// <summary>
        /// Initialize the card reader
        /// </summary>
        private void ConnectReader()
        {
            if (_isAvailable) { _bgworker.Stop(); return; }
            try
            {
                _readerName = "";
                _contextFacory = ContextFactory.Instance;
                _cardContext = _contextFacory.Establish(SCardScope.System);
                string[] readernames = _cardContext.GetReaders();
                for (int i = 0; i < readernames.Length; i++)
                {
                    if (readernames[i].Contains(READER_TYPE_NAME))
                    {
                        _readerName = readernames[i];
                        break;
                    }
                }
                if (_readerName == "")
                {
                    _isAvailable = false;
                    if (!_bgworker.Enabled) 
                    { 
                        System.Windows.MessageBox.Show("Reader not connected to system. Check USB connection. Reader LED should be green or red.", "Error"); 
                        _bgworker.Start(); 
                    }
                    return;
                }
                _cardMonitor = new SCardMonitor(_contextFacory, SCardScope.System);
                _cardMonitor.Initialized += MonitorInit;
                _cardMonitor.CardInserted += CardInsert;
                _cardMonitor.CardRemoved += CardRemoved;
                _cardMonitor.MonitorException += MonitorException;
                _cardMonitor.Start(_readerName);
                _isAvailable = true;
                _isErrorActive = false;
                _error = "";
                _bgworker.Stop();
            }
            catch (Exception e)
            {
                _isAvailable = false;
                if (!_bgworker.Enabled) 
                { 
                    System.Windows.MessageBox.Show(e.ToString(), "Error"); 
                    _bgworker.Start(); 
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Event that fires when printer's availibilty has changed. Get IsAvailable to check state.
        /// </summary>
        public event EventHandler OnAvailabilityChanged;
        protected virtual void AvailabilityChanged(EventArgs e)
        {
            EventHandler handler = OnAvailabilityChanged;
            handler?.Invoke(this, e);
        }

        #endregion

        #region Helpers
        private Card GetCard()
        {
            try
            {
                _cardReader = _cardContext.ConnectReader(_readerName, SCardShareMode.Shared, SCardProtocol.T1);
                _isoReader = new IsoReader(_cardContext, _readerName, SCardShareMode.Shared, SCardProtocol.T1);
                Card c = new Card() { IsValid = false, Message = "Card detection ..." };
                //Get CardType
                c.CardType = GetCardType();
                if (c.CardType == null)
                {
                    _isErrorActive = true;
                    _error = "Card not supported";
                    c.Message = _error;
                    return c;
                }
                //Get UID
                c.UID = GetUID();
                if (c.UID == null)
                {
                    _isErrorActive = true;
                    _error = "UID was not readable";
                    c.Message = _error;
                    return c;
                }
                //Authentication setup
                if (!SetReaderKeys())
                {
                    _isErrorActive = true;
                    _error = "Reader error: Unable to set reader authentication keys";
                    return null;
                }
                //Read Card Data
                //Name
                byte[] data;
                data = ReadData(BLOCK_NAME, 16);
                if (data == null)
                {
                    c.Message = _error;
                    return c;
                }
                c.Name = Encoding.ASCII.GetString(data);
                //Surname
                data = ReadData(BLOCK_SURNAME, 32);
                if (data == null)
                {
                    c.Message = _error;
                    return c;
                }
                c.SurName = Encoding.ASCII.GetString(data);
                //Credit
                data = ReadData(BLOCK_CREDIT, 8);
                if (data == null)
                {
                    c.Message = _error;
                    return c;
                }
                c.Credit = BitConverter.ToDouble(data, 0);
                //Hash
                if (_useValidation)
                {
                    data = ReadData(BLOCK_HASH, 32);
                    if (data == null)
                    {
                        c.Message = _error;
                        return c;
                    }
                    c.Hash = data;
                    //Validate Hash
                    if (!c.ValidateHash(HASH_PREFIX))
                    {
                        _isErrorActive = true;
                        _error = "Card Hash not correct. Possible tampering with card";
                        c.Message = "Unable to validate card";
                        return c;
                    }
                }
                c.IsValid = true;
                _isErrorActive = false;
                _error = "";
                return c;
            }
            catch (Exception ex)
            {
                _error = "No card detected or error during detection. Remove card and retry.";
                return null;
            }
        }
        // https://www.acs.com.hk/en/utility-tools/ => download utilities/tools for testing with acs readers
        // https://www.acs.com.hk/en/driver/3/acr122u-usb-nfc-reader/ => download drivers and manuals of ACR122U reader including api and specs
        private bool WriteData(byte block, byte[] data, byte blockLength)
        {
            //First authenticate the block
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x86; // Authenticate Block instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            byte[] buf = { 0x01, 0x00, block, 0x60, 0x00 }; // fixed data structure: 5 bytes (b0 b1 fixed, b2 = Block number hex , b3 = key type A = 0x60 or B = 0x61, b4 = key location in reader 0x00 or 0x01)
            apdu.Data = buf;
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Sectors are used for other application, no authentication possible";
                return false;
            }
            //Write data
            if (data.Length <= 16 & blockLength > 0x01)
            {
                blockLength = 0x01;
            }
            for (byte i = 0x00; i < blockLength; i++)
            {
                apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
                apdu.CLA = 0xFF; //first byte send
                apdu.INS = 0xD6; // write binary block
                apdu.P1 = 0x00;
                apdu.P2 = (byte)(block + i); // block to write
                // 16 bytes of data
                buf = new byte[16];
                Array.Copy(data, 16 * i, buf, 0, Math.Min(16, data.Length - i*16));
                apdu.Data = buf;
                resp = _isoReader.Transmit(apdu);
                if (resp.SW1 != 0x90)
                {
                    _isErrorActive = true;
                    _error = "Unable to write data";
                    return false;
                }
            }
            return true;
        }
        private void BlinkBuzzerFeedback(bool writesucces, byte buzzerCycles = 2)
        {
            if (writesucces)
            {
                CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
                apdu.CLA = 0xFF;
                apdu.INS = 0;
                apdu.P1 = 0x40;
                apdu.P2 = 0x8A; // only blink green
                byte[] dataCmd = { 0x1, 0x1, 0x4, 0x0 }; // T1 duration (unit = 100ms), T2 duration, Repetitions, Buzzer
                apdu.Data = dataCmd;
                Response resp = _isoReader.Transmit(apdu);
                if (resp.SW1 != 0x90)
                {
                    _isErrorActive = true;
                    _error = "Connection to reader was temporary lost, but no critical function was busy";
                }
                apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
                apdu.CLA = 0xFF;
                apdu.INS = 0;
                apdu.P1 = 0x40;
                apdu.P2 = 0x8A; // only blink green
                dataCmd = new byte[] { 0x1, 0x1, buzzerCycles, 0x01 }; // T1 duration (unit = 100ms), T2 duration, Repetitions, Buzzer
                apdu.Data = dataCmd;
                resp = _isoReader.Transmit(apdu);
                if (resp.SW1 != 0x90)
                {
                    _isErrorActive = true;
                    _error = "Connection to reader was temporary lost, but no critical function was busy";
                }
            }
            else
            {
                CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
                apdu.CLA = 0xFF;
                apdu.INS = 0;
                apdu.P1 = 0x40;
                apdu.P2 = 0xDD; // const red 
                byte[] dataCmd = { 0x8, 0x0, buzzerCycles, 0x1 }; // T1 duration (unit = 100ms), T2 duration, Repetitions, Buzzer
                apdu.Data = dataCmd;
                Response resp = _isoReader.Transmit(apdu);
                if (resp.SW1 != 0x90)
                {
                    _isErrorActive = true;
                    _error = "Connection to reader was temporary lost, but no critical function was busy";
                }
            }

        }
        private byte[] ReadData(byte block, int readLength)
        {
            //First authenticate the block
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x86; // Authenticate Block instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            byte[] buf = { 0x01, 0x00, block, 0x60, 0x00 }; // fixed data structure: 5 bytes (b0 b1 fixed, b2 = Block number hex , b3 = key type A = 0x60 or B = 0x61, b4 = key location in reader 0x00 or 0x01)
            apdu.Data = buf;
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Sectors are used for other application, no authentication possible";
                return null;
            }
            //Read data
            int blockLength = (int)Math.Ceiling(readLength / 16.0);
            byte[] data = new byte[readLength];
            for (byte i = 0x00; i < blockLength; i++)
            {
                apdu = new CommandApdu(IsoCase.Case2Short, SCardProtocol.T1);
                apdu.CLA = 0xFF; //first byte send
                apdu.INS = 0xB0; // Read binary block
                apdu.P1 = 0x00;
                apdu.P2 = (byte)(block + i); // block to write
                apdu.Le = Math.Min(16, readLength - i * 16);
                resp = _isoReader.Transmit(apdu);
                if (resp.SW1 != 0x90)
                {
                    _isErrorActive = true;
                    _error = "Card damaged, unable to read data";
                    return null;
                }
                Array.Copy(resp.GetData(), 0, data, i*16, Math.Min(16, readLength - i * 16));
            }
            return data;
        }
        private bool SetAuthenticationKeys(byte block)
        {
            //Set Authentication Keys in reader
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x82; // load key into reader
            apdu.P1 = 0x00; // key structure -> 00
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            apdu.Data = KEY_FACTORY; // key to load
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Set Authentication Key Error";
                return false;
            }
            //Authenticate Block with factory key
            apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x86; // Authenticate Block instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            byte[] data = { 0x01, 0x00, block, 0x60, 0x00 }; // fixed data structure: 5 bytes (b0 b1 fixed, b2 = Block number hex , b3 = key type A = 0x60 or B = 0x61, b4 = key location in reader 0x00 or 0x01)
            apdu.Data = data;
            resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Card sectors can't be initialized. Sectors are used by other application.";
                return false;
            }
            //update sector keys
            apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0xD6; // write binary block
            apdu.P1 = 0x00;
            apdu.P2 = block; // block to write
            // 16 bytes of sector trailer data
            data = new byte[16];
            KEY_A.CopyTo(data,0);
            ACCES_TYPE_STD.CopyTo(data, 6);
            KEY_B.CopyTo(data, 10);
            apdu.Data = data;
            resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Unable to overwrite authentication keys for sectors";
                return false;
            }
            //Set Authentication Keys back to application keys
            apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x82; // load key into reader
            apdu.P1 = 0x00; // key structure -> 00
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            apdu.Data = KEY_A; // key to load
            resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Set Authentication Key Error";
                return false;
            }
            return true;
        }
        private bool ResetAuthenticationKeys(byte block)
        {
            //Authenticate Block with Application key
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x86; // Authenticate Block instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            byte[] data = { 0x01, 0x00, block, 0x60, 0x00 }; // fixed data structure: 5 bytes (b0 b1 fixed, b2 = Block number hex , b3 = key type A = 0x60 or B = 0x61, b4 = key location in reader 0x00 or 0x01)
            apdu.Data = data;
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Card sectors can't be reset. Sectors are used by other application.";
                return false;
            }
            //update sector keys
            apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0xD6; // write binary block
            apdu.P1 = 0x00;
            apdu.P2 = block; // block to write
            // 16 bytes of sector trailer data
            data = new byte[16];
            KEY_FACTORY.CopyTo(data, 0);
            ACCES_TYPE_STD.CopyTo(data, 6);
            KEY_FACTORY.CopyTo(data, 10);
            apdu.Data = data;
            resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                _isErrorActive = true;
                _error = "Unable to overwrite authentication keys for sectors";
                return false;
            }
            return true;
        }
        private bool SetReaderKeys()
        {
            //Set Authentication Keys in reader
            //KEY A
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x82; // load key into reader
            apdu.P1 = 0x00; // key structure -> 00
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            apdu.Data = KEY_A; // key to load
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                return false;
            }
            //KEY B
            apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x82; // load key into reader
            apdu.P1 = 0x00; // key structure -> 00
            apdu.P2 = 0x01; // key number 00 or 01 (use as A and B)
            apdu.Data = KEY_B; // key to load
            resp = _isoReader.Transmit(apdu);
            if (resp.SW1 != 0x90)
            {
                return false;
            }
            return true;
        }
        private string GetCardType()
        {
            //Check if Mifare Classic Card
            _state = _cardReader.GetStatus();
            byte[] ATR = _state.GetAtr();
            if (ATR != null && ATR[14] == 0x01)
            {
               return "Mifare Classic 1K";
            }
            if (ATR != null && ATR[14] == 0x02)
            {
               return "Mifare Classic 4K";
            }
            return null;
        }
        private string GetUID()
        {
            // Read UID
            CommandApdu apdu = new CommandApdu(IsoCase.Case2Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0xCA; // READ UID Instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00;
            apdu.Le = 0x00; //predicted expected length (0x00 = unexpected or full length)
            Response resp = _isoReader.Transmit(apdu);
            return resp.HasData && resp.SW1 == 0x90 ? BitConverter.ToString(resp.GetData()) : null;
        }
        //Authenticate data is OBSOLETE
        private bool AuthenticateData(byte block,byte[] Key)
        {
            //Authenticate name sector trailer, if authentication is possible then card is already initialized.
            CommandApdu apdu = new CommandApdu(IsoCase.Case3Short, SCardProtocol.T1);
            apdu.CLA = 0xFF; //first byte send
            apdu.INS = 0x86; // Authenticate Block instruction
            apdu.P1 = 0x00;
            apdu.P2 = 0x00; // key number 00 or 01 (use as A and B)
            byte[] data = { 0x01, 0x00, block, 0x60, 0x00 }; // fixed data structure: 5 bytes (b0 b1 fixed, b2 = Block number hex , b3 = key type A = 0x60 or B = 0x61, b4 = key location in reader 0x00 or 0x01)
            apdu.Data = data;
            Response resp = _isoReader.Transmit(apdu);
            if (resp.SW1 == 0x90)
            {
                _isErrorActive = true;
                _error = "Card already initialized.";
                return false;
            }
            if (!SetAuthenticationKeys(BLOCK_NAME_TRAILER) || !SetAuthenticationKeys(BLOCK_CREDIT_TRAILER))
            {
                _isErrorActive = true;
                _error = "Card sectors can't be initilized. Sectores are used by other application.";
                return false;
            }
            return true;
        }
 
        #endregion
    }
    public class Card
    {
        public string UID { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string Owner => Name + " " + SurName;
        public double Credit { get; set; }
        public byte[] Hash { get; set; }
        public string CardType { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public void CalcHash(string hashprefix)
        {
            System.Security.Cryptography.SHA256 hasher = System.Security.Cryptography.SHA256.Create();
            Hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(hashprefix + Credit.ToString("F2")));
        }
        public bool ValidateHash(string hashprefix)
        {
            IsValid = true;
            System.Security.Cryptography.SHA256 hasher = System.Security.Cryptography.SHA256.Create();
            //old full double check -> possibly has rounding errors in edge cases -> invalidated correct card
            byte[] pwbytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(hashprefix + Credit.ToString()));
            for (int i = 0; i < pwbytes.Length; i++)
            {
                if (Hash[i] != pwbytes[i])
                {
                    IsValid = false; Message = "Invalid Hash. Card memory is damaged or card has been tampered with."; break;
                }
            }
            //new check: only 2 digits -> hopefully no more edge cases. Now do both checks in case of edge case
            if (!IsValid)
            {
                IsValid = true;
                pwbytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(hashprefix + Credit.ToString("F2")));
                for (int i = 0; i < pwbytes.Length; i++)
                {
                    if (Hash[i] != pwbytes[i])
                    {
                        IsValid = false; Message = "Invalid Hash. Card memory is damaged or card has been tampered with."; return false;
                    }
                }
            }
            IsValid = true;
            Message = "Valid card detected";
            return true;
        }

    }
}
