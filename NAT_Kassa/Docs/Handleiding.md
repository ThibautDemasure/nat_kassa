# NAT Kassa handleiding
Deze handleiding heeft een overzicht van alle acties die door de gebruikers van de kassa applicatie gedaan kunnen worden. Alle acties kunnen uitgevoerd worden vanuit het menu @icon-bars, afhankelijk van de status van de applicatie en de randapparatuur (RFID kaartlezer, bon printer, ...). Sommige acties kunnen ook gedaan worden door andere knoppen op het scherm te bedienen. Deze acties zijn beschikbaar afhankelijk van wat geselecteerd is op het scherm.

![Applicatie](img/app.png)

<!--TODO: nog toevoegen een FAQ of Q en A of iets in de zin van mogelijke problemen en hoe op te lossen. bvb voor - kan niet printen wat te doen -> check aan, kabel, papier, ... of opties conskaart niet acties wat te doen, hoe de taal aanpassen, ...
gegevens toevoegen van systeem admin-->

# Inhoud

<!-- Start Document Outline -->

* [Applicatie starten](#applicatie-starten)
* [Applicatie afsluiten](#applicatie-afsluiten)
* [User interface (UI)](#user-interface-ui)
	* [Menubalk](#menubalk)
	* [Transactieoverzicht](#transactieoverzicht)
	* [Gebruiker werkbalk](#gebruiker-werkbalk)
	* [Productoverzicht](#productoverzicht)
	* [Transactiegegevens](#transactiegegevens)
	* [Transactie actieknoppen](#transactie-actieknoppen)
	* [Statusbalk](#statusbalk)
* [Gebruikers](#gebruikers)
	* [Inloggen](#inloggen)
		* [Inlogscherm](#inlogscherm)
	* [Uitloggen](#uitloggen)
	* [Nieuwe gebruiker registreren](#nieuwe-gebruiker-registreren)
		* [Registratiescherm](#registratiescherm)
	* [Gebruikersgegevens aanpassen](#gebruikersgegevens-aanpassen)
		* [Gebruikersscherm](#gebruikersscherm)
	* [Account deactiveren](#account-deactiveren)
	* [Gebruikers activeren of deactiveren (Admin)](#gebruikers-activeren-of-deactiveren-admin)
* [Producten](#producten)
	* [Productoverzicht sorteren](#productoverzicht-sorteren)
	* [Productcategorieën weergeven of verbergen](#productcategorieën-weergeven-of-verbergen)
		* [Productcategorieën](#productcategorieën)
	* [Nieuw product toevoegen (Admin)](#nieuw-product-toevoegen-admin)
		* [Productdefinitiescherm](#productdefinitiescherm)
	* [Productgegevens aanpassen (Admin)](#productgegevens-aanpassen-admin)
		* [Productscherm](#productscherm)
	* [Producten activeren of deactiveren (Admin)](#producten-activeren-of-deactiveren-admin)
* [Transacties](#transacties)
	* [Nieuwe transactie openen](#nieuwe-transactie-openen)
	* [Transactie hernoemen](#transactie-hernoemen)
	* [Producten toevoegen](#producten-toevoegen)
		* [Producthoeveelheid ingeven](#producthoeveelheid-ingeven)
	* [Producten verwijderen](#producten-verwijderen)
	* [Betalingen toevoegen](#betalingen-toevoegen)
		* [Betaalscherm](#betaalscherm)
		* [Betalingsmethodes](#betalingsmethodes)
			* [Cash](#cash)
			* [Payconiq](#payconiq)
			* [Drankkaart](#drankkaart)
			* [Consumptiekaart](#consumptiekaart)
	* [Betalingen verwijderen](#betalingen-verwijderen)
	* [Bon printen](#bon-printen)
	* [Transactie afsluiten](#transactie-afsluiten)
	* [Transactie verwijderen](#transactie-verwijderen)
* [Consumptiekaarten](#consumptiekaarten)
	* [Saldo Bekijken](#saldo-bekijken)
	* [Kaart opladen](#kaart-opladen)
		* [Oplaadscherm](#oplaadscherm)
	* [Nieuwe kaart initialiseren](#nieuwe-kaart-initialiseren)
	* [Eigenaar aanpassen](#eigenaar-aanpassen)
	* [Overzicht kaarten](#overzicht-kaarten)
	* [Betalen met consumptiekaart](#betalen-met-consumptiekaart)
* [Betalingen](#betalingen)
* [Voorraadbeheer (Admin)](#voorraadbeheer-admin)
* [Instellingen](#instellingen)
	* [Taal](#taal)
	* [Hoeveelheid ingave](#hoeveelheid-ingave)
* [Notificaties](#notificaties)
* [Geldlade](#geldlade)
	* [Geldlade openen](#geldlade-openen)
	* [Cash tellen](#cash-tellen)
	* [Geldopname (Admin)](#geldopname-admin)
* [Help](#help)
* [Grafieken](#grafieken)
* [Logging](#logging)

<!-- End Document Outline -->

# Applicatie starten
De kassa applicatie kan gestart worden vanop het bureaublad door te dubbelklikken op de snelkoppeling NAT_Kassa of via de taakbalk door het NAT_Kassa icoon te klikken.

# Applicatie afsluiten
De applicatie kan op ieder moment afgesloten worden door op de **Applicatie Afsluiten** knop te klikken of door het hoofdvenster te sluiten :heavy_multiplication_x:.

<table style="width:25%">
<tr><th>Applicatie Afsluiten</th></tr>
<tr><th><img src="img/button-app-close.png"></img></th></tr>
</table>

> @icon-exclamation-circle Sluit eerst alle andere openstaande schermen van de applicatie, mocht dit het geval zijn, alvorens de applicatie af te sluiten.

> :exclamation: Sluit de applicatie niet af indien deze offline staat. Log gewoon uw account uit en laat weten aan een administrator dat de applicatie offline is. 

# User interface (UI)
De gebruikersinterface bestaat uit 1 hoofdscherm met onderstaande indeling:
- [Menubalk](#menubalk) met alle acties -> links
- [Transactieoverzicht](#transactieoverzicht) met alle openstaande transacties -> linksboven
- [Gebruiker werkbalk](#gebruiker-werkbalk) met de aangemelde gebruiker en aan-/afmeldacties -> rechtsboven
- [Productoverzicht](#productoverzicht) met alle zichtbare actieve producten -> centraal
- [Transactiegegevens](#transactiegegevens) met de details van de geselecteerde transactie -> rechts
- [Transactie actieknoppen](#transactie-actieknoppen) met vlugge acties voor de geselecteerde transactie -> rechtsonder
- [Statusbalk](#statusbalk) met informatie voor de gebruiker over de status van de applicatie en uitgevoerde acties -> onderaan

![Applicatie layout](img/app-layout.png)

Voor de verschillende acties worden telkens andere vensters of pop-ups geopend. Deze vensters worden weergegeven bij de respectievelijke acties.

## Menubalk
Het menu bevat alle acties die kunnen uitgevoerd worden met de kassa applicatie. De acties zijn gegroepeerd in verschillende categorieën, weergegeven in onderstaande afbeelding, en worden in deze handleiding ook opgedeeld volgens dezelfde categorieën. De menubalk wordt standaard ingevouwen weergegeven (categorielabels worden niet getoond), maar kan uitgevouwen worden door op de @icon-bars knop te drukken. @icon-chevron-left vouwt het menu terug in.

![Menubalk ingevouwen](img/menubar-collapsed.png) ![Menubalk volledig](img/menubar-full.png)

## Transactieoverzicht
Het transactieoverzicht bevat alle openstaande transacties van de aangemelde gebruiker op deze instantie van de applicatie (boven en beneden zijn apart). Voor iedere transactie wordt de unieke ID, het openstaande saldo en de beschrijving weergegeven. De geselecteerde of actieve transactie wordt gemarkeerd. Door op een transactie te klikken wordt deze geselecteerd.

Er kan ook vlug een [nieuwe transactie geopend worden](#nieuwe-transactie-openen) via de **Nieuwe Transactie** knop.

![Transactieoverzicht](img/transactions-overview.png)

## Gebruiker werkbalk
De gebruiker werkbalk toont de aangemelde gebruiker en laat ook toe om vlug de [gebruiker af te melden](#uitloggen), een [andere gebruiker aan te melden](#inloggen) of de [applicatie af te sluiten](#applicatie-afsluiten).

![Gebruiker werkbalk](img/active-user.png)

## Productoverzicht
Het productoverzicht bevat alle zichtbare actieve producten. Een gebruiker kan een product tegel aanklikken om dit product toe te voegen aan de geselecteerde open transactie.

![Productoverzicht](img/products-overview.png)

## Transactiegegevens
De transactiegegevens tonen de producten en betalingen die zijn toegevoegd aan de geselecteerde transactie. Het is ook mogelijk om van hieruit de [transactie beschrijving aan te passen](#transactie-hernoemen) en de [transactie te verwijderen](#transactie-verwijderen).

![Transactiegegevens](img/transaction-details.png)

## Transactie actieknoppen
Met deze knoppen kunnen vlug acties met betrekking tot de geselecteerde transactie uitgevoerd worden. Sommige acties zijn enkel beschikbaar indien een element in de transactiegegevens geselecteerd is. De acties worden beschreven in het hoofdstuk Transacties vanaf [Producten verwijderen](#producten-verwijderen) tot en met [Transactie afsluiten](#transactie-afsluiten).

![Transactie actieknoppen](img/transaction-quickactions.png)

## Statusbalk
De statusbalk geeft de status van de applicatie weer en toont de notificaties van de laatst uitgevoerde actie. De statusbalk bevat volgende componenten:
- Nummer van de kassa applicatie : 1 - 2
- Connectie status met de database : ONLINE - OFFLINE
- Knop om de volledige geschiedenis van de notificaties te bekijken : Show All - Hide All
- Notificatie van de laatst uitgevoerde actie of info voor de gebruiker

![Statusbalk](img/statusbar.png)

> @icon-exclamation-circle Het is belangrijk dat de applicatie zoveel mogelijk (liefst altijd) online is bij gebruik. Indien de applicatie offline zou zijn, gelieve te melden aan een systeem administrator.

<!--## Overzicht interacties

| Knop | Naam | Functie |
|------|------|---------|
| ![](img/button-add.png) | **Betalen** | Betaalvenster openen om een betaling toe te voegen |
| ![](img/button-close-transaction.png) | **Transactie Afsluiten**  | Afwerken van een transactie in de applicatie  |
-->

# Gebruikers
Een gebruikersaccount is nodig om de verschillende personen die de applicatie gebruiken te kunnen onderscheiden. Een gebruiker is altijd geassocieerd met een club of de NAT vzw op zich, dit laat toe om alle verkopen te koppelen met een bepaalde club om de inkomsten van drankverkoop correct te kunnen verdelen.

Onderstaande afbeelding geeft alle mogelijke acties met betrekking tot gebruikersaccounts weer. [Aanmelden/Inloggen](#inloggen), [Afmelden/Uitloggen](#uitloggen), [Gebruiker aanpassen](#gebruikersgegevens-aanpassen), [Nieuwe gebruiker registreren](#nieuwe-gebruiker-registreren) kan door iedereen gedaan worden. [Gebruikers activeren of deactiveren](#gebruikers-activeren-of-deactiveren-admin) kan enkel door admin accounts.

![Gebruikers menu acties](img/menu-users-actions.png)

## Inloggen
Inloggen kan op elk moment via de menu actie @icon-bars **Gebruikers -> Inloggen**, via de **Actieve Gebruiker** knop in de [gebruiker werkbalk](#gebruiker-werkbalk) of via de snelkoppeling **CTRL + S**. 

<table style="width:20%">
<tr><th>Actieve Gebruiker</th></tr>
<tr><th><img src="img/button-active-user.png"/></th></tr>
</table>

### Inlogscherm
Het inlogscherm wordt geopend. Selecteer de gebruiker die moet ingelogd worden, geef de pincode in en druk op de **Login** knop om de gebruiker aan te melden. Inloggen kan altijd geannuleerd worden door op **Cancel** te drukken of het inlogscherm te sluiten :heavy_multiplication_x:.

![Inlogscherm](img/screen-login.png)

> :bulb: **TIP**  
> De ledenlijsten worden opgevuld volgens aanmaakdatum van de gebruikers. Er kan gescrold worden door de lijsten door middel van een simpele sleepbeweging op de lijst (zoals dit ook kan op tablets en smartphones).

Indien reeds een gebruiker ingelogd is, wordt de huidige gebruiker eerst uitgelogd alvorens de geselecteerde gebruiker in te loggen. Na een succesvolle inlogpoging zal de actieve gebruiker veranderen.

![Actieve gebruiker](img/active-user.png)

Als de inlogpoging niet gelukt is (bv. omwille van een foute pincode), blijft de huidige gebruiker actief. Er wordt na een inlogpoging extra informatie weergegeven in de [statusbalk](#statusbalk).

## Uitloggen
Uitloggen kan op elk moment via de menu actie @icon-bars **Gebruikers -> Uitloggen**, via de **Uitloggen** knop in de [gebruiker werkbalk](#gebruiker-werkbalk) of via de snelkoppeling **CTRL + Q**. 

<table style="width:20%">
<tr><th>Uitloggen</th></tr>
<tr><th><img src="img/button-logout.png"/></th></tr>
</table>

> @icon-info-circle Het is aangeraden om altijd uw account af te melden indien u de applicatie niet meer gebruikt, zo kan niemand anders acties uitvoeren onder uw account. Geef ook best nooit uw pincode aan andere personen.

## Nieuwe gebruiker registreren
Om een nieuwe gebruiker van de kassa applicatie toe te voegen moet deze eerst geregistreerd worden. Een gebruiker registreren kan op elk moment door de menu actie @icon-bars **Gebruikers -> Registreren** uit te voeren.

### Registratiescherm
Het registratiescherm wordt geopend. Vul de gegevens van de gebruiker in en druk op **Add**.

Naam, email, pincode en club moeten minstens ingevuld worden, deze velden zijn gemarkeerd met een \*. Email en gebruikersnaam (voornaam + naam) moeten uniek zijn, er mogen geen andere gebruikers reeds dezelfde gegevens hebben. De registratie kan ook op ieder moment geannuleerd worden via de **Cancel** knop of door het registratiescherm te sluiten :heavy_multiplication_x:.

![Registratiescherm](img/screen-register.png)

Na een succesvolle registratie wordt de nieuwe gebruiker ook direct ingelogd op de applicatie. Dit is zichtbaar bij de actieve gebruiker in het hoofdscherm.  

![Actieve gebruiker](img/active-user.png)

Als de registratie niet gelukt is, wordt in de [statusbalk](#statusbalk) een notificatie gegeven waarom de registratie niet gelukt is.

## Gebruikersgegevens aanpassen
Een gebruiker kan na inloggen altijd zijn/haar eigen gegevens aanpassen via de menu actie @icon-bars **Gebruikers -> Aanpassen**.

> :bulb: TIP  
> Via deze actie kan ook een nieuwe pincode ingesteld worden.

### Gebruikersscherm
Het gebruikersscherm wordt geopend. Alle gegevens van de gebruiker worden weergegeven, met uitzondering van de pincode.

Pas de gegevens aan naar wens, vul de huidige pincode in bij *Old Pincode* en druk op **Edit**. De gegevens aanpassen kan altijd geannuleerd worden via de **Cancel** knop of door het scherm te sluiten :heavy_multiplication_x:.

![Gebruikersscherm](img/screen-users-edit.png)

> @icon-exclamation-circle Het is belangrijk dat alle gegevens ingevuld worden want alles wordt overschreven. Als het bv. niet de bedoeling is om de pincode aan te passen, moet de huidige pincode ook ingevuld worden bij *Pincode*.

> @icon-exclamation-circle *UserLevel* staat ook standaard op guest, dus admins moeten dit ook terug op admin zetten alvorens te bevestigen.

## Account deactiveren
U kunt uw eigen account altijd deactiveren via @icon-bars **Gebruikers -> Deactiveren**. Bij deactiveren wordt de account niet permanent verwijderd en dus kan deze achteraf terug geactiveerd worden door een administrator indien nodig.

## Gebruikers activeren of deactiveren (Admin)
De menu actie @icon-bars **Gebruikers -> Activeer/Deactiveer Gebruikers** toont een lijst van alle geregistreerde gebruikers. Een administrator kan een andere gebruiker activeren of deactiveren door op de **Gebruiker tegel** te tikken.

<table style="width:40%">
<tr><th>Geactiveerde gebruiker</th><th>Gedeactiveerde gebruiker</th></tr>
<tr><th><img src="img/toggle-user-enabled.png"/></th><th><img src="img/toggle-user-disabled.png"/></th></tr>
</table>

> :bulb: TIP  
> De lijst is alfabetisch op gebruikersnaam (voornaam + achternaam) gesorteerd en bevat nooit de ingelogde gebruiker zelf. Om uw eigen account te deactiveren moet de [account deactiveren actie](#account-deactiveren) gebruikt worden.

# Producten
Onder producten wordt alles verstaan die kan verkocht worden. Dit zijn voornamelijk dranken en snacks, maar de productenlijst kan eigenlijk ook andere items bevatten. 

De meeste acties voor producten zijn voorbehouden voor systeem administrators: [Nieuwe producten definiëren](#nieuw-product-toevoegen-admin), [Producten aanpassen](#productgegevens-aanpassen-admin), [Producten activeren of deactiveren](#producten-activeren-of-deactiveren-admin). Gewone *Guest* gebruikers kunnen enkel het [productoverzicht](#productoverzicht) aanpassen: [Productoverzicht sorteren](#productoverzicht-sorteren), [Productcategorieën weergeven of verbergen](#productcategorieën-weergeven-of-verbergen).

![Producten menu acties](img/menu-products-actions.png)

## Productoverzicht sorteren
Alle zichtbare producten worden altijd weergegeven per productcategorie, maar binnen 1 categorie kunnen de producten wel op verschillende manieren gesorteerd worden. Gebruik de menu actie @icon-bars **Producten -> @icon-list Sorteren** om een manier te selecteren in de dropdown lijst. 

![Producten sorteren](img/menu-products-sort.png)

Sorteren kan volgens:
- NAME-ASC  : Alfabetisch op productnaam
- NAME-DESC : Omgekeerd alfabetisch op productnaam
- ID-ASC    : Volgens aanmaakdatum van de producten, oudste eerst
- ID-DESC   : Volgens aanmaakdatum van de producten, nieuwste eerst

> @icon-info-circle Ieder product krijgt bij aanmaken een unieke ID in oplopende volgorde, daarom dat sorteren op ID eigenlijk overeenkomt met de aanmaakdatum.

> @icon-info-circle Standaard worden de producten alfabetisch op naam gesorteerd.

## Productcategorieën weergeven of verbergen
Het is mogelijk om weinig of niet gebruikte productcategorieën te verbergen in het [productoverzicht](#productoverzicht). Hiervoor kan de menu actie @icon-bars **Producten -> Bekijk Categorieën** gebruikt worden. Alle Categorieën worden weergegeven in een lijst. Verberg of toon een categorie door op de respectievelijke **Productcategorie tegel** te tikken.

<table style="width:40%">
<tr><th>Zichtbare Productcategorie</th><th>Verborgen Productcategorie</th></tr>
<tr><th><img src="img/toggle-productcategory-enabled.png"></img></th><th><img src="img/toggle-productcategory-disabled.png"></img></th></tr>
</table>

### Productcategorieën
Momenteel zijn volgende productcategorieën gedefinieerd in de applicatie:

![Productcategorieën](img/menu-products-categories.png)

> @icon-info-circle Standaard worden alle productcategorieën weergegeven. De categorie *All* en *None* kunnen gebruikt worden om vlug alle of geen enkele categorie weer te geven.

## Nieuw product toevoegen (Admin)
Een product heeft enkele belangrijke gegevens die door het systeem gebruikt worden voor aankooporders (aantal per pack, minimale bestelhoeveelheid, begin aantal, ...) en inkomstenberekeningen (aankoopprijs, verkoopprijs, ...). Deze gegevens moeten correct ingevuld worden voor nieuwe producten. Een nieuwe productdefinitie toevoegen kan via de menu actie @icon-bars **Producten -> Nieuw**.

### Productdefinitiescherm
Het productdefinitiescherm wordt geopend. Geef de gegevens voor het nieuwe product in en druk op **Add** om het nieuwe product toe te voegen. De actie kan altijd geannuleerd worden door op de **Cancel** knop te klikken of het scherm te sluiten :heavy_multiplication_x:.

![Productdefinitiescherm](img/screen-products-new.png)

De naam van het product moet uniek zijn en bedragen moeten correct ingegeven worden. Bij het toevoegen worden de ingave velden gecontroleerd en notificaties worden gegeven bij foutieve ingave. Controleer ook de [statusbalk](#statusbalk) na de toevoegpoging om te zien of de definitie van een nieuw product gelukt is.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

## Productgegevens aanpassen (Admin)
De productgegevens van bestaande (reeds gedefinieerde) producten kunnen ook altijd aangepast worden. Gebruik hiervoor de menu actie @icon-bars **Producten -> Aanpassen**.

### Productscherm
Het productscherm wordt geopend. Selecteer het aan te passen product in de lijst en pas de gegevens aan naar wens. Om de aanpassingen door te voeren moet telkens op de **Edit Product** knop geklikt worden.

![Productscherm](img/screen-products-edit.png)

De **Edit Products** knop geeft weer of de aanpassing gelukt is of niet (bv. door een duplicate naam). Een melding op het scherm toont wat er fout gelopen is en de velden met foutieve gegevens worden gemarkeerd.

<table>
<tr><th>Correcte toevoeging</th></tr>
<tr><th><img src="img/products-add-good.png"/></th></tr>
<tr><th>Foutieve bedrag ingave</th></tr>
<tr><th><img src="img/products-add-error-priceformat.png"/></th></tr>
<tr><th>Duplicate productnaam</th></tr>
<tr><th><img src="img/products-add-error-dup-name.png"/></th></tr>
</table>

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

Een nieuw product selecteren in de lijst, stelt de aanpasbare velden van de productgegevens in voor het nieuwe geselecteerde product. Sluit het scherm :heavy_multiplication_x: om te stoppen met aanpassen.

> :exclamation: Het is belangrijk dat dit scherm afgesloten wordt nadat de aanpassingen gedaan zijn. Het afsluiten zorgt ervoor dat de producten met de aangepaste gegevens terug uit de database geladen worden. Zolang dit niet gedaan wordt, werkt de applicatie verder met de oude productgegevens.  

> :exclamation: De andere kassa applicatie wordt niet automatisch mee geüpdatet en moet na dergelijke aanpassingen manueel herstart worden. Het is geen probleem voor de applicatie op zich, maar de gegevens (bv. verkoopprijs) zullen niet gelijk zijn in beide applicaties tot een herstart is uitgevoerd.

## Producten activeren of deactiveren (Admin)
De menu actie @icon-bars **Producten -> Activeer/Deactiveer Producten** toont een lijst van alle gedefinieerde producten. Een administrator kan producten activeren of deactiveren door op de **Productactivatie tegel** te tikken. Gedeactiveerde producten zijn niet zichtbaar voor de gebruikers van de applicatie en kunnen dus niet ingevoerd worden in transacties. De productdefinitie wordt niet permanent verwijderd uit de database en kan altijd later terug geactiveerd worden.

<table style="width:40%">
<tr><th>Geactiveerde producten</th><th>Gedeactiveerde producten</th></tr>
<tr><th><img src="img/toggle-product-enabled.png"/></th><th><img src="img/toggle-product-disabled.png"/></th></tr>
</table>

> @icon-info-circle De lijst is alfabetisch op productnaam gesorteerd.

Producten kunnen ook gedeactiveerd worden via @icon-bars **Producten -> Deactiveren**. In plaats van een lijst met de producten, kan nu op een **Product tegel** in het [productoverzicht](#productoverzicht) geklikt worden om dat product te deactiveren. Het overzicht komt hiervoor in een andere mode terecht. Om terug naar normale mode te gaan, moet opnieuw de menu actie @icon-bars **Producten -> Deactiveren** uitgevoerd worden.

> :exclamation: De andere kassa applicatie wordt niet automatisch mee geüpdatet en moet na dergelijke aanpassingen manueel herstart worden. Het is geen probleem voor de applicatie op zich, maar het product zal daar niet geactiveerd/gedeactiveerd zijn tot een herstart is uitgevoerd.

# Transacties
Een transactie bestaat uit **verkochte producten en de betalingen van deze producten**. De gebruiker bepaalt hoe hij/zij transacties wenst op te nemen en af te werken.

Mogelijke verwerkingen:
- 1 transactie per bestelling: Een transactie openen, de producten van de bestelling toevoegen, de bestelling direct afrekenen, na klaarzetten van de producten de transactie afsluiten.
- 1 transactie per klant per sessie: Een transactie openen per klant, voor iedere bestelling van die klant de producten toevoegen en klaarzetten, op het einde van avond/dag of alvorens de klant vertrekt de transactie afrekenen en afsluiten. Geef de transactie altijd een [aangepaste beschrijving](#transactie-hernoemen) om te weten van welke klant deze is.
- 1 transactie per sessie: Een transactie openen, alle verkopen van een volledige avond en alle betalingen van een volledige avond telkens toevoegen, op het einde van de sessie de transactie afsluiten.

> @icon-exclamation-circle De eerste manier (transactie per bestelling) wordt sterk aangeraden

Onderstaande afbeelding geeft alle mogelijke acties met betrekking tot transacties weer. [Nieuwe transacties starten](#nieuwe-transactie-openen), [Transacties afsluiten](#transactie-afsluiten), [Beschrijving van een transactie aanpassen](#transactie-hernoemen), [Producten toevoegen](#producten-toevoegen) aan of [producten verwijderen](#producten-verwijderen) van een transactie, [Betalingen toevoegen](#betalingen-toevoegen) aan of [betalingen verwijderen](#betalingen-verwijderen) van een transactie, [Bonnen printen](#bon-printen) en [Transacties verwijderen](#transactie-verwijderen) kan door iedereen gedaan worden.

![Transactie menu acties](img/menu-transactions-actions.png)
![Transactie vlugge acties](img/transaction-quickactions.png)

## Nieuwe transactie openen
Een bestelling ingeven kan enkel na het openen van een nieuwe transactie. Een transactie openen kan via de menu actie @icon-bars **Transacties -> Nieuwe Transactie**, via de **Nieuwe Transactie** knop in het [transactieoverzicht](#transactieoverzicht) of via de snelkoppeling **CTRL + N**.

<table style="width:25%">
<tr><th>Nieuwe Transactie</th></tr>
<tr><th><img src="img/button-new-transaction.png"/></th></tr>
</table>

De nieuwe transactie komt achteraan in het overzicht van de openstaande transacties en wordt direct geselecteerd. De details van de actieve (geselecteerde) transactie worden altijd weergegeven in de [transactiegegevens](#transactiegegevens) en sommige vlugge acties worden beschikbaar afhankelijk van wat geselecteerd is in de details.

![Transactieoverzicht](img/transactions-overview.png)

> :bulb: **TIP**  
> In het overzicht kunnen de andere openstaande transacties ook geselecteerd worden door deze aan te tikken. Op die manier kan gewisseld worden tussen transacties. Acties met betrekking tot een transactie (bv. producten toevoegen) gebeuren telkens voor de geselecteerde transactie.

<table style="width:50%">
<tr><th>Geselecteerde Transactie</th><th>Ongeselecteerde Transactie</th></tr>
<tr><th><img src="img/transaction-selected.png"></img></th><th><img src="img/transaction-unselected.png"></img></th></tr>
</table>

## Transactie hernoemen
Iedere transactie heeft een beschrijving die standaard tijdens het openen op "New" ingevuld wordt. Deze beschrijving kan vrij aangepast worden door op de @icon-pencil **knop** in de [transactiegegevens](#transactiegegevens) te drukken. Typ de beschrijving en druk nogmaals op de @icon-pencil **knop** of de entertoets op het toetsenbord om de beschrijving aan te passen.

![Transactiegegevens](img/transaction-details.png)

## Producten toevoegen
Producten toevoegen aan de geselecteerde transactie kan eenvoudig door op de betreffende **Product tegels** in het [productoverzicht](#productoverzicht) te tikken. 1 maal tikken op een product zal 1 product van dat type toevoegen aan de transactie.

<table style="width:40%">
<tr><th>Product tegel</th></tr>
<tr><th><img src="img/product-tile.png"/></th></tr>
<tr><td>Een product tegel bevat de naam van het product en de vraagprijs van dat product in € (euro).</td></tr>
</table>

 
> @icon-info-circle Het productoverzicht wordt gesorteerd volgens categorie en binnen een categorie alfabetisch. Dit kan aangepast worden via de [Productoverzicht sorteren](#productoverzicht-sorteren) en [Productcategorieën weergeven of verbergen](#productcategorieën-weergeven-of-verbergen) acties.

![Productoverzicht](img/products-overview.png)

> :bulb: **TIP**   
> Er kan gescrold worden door de producten door middel van een simpele sleepbeweging (1 vinger) op de lege ruimte in dit overzicht.

De [transactiegegevens](#transactiegegevens) aan de rechterkant worden direct geüpdatet, daar kan het toegevoegd aantal van ieder product bekeken worden. Ook de [statusbalk](#statusbalk) geeft telkens weer welk product er toegevoegd is en wat de huidige voorraad nog is.

### Producthoeveelheid ingeven
Via de menu actie @icon-bars **Instellingen -> Hoeveelheid ingave** kan een extra ingave balk geactiveerd worden om meerdere producten in 1 keer in te geven. Geef **eerst het aantal** in en druk dan op de betreffende **Product tegel** om het aantal producten van dat type toe te voegen aan de geselecteerde transactie. Geen aantal ingeven komt overeen met 1 product, zoals zonder [hoeveelheid ingave](#hoeveelheid-ingave) actief. Na iedere toevoeging wordt het aantal terug gereset.

![Hoeveelheid ingave balk](img/quantity-input.png)

## Producten verwijderen
Producten verwijderen uit een transactie kan enkel via de [transactie actieknoppen](#transactie-actieknoppen) onder de [transactiegegevens](#transactiegegevens) van de geselecteerde transactie. 

![Transactie producten verwijderen](img/remove-products.png)

Selecteer eerst een product type in de transactiegegevens en kies dan om 1 te verwijderen of om alle producten van dat type te verwijderen.

<table style="width:40%">
<tr><th>Verwijder 1</th><th>Verwijder Alle</th></tr>
<tr><th><img src="img/products-remove-1.png"/></th><th><img src="img/products-remove-all.png"/></th></tr>
</table>

## Betalingen toevoegen
Een betaling toevoegen kan via de **Betaling Toevoegen** actieknop in de [transactie actieknoppen](#transactie-actieknoppen). De actie kan enkel uitgevoerd worden als de geselecteerde transactie nog een openstaand saldo heeft.

<table style="width:25%">
<tr><th>Betaling Toevoegen</th></tr>
<tr><th><img src="img/button-add-payment.png"/></th></tr>
</table>

### Betaalscherm
Het betaalscherm wordt geopend. Selecteer de betalingsmethode (Cash, Payconiq, Drankkaart, Consumptiekaart) en druk op **Pay** om de betaling toe te voegen. Een betaling kan altijd geannuleerd worden door op de **Cancel** knop te drukken of het scherm te sluiten :heavy_multiplication_x:.

![Betaalscherm](img/screen-pay.png)

> :bulb: TIP  
> Er wordt standaard van uitgegaan dat gepast betaald wordt (altijd voor Payconiq en consumptiekaarten). Als geen bedrag ingegeven wordt rechtsboven, zal het betalingsbedrag gelijk zijn aan het openstaand saldo van de transactie (linksboven). 

Is het de bedoeling om slechts een deel van de transactie te betalen, dan kan het deelbedrag ingegeven worden in het betaalscherm en zal dat het betalingsbedrag zijn. De **Pay** knop toont altijd het betalingsbedrag dat zal geregistreerd worden.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

<table style="width:25%">
<tr><th>Pay met betalingsbedrag</th></tr>
<tr><th><img src="img/button-pay.png"/></th></tr>
</table>

### Betalingsmethodes
Er zijn momenteel 4 betalingsmethodes: Cash, Payconiq, Drankkaart en Consumptiekaart. Enkele opmerkingen i.v.m. de betalingsmethodes:

#### Cash
Bij cash betalingen kan ook het betaalde bedrag (hoger dan het openstaand saldo) ingegeven worden, dan zal tijdens het openen van de geldlade het teruggave bedrag getoond worden. De geldlade opent automatisch na het drukken op de **Pay** knop.

![Betaalscherm met teruggave bij cash betaling](img/screen-pay-refund.png)

> @icon-info-circle Het betaalscherm sluit ook automatisch na dichtdoen van de geldlade.

#### Payconiq
Een betaling via Payconiq moet afzonderlijk gedaan worden door de klant. Hiervoor kan de Payconiq QR code gescand worden. Het bedrag is normaal het openstaande saldo van de transactie, maar er kan ook een deel betaald worden. Geef hetzelfde bedrag in , als overgeschreven via Payconiq, in het betaalscherm.

:exclamation: De applicatie controleert het Payconiq portaal niet, dus de gebruiker van de kassa moet de Payconiq betaling van de klant manueel controleren.

#### Drankkaart
Papieren drankkaarten van €22.5 waarde, waarvoor €20 betaald wordt. Een drankkaart wordt aangekocht zoals een product en moet met cash of via Payconiq betaald worden. Het is onmogelijk om een transactie met een drankkaart artikel in te betalen met een drankkaart of consumptiekaart. 

:exclamation: Het is aan de gebruiker van de applicatie om het juiste bedrag van de drankkaarten te schrappen.

> @icon-info-circle Deze betalingsmethode zal binnenkort niet meer ondersteund worden.

#### Consumptiekaart
De consumptiekaart is een pre-paid RFID kaart waarop geld kan geladen worden. Hierbij wordt ook een €2.5 bonus gerekend per €20 dat opgeladen wordt. Een bedrag op de consumptiekaart laden kan enkel met cash of via Payconiq betaald worden.

> @icon-info-circle Deze kaarten worden nu nog als NFC Drankkaart weergegeven in de applicatie.
    <table style="width:20%">
    <tr><th>Betaalmethode Concumptiekaart</th></tr>
    <tr><th><img src="img/button-paymethod-custommercard.png"></img></th></tr>
    </table>

Consumptiekaarten moeten verbonden zijn met de applicatie om ermee te kunnen betalen, dit kan door de kaart op de RFID kaartlezer te leggen of dichtbij de RFID kaartlezer te houden.

Het beschikbare saldo op de kaart is zichtbaar in het betaalscherm op de keuze knop wanneer een kaart verbonden is. Indien het beschikbare saldo ontoereikend is, kan de transactie niet betaald worden met de consumptiekaart zonder manueel een betalingsbedrag in te geven lager of gelijk aan het beschikbare saldo.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

> @icon-info-circle Een consumptiekaart wordt best verbonden alvorens het betaalscherm te openen, maar het is ook mogelijk om dit te doen tijdens de betaling.

Controleer na de betaling de notificatie in de [statusbalk](#statusbalk) om te zien of de betaling correct verwerkt is. 

Meer informatie over het gebruik van deze kaarten (geld opladen e.d.) in het onderdeel [Consumptiekaarten](#consumptiekaarten).

## Betalingen verwijderen
Om een betaling van een transactie te verwijderen kan de **Betaling Verwijderen** actieknop in de [transactie actieknoppen](#transactie-actieknoppen) gebruikt worden. Selecteer eerst de gewenste betaling van de transactie om deze actie mogelijk te maken.

<table style="width:25%">
<tr><th>Betaling Verwijderen</th></tr>
<tr><th><img src="img/button-remove-payment.png"/></th></tr>
</table>

*Betalingen met cash* kunnen altijd verwijderd worden. Het is aan de gebruiker van de kassa om correct terug te geven aan de klant. De geldlade kan geopend worden via de menu actie @icon-bars **Geldlade -> Geldlade Openen**. 

> @icon-info-circle Alle acties met betrekking tot de geldlade en cash geld worden geregistreerd en bijgehouden. Indien grote bedragen zouden ontbreken, kan onderzocht worden welke gebruikers allemaal de geldlade geopend hebben sinds de laatste controle.

> @icon-exclamation-circle De ingelogde account zal verantwoordelijk gesteld worden, let dus op dat niemand kan inloggen onder uw naam.

*Betalingen met Payconiq* kunnen niet ongedaan gemaakt worden. Het is onmogelijk om het betaalde bedrag van de effectieve Payconiq overschrijving terug te storten. De betaling in het kassasysteem kan wel verwijderd worden mocht een verkeerde ingave gedaan zijn en het bedrag nog niet daadwerkelijk overgeschreven werd via Payconiq.

Een *betaling met consumptiekaart* kan momenteel nog niet correct ongedaan gemaakt worden. De betaling kan verwijderd worden, maar het bedrag zal niet opnieuw op de kaart geladen worden.

> @icon-exclamation-circle Het is dus aangeraden om dergelijke betalingen niet te verwijderen. Indien dit toch gebeurd is, laat dan de kaart UID (XX-XX-XX-XX-XX-XX-XX) en de transactie ID in kwestie weten aan een systeem administrator en sluit de applicatie niet af. Een administrator kan de logs controleren en het nodige ondernemen om het verloren bedrag op de kaart te laden.

> @icon-info-circle Het ongedaan maken van een betaling met consumptiekaart zal nog worden toegevoegd later. De kaart moet natuurlijk wel verbonden zijn met de applicatie op het moment van verwijderen.

## Bon printen
Indien gewenst kan een bon van de transactie geprint worden door op de **Bon Printen** actieknop in de [transactie actieknoppen](#transactie-actieknoppen) te drukken. Dit is enkel mogelijk indien de ticketprinter verbonden is, actief is en geen foutmelding weergeeft (bv. papier op). De knop wordt geactiveerd (komt blauw) indien het mogelijk is om te printen.

<table style="width:20%">
<tr><th>Bon Printen</th></tr>
<tr><th><img src="img/print-ticket-disabled.png"/></th></tr>
</table>

Er kan ook een bon geprint worden via de menu actie @icon-bars **Transacties -> Bon Afdrukken**. Via de menu actie @icon-bars **Transacties -> Automatisch Bonnen Printen** kan een functie geactiveerd worden zodat de bon sowieso geprint wordt tijdens het afsluiten van een transactie.
<!-- TODO menu transacties is nog met engelse auto print-->

![Transactie menu auto bon printen](img/menu-autoprint.png)

## Transactie afsluiten
**Een transactie moet na verwerking altijd afgesloten worden.** Dit kan via de **Transactie Afsluiten** knop in de [transactie actieknoppen](#transactie-actieknoppen) onder de transactie details of via de menu actie @icon-bars **Transacties -> Transactie Afsluiten**.

<table style="width:25%">
<tr><th>Transactie Afsluiten</th></tr>
<tr><th><img src="img/button-close-transaction.png"/></th></tr>
</table>

@icon-exclamation-circle Afsluiten is enkel mogelijk als de transactie volledig betaald is.

![Transactie afsluiten](img/close-transaction.png)

Het afsluiten of afvinken van een transactie zorgt ervoor dat de status niet meer (ongewenst of per ongeluk) aangepast kan worden door de gebruiker van kassa. De transactie wordt als volledig verwerkt behandeld en ingevoerde betalingen of product afnames kunnen niet meer ongedaan worden. De transactie is ook niet meer zichtbaar na afsluiten.

## Transactie verwijderen
Een transactie verwijderen maakt alle acties met betrekking tot die transactie ongedaan. De volledige transactie wordt uit de database verwijderd, alle producten worden terug toegevoegd aan de voorraad en alle betalingen worden gewist. Dit komt overeen met de transactie volledig annuleren.

De transactie verwijderen kan via de menu actie @icon-bars **Transacties -> Verwijderen** of via de **Transactie Verwijderen** knop op de [transactiegegevens](#transactiegegevens).

<table style="width:20%">
<tr><th>Transactie Verwijderen</th></tr>
<tr><th><img src="img/button-transaction-remove.png"/></th></tr>
</table>

:exclamation: Eigenlijk zou deze actie nooit nodig moeten zijn, foutieve ingaven kunnen via de corresponderende acties ongedaan gemaakt worden (bv. een product terug verwijderen van de transactie). Het is dus zeer belangrijk dat dit enkel gedaan wordt voor transacties die daadwerkelijk voortijdig geannuleerd zijn of niet zijn doorgegaan. Er zit daarom ook een extra prompt op de actie om te bevestigen dat verwijderen van de transactie zeker gewenst is.

# Consumptiekaarten
Consumptiekaarten zijn pre-paid RFID kaarten waarop geld kan geladen worden. Hierbij wordt ook een €2.5 bonus gerekend per €20 dat opgeladen wordt. Een bedrag op de consumptiekaart laden kan enkel met cash of via Payconiq betaald worden. Via het menu @icon-bars **Consumptiekaarten** kunnen verschillende acties ondernomen worden in verband met deze kaarten. 

@icon-exclamation-circle Deze acties zijn enkel beschikbaar indien een kaart verbonden is met de applicatie via de RFID kaartlezen. Het is dus belangrijk om altijd eerst een kaart op de lezer te leggen alvorens er een actie mee te doen.

![Consumptiekaarten menu](img/menu-customercards.png)

## Saldo Bekijken
Via de menu actie @icon-bars **Consumptiekaarten -> Saldo Bekijken** kunnen de actuele gegevens van de verbonden kaart weergegeven worden. Er wordt een klein venster met de eigenaar, het saldo en de UID (unieke ID) van de kaart weergegeven. Het venster moet terug afgesloten worden (via de **Sluiten** knop) alvorens verder te werken met de kassa applicatie.

![Consumptiekaart gegevens weergeven](img/screen-check-card-balance.png)

## Kaart opladen
<!--TODO testen dat er geen geld teveel opgeladen wordt bij saldo veranderen (geen bonus).-->
Consumptiekaarten zijn pre-paid kaarten, er moet dus eerst een bepaald krediet opgeladen worden alvorens deze kaarten gebruikt kunnen worden om betalingen mee te doen. De menu actie @icon-bars **Consumptiekaarten -> Kaart Opladen** moet gebruikt worden om geld op de verbonden kaart te laden.

### Oplaadscherm
Het oplaadscherm wordt geopend, dit is een aangepaste versie van het betaalscherm. Selecteer het bedrag om op te laden (zonder rekening te houden met bonussen) en de betaalmethode en druk op **Kaart Opladen**.

![Oplaadscherm](img/screen-charge-card.png)

> :bulb: TIP
> Het is mogelijk om een ander bedrag in te geven in het tekstveld *Ander bedrag*, dit zal het geselecteerde bedrag overschrijven. Verwijder de input in het tekstveld om het geselecteerde bedrag te gebruiken.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

Onderaan het oplaadscherm wordt weergegeven welk bedrag betaald moet worden en welk bedrag er zal bijgeladen worden (inclusief bonussen). Controleer na uitvoering de [statusbalk](#statusbalk) voor mocht iets misgegaan zijn tijdens de verwerking van de actie.

:exclamation: Het is aan de kassa gebruiker om de Payconiq betaling te controleren indien voor Payconiq gekozen is. 

> @icon-info-circle Bij cash betalingen wordt de geldlade geopend om het betaalde bedrag te kunnen ontvangen.

## Nieuwe kaart initialiseren
Om een nieuwe kaart in gebruik te nemen moet de kaart eerst geïnitialiseerd worden. Initialiseren kan via de menu actie @icon-bars **Consumptiekaarten -> Nieuw**.

Nieuwe kaarten worden in de geldlade bijgehouden. Open de geldlade via de menu actie @icon-bars **Geldlade -> Geldlade Openen**. Leg de nieuwe kaart op de kaartlezer en start de initialisatie. Er wordt een klein ingave venster geopend. Initialisatie vereist enkel de ingave van de eigenaar van de kaart. Druk op **OK** om de initialisatie te voltooien. **Cancel** annuleert de initialisatie en sluit het venster.

![Initialisatie venster](img/screen-init-card.png)

> @icon-exclamation-circle Tijdens initialisatie wordt het saldo van de kaart op -€1.5 (negatief €1.5) gezet. Dit is de kost van de kaart zelf en wordt zo eenmalig in rekening gebracht aan de gebruiker van de kaart. De eerste oplaadbeurt zal er dus €1.5 minder op de kaart staan dan verwacht. Het is ook belangrijk om dit te vermelden aan de klant.

## Eigenaar aanpassen
De eigenaar van de kaart kan altijd aangepast worden via de menu actie @icon-bars **Consumptiekaarten -> Eigenaar Aanpassen**. De eigenaar is de voornaam + achternaam van de kaarthouder en dient om een te weten van wie een bepaalde kaart is. Het venster is hetzelfde als voor het initialiseren van een kaart, maar enkel de eigenaar wordt aangepast en het saldo wordt niet geïnitialiseerd. Druk op **OK** om de aanpassing door te voeren, de actie annuleren kan via **Cancel**.

![Initialisatie venster](img/screen-init-card.png)

> @icon-info-circle De voornaam mag maximaal 16 karakters lang zijn en de achternaam 32 karakters.

## Overzicht kaarten
De menu actie @icon-bars **Consumptiekaarten -> Overzicht Kaarten** toont een lijst van alle geregistreerde kaarten. Kan eventueel gebruikt worden om het saldo van iemand op te zoeken die zijn kaart niet bij zich heeft. Verder kunnen geen acties ondernomen worden via dit overzicht.

## Betalen met consumptiekaart
Zie [betalingen met consumptiekaart](#consumptiekaart) onder [betalingen toevoegen aan transactie](#betalingen-toevoegen).

# Betalingen
Zie [betalingen toevoegen](#betalingen-toevoegen) onder Transacties.

# Voorraadbeheer (Admin)
:construction: :construction: :construction: **UNDER CONSTRUCTION** :construction: :construction: :construction:

# Instellingen
Onder het menu @icon-bars **Instellingen** @icon-gear kunnen enkele applicatie instellingen aangepast worden. 

> @icon-info-circle De instellingen worden niet per gebruiker opgeslagen, dus moeten momenteel na heropstart van de applicatie opnieuw ingesteld worden. De enige uitzondering is de taal van de applicatie. Deze wordt globaal geactiveerd voor alle gebruikers.

## Taal
De taal van de applicatie kan op elk moment aangepast worden via de menu actie @icon-bars **Instellingen -> Taal**. Selecteer de gewenste taal door op het corresponderende menu item te klikken.

> @icon-info-circle Momenteel worden enkel Nederlands (NLD) en Engels (ENG) ondersteund en het kan zijn dat nog niet alle grafische elementen vertaald worden.

## Hoeveelheid ingave
Via de menu actie @icon-bars **Instellingen -> Hoeveelheid ingave** kan een extra ingave balk geactiveerd worden om een hoeveelheid in te geven voor [product toevoegingen aan transacties](#producten-toevoegen).

![Instellingen menu hoeveelheid ingave](img/menu-settings.png)![](img/menu-qinput.png)

Dit is handig indien er veel producten van hetzelfde type toegevoegd moeten worden in 1 keer. Geef **eerst het aantal** in en druk dan op de betreffende **Product tegel** om het aantal producten van dat type toe te voegen aan de geselecteerde transactie. Geen aantal ingeven komt overeen met 1 product, zoals zonder hoeveelheid ingave actief. Na iedere toevoeging wordt het aantal terug gereset.

![Hoeveelheid ingave balk](img/quantity-input.png)

# Notificaties
:construction: :construction: :construction: **UNDER CONSTRUCTION** :construction: :construction: :construction:

# Geldlade
Het geldlade menu bevat alle acties die te maken hebben met de geldlade van het kassasysteem en toont ook hoeveel geld er in de geldlade zou moeten zitten bij controle.

> :exclamation: Alle acties met betrekking tot de geldlade en cash geld worden geregistreerd en bijgehouden. Indien grote bedragen zouden ontbreken, kan onderzocht worden welke gebruikers allemaal de geldlade geopend hebben sinds de laatste controle. De ingelogde account zal verantwoordelijk gesteld worden, let dus op dat niemand kan inloggen onder uw naam.

Er zijn 2 acties die door iedere gebruiker kunnen uitgevoerd worden: [Geldlade openen](#geldlade-openen) en [Cash tellen](#cash-tellen). Een administrator kan ook een 3de actie [Geldopname](#geldopname-admin) uitvoeren.

![Geldlade menu](img/menu-cashdrawer.png)

## Geldlade openen
Indien het nodig is om de geldlade te openen, zonder dat een cash betaling verwerkt wordt, kan de menu actie @icon-bars **Geldlade -> Geldlade Openen** gebruikt worden. Deze actie zal de geldlade openen zonder extra prompt aan de gebruiker om terug te sluiten.

> @icon-exclamation-circle Het is wel belangrijk dat de geldlade opnieuw dicht gedaan wordt alvorens een andere actie met betrekking tot de geldlade of cash verwerkingen uitgevoerd wordt.

## Cash tellen
De menu actie @icon-bars **Geldlade -> Cash Tellen** opent de geldlade, maar de gebruiker wordt gevraagd om het actuele totale bedrag in de geldlade te tellen en dit in te geven.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

> @icon-exclamation-circle De inhoud van de kassa tellen is geen verplichting momenteel, maar kan zeker helpen om correct gebruik van het kassasysteem te waarborgen en te controleren. Mochten er problemen ondervonden worden met cash verwerkingen, kan dit verplicht worden bij afsluit van de kassa om een betere controle te hebben.

## Geldopname (Admin)
Via de menu actie @icon-bars **Geldlade -> Geldopname** kan een administrator registreren wanneer hij/zij een bedrag uit de geldlade genomen heeft om de hoeveelheid cash te beperken. De actie opent de geldlade en een klein input venster waarin de gebruiker moet ingeven hoeveel er uitgenomen is. De applicatie kan niet verder gebruikt worden tot een bedrag is ingegeven en het venster gesloten is.

> :bulb: TIP  
> Kommagetallen moeten ingegeven worden met een . (punt) en niet een , (komma) als separator.

> @icon-exclamation-circle Het is belangrijk om geldopnames te registreren in het systeem (en niet gewoon geld uitnemen tijdens het openen van de geldlade op andere momenten) omdat het *bedrag on hand* anders niet meer zal kloppen zonder dat er eigenlijk problemen zijn geweest.

# Help
De menu acties @icon-bars **Help** @icon-question-circle zijn snelkoppelingen naar de corresponderende hoofdstukken in deze handleiding. Een topic selecteren in de **Help** @icon-question-circle opent deze handleiding in de standaard browser (Chrome, Edge, Firefox, ...) op die topic en de volledige handleiding is van daaruit beschikbaar.

De handleiding kan ook geopend worden via de snelkoppeling op het bureaublad.

# Grafieken
:construction: :construction: :construction: **UNDER CONSTRUCTION** :construction: :construction: :construction:

# Logging
:construction: :construction: :construction: **UNDER CONSTRUCTION** :construction: :construction: :construction: