﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for SplitTransactionWindow.xaml
    /// </summary>
    public partial class SplitTransactionWindow : Window
    {
        public SplitTransactionWindow(BLL.Transaction master)
        {
            InitializeComponent();
            
            _mastercopy = new BLL.Transaction(master.Id, master.Openedby);
            _mastercopy.Description = master.Description;
            foreach (BLL.Product p in master.Products)
            {
                _mastercopy.AddProduct(p);
            }
            _split = new BLL.Transaction(0, master.Openedby);
            wpMasterT.Children.Add(new TransactionDetailsControl(_mastercopy));
            wpSplitT.Children.Add(new TransactionDetailsControl(_split));

        }


        private int _selectedMasterIndex = -1;
        private int _selectedSplitIndex = -1;
        private bool _cancel = true;
        public bool Cancel { get => _cancel; set => _cancel = value; }

        private BLL.Transaction _mastercopy;
        private BLL.Transaction _split;
        public List<BLL.Product> ProductsToSplit { get => _split.Products; }


        private void BtnSplit_Click(object sender, RoutedEventArgs e)
        {
            _cancel = false;
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            _cancel = true;
            this.Close();
        }

        private void BtnAdd1_Click(object sender, RoutedEventArgs e)
        {
            var td = wpMasterT.Children[0] as UI.TransactionDetailsControl;
            if (td.lvProducts.SelectedIndex != -1)
            {
                _selectedMasterIndex = td.lvProducts.SelectedIndex;
                BLL.Product p = _mastercopy.Products[_selectedMasterIndex];
                BLL.Product newp = new BLL.Product(p.Name, p.Price,p.Purchaseprice, p.Category,p.Id,1,p.IsActive,p.IsSynchronized);
                _split.AddProduct(newp);
                int count = _mastercopy.Products.Count;
                _mastercopy.SubstractProduct(newp);
                if (_mastercopy.Products.Count < count)
                {
                    _selectedMasterIndex--;
                }
                foreach (BLL.Product splitp in _split.Products)
                {
                    if (splitp.Name == p.Name)
                    {
                        _selectedSplitIndex = _split.Products.IndexOf(splitp);
                    }
                }
                wpMasterT.Children.RemoveAt(0);
                wpMasterT.Children.Add(new TransactionDetailsControl(_mastercopy));
                var mastertd = wpMasterT.Children[0] as UI.TransactionDetailsControl;
                mastertd.lvProducts.SelectedIndex = _selectedMasterIndex;

                wpSplitT.Children.RemoveAt(0);
                wpSplitT.Children.Add(new TransactionDetailsControl(_split));
                var splittd = wpSplitT.Children[0] as UI.TransactionDetailsControl;
                splittd.lvProducts.SelectedIndex = _selectedSplitIndex;
            }
        }

        private void BtnAddAll_Click(object sender, RoutedEventArgs e)
        {
            var td = wpMasterT.Children[0] as UI.TransactionDetailsControl;
            if (td.lvProducts.SelectedIndex != -1)
            {
                _selectedMasterIndex = td.lvProducts.SelectedIndex;
                BLL.Product p = _mastercopy.Products[_selectedMasterIndex];
                //BLL.Product newp = new BLL.Product(p.Name, p.Price, p.Category);
                _split.AddProduct(p);
                _mastercopy.SubstractProduct(p);

                _selectedMasterIndex--;
                foreach (BLL.Product splitp in _split.Products)
                {
                    if (splitp.Name == p.Name)
                    {
                        _selectedSplitIndex = _split.Products.IndexOf(splitp);
                    }
                }
                
                wpMasterT.Children.RemoveAt(0);
                wpMasterT.Children.Add(new TransactionDetailsControl(_mastercopy));
                var mastertd = wpMasterT.Children[0] as UI.TransactionDetailsControl;
                mastertd.lvProducts.SelectedIndex = _selectedMasterIndex;

                wpSplitT.Children.RemoveAt(0);
                wpSplitT.Children.Add(new TransactionDetailsControl(_split));
                var splittd = wpSplitT.Children[0] as UI.TransactionDetailsControl;
                splittd.lvProducts.SelectedIndex = _selectedSplitIndex;
            }
        }

        private void BtnDel1_Click(object sender, RoutedEventArgs e)
        {
            var td = wpSplitT.Children[0] as UI.TransactionDetailsControl;
            if (td.lvProducts.SelectedIndex != -1)
            {
                _selectedSplitIndex = td.lvProducts.SelectedIndex;
                BLL.Product p = _split.Products[_selectedSplitIndex];
                BLL.Product newp = new BLL.Product(p.Name, p.Price, p.Purchaseprice, p.Category, p.Id, 1, p.IsActive, p.IsSynchronized);
                _mastercopy.AddProduct(newp);
                int count = _split.Products.Count;
                _split.SubstractProduct(newp);
                if (_split.Products.Count < count)
                {
                    _selectedSplitIndex--;
                }
                foreach (BLL.Product splitp in _mastercopy.Products)
                {
                    if (splitp.Name == p.Name)
                    {
                        _selectedMasterIndex = _mastercopy.Products.IndexOf(splitp);
                    }
                }

                wpMasterT.Children.RemoveAt(0);
                wpMasterT.Children.Add(new TransactionDetailsControl(_mastercopy));
                var mastertd = wpMasterT.Children[0] as UI.TransactionDetailsControl;
                mastertd.lvProducts.SelectedIndex = _selectedMasterIndex;

                wpSplitT.Children.RemoveAt(0);
                wpSplitT.Children.Add(new TransactionDetailsControl(_split));
                var splittd = wpSplitT.Children[0] as UI.TransactionDetailsControl;
                splittd.lvProducts.SelectedIndex = _selectedSplitIndex;
            }
        }

        private void BtnDelAll_Click(object sender, RoutedEventArgs e)
        {
            var td = wpSplitT.Children[0] as UI.TransactionDetailsControl;
            if (td.lvProducts.SelectedIndex != -1)
            {
                _selectedSplitIndex = td.lvProducts.SelectedIndex;
                BLL.Product p = _split.Products[_selectedSplitIndex];
                //BLL.Product newp = new BLL.Product(p.Name, p.Price, p.Category);
                _mastercopy.AddProduct(p);
                int count = _split.Products.Count;
                _split.SubstractProduct(p);
                if (_split.Products.Count < count)
                {
                    _selectedSplitIndex--;
                }
                foreach (BLL.Product splitp in _mastercopy.Products)
                {
                    if (splitp.Name == p.Name)
                    {
                        _selectedMasterIndex = _mastercopy.Products.IndexOf(splitp);
                    }
                }

                wpMasterT.Children.RemoveAt(0);
                wpMasterT.Children.Add(new TransactionDetailsControl(_mastercopy));
                var mastertd = wpMasterT.Children[0] as UI.TransactionDetailsControl;
                mastertd.lvProducts.SelectedIndex = _selectedMasterIndex;

                wpSplitT.Children.RemoveAt(0);
                wpSplitT.Children.Add(new TransactionDetailsControl(_split));
                var splittd = wpSplitT.Children[0] as UI.TransactionDetailsControl;
                splittd.lvProducts.SelectedIndex = _selectedSplitIndex;
            }
        }
    }
}
