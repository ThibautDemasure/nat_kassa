﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for PayWindow.xaml
    /// </summary>
    public partial class PayWindow : Window
    {
        private BLL.Transaction _transaction;
        private CashDrawer _CD = new CashDrawer();
        private NfcReader _reader;
        private BLL.PaymentMethod _method = BLL.PaymentMethod.None;
        private double _amount;
        private bool _cancel = true;
        private double _refund = 0;
        private Card _card;
        public PaymentMethod Method { get => _method; set => _method = value; }
        public double Amount { get => _amount; set => _amount = value; }
        public bool Cancel { get => _cancel; set => _cancel = value; }
        public double Refund { get => _refund; set => _refund = value; }
        public Card CustomerCard { get => _card;
            set
            {
                _card = value;
                txtBalance.Text = "Saldo " + _card.Credit.ToString("F2");
            }
        }

        public PayWindow(BLL.Transaction transaction, NfcReader reader)
        {
            InitializeComponent();
            lblInfo.Visibility = Visibility.Hidden;
            lblRefundAmount.Visibility = Visibility.Hidden;
            _transaction = transaction;
            wpTransactionDetails.Children.Add(new TransactionDetailsControl(_transaction));
            _amount = _transaction.Amount - _transaction.AmountPaid;
            txtAmount.Text = _amount.ToString();
            ucInputCalc.AmountInputChanged += UcInputCalc_AmountInputChanged;
            ucInputCalc.AmountInputDeleted += UcInputCalc_AmountInputDeleted;
            CustomerCard = new Card();
            _CD.Closed += OnDrawerClosed;
            _reader = reader;
            _reader.OnAvailabilityChanged += Card_Changed;
        }

        private void Card_Changed(object sender, EventArgs e)
        {
            if (_reader.IsCardConnected && _reader.ConnectedCard.IsValid) { this.Dispatcher.Invoke(() => { this.CustomerCard = _reader.ConnectedCard; }); }
            if (!_reader.IsCardConnected) { this.Dispatcher.Invoke(() => { this.CustomerCard = new Card(); }); }
            if (_method == PaymentMethod.MemberCard) { this.Dispatcher.Invoke(() => { this.BtnNFC_Click(new object(), new RoutedEventArgs()); }); }
            if (_method == PaymentMethod.None) { this.Dispatcher.Invoke(() => { this.BtnNFC_Click(btnCard, new RoutedEventArgs()); }); }
        }

        private void OnDrawerClosed(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() => { this.Close(); });
        }

        private void UcInputCalc_AmountInputDeleted(object sender, EventArgs e)
        {
            _refund = 0;
            _amount = _transaction.Amount - _transaction.AmountPaid;
            txtAmount.Text = _amount.ToString();
        }

        private void UcInputCalc_AmountInputChanged(object sender, EventArgs e)
        {
            _amount = ucInputCalc.Amount();
            txtAmount.Text = _amount.ToString();
            _refund = 0;
            if (_amount <= 0)
            {
                _refund = 0;
                btnPay.IsEnabled = false;
                _method = PaymentMethod.None;
            }
            if (_amount > _transaction.Amount - _transaction.AmountPaid)
            {
                _refund = _amount - (_transaction.Amount - _transaction.AmountPaid);
                _amount = _transaction.Amount - _transaction.AmountPaid;
            }
        }

        private void BtnCash_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();
            if (_method != BLL.PaymentMethod.Cash && _amount > 0)
            {
                _method = BLL.PaymentMethod.Cash;
                btnPay.IsEnabled = true;
                var involved = sender as Button;
                involved.Background = (SolidColorBrush)Application.Current.Resources["Brush_Selected_Secondary"];
                return;
            }
            _method = BLL.PaymentMethod.None;
            btnPay.IsEnabled = false;
        }

        private void BtnPayconic_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();
            if (_method != BLL.PaymentMethod.Payconiq && _amount > 0)
            {
                _method = BLL.PaymentMethod.Payconiq;
                btnPay.IsEnabled = true;
                var involved = sender as Button;
                involved.Background = (SolidColorBrush)Application.Current.Resources["Brush_Selected_Secondary"];
                return;
            }
            _method = BLL.PaymentMethod.None;
            btnPay.IsEnabled = false;
        }

        private void BtnPay_Click(object sender, RoutedEventArgs e)
        {
            _cancel = false;
            if (_method == PaymentMethod.Cash)
            {
                lblRefundAmount.Content = "Refund Amount: € " + _refund.ToString();
                lblRefundAmount.Visibility = Visibility.Visible;
                lblInfo.Visibility = Visibility.Visible;
                _CD.Open();
            }
            if (_method == PaymentMethod.Payconiq)
            {
                //foreach  (Product p in _transaction.Products)
                //{
                //    if(p.Id == 46)
                //    {
                //        lblInfo.Visibility = Visibility.Visible;
                //        _CD.Open();
                //        return;
                //    }
                //}
                this.Close();
            }
            if (_method == PaymentMethod.Voucher)
            {
                foreach (Product p in _transaction.Products)
                {
                    if (p.Id == 46)
                    {
                        lblInfo.Content = "Can't pay Drankkaart with Drankkaart";
                        lblInfo.Visibility = Visibility.Visible;
                        return;
                    }
                }
                this.Close();
            }
            if (_method == PaymentMethod.MemberCard)
            {
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            _cancel = true;
            this.Close();
        }

        private void BtnVoucher_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();
            if (_method != BLL.PaymentMethod.Voucher && _amount > 0)
            {
                _method = BLL.PaymentMethod.Voucher;
                btnPay.IsEnabled = true;
                var involved = sender as Button;
                involved.Background = (SolidColorBrush)Application.Current.Resources["Brush_Selected_Secondary"];
                return;
            }
            _method = BLL.PaymentMethod.None;
            btnPay.IsEnabled = false;
        }
        private void BtnNFC_Click(object sender, RoutedEventArgs e)
        {
            ClearSelection();
            if (_method != BLL.PaymentMethod.MemberCard && _amount > 0)
            {
                if(_amount <= CustomerCard.Credit)
                {
                    _method = BLL.PaymentMethod.MemberCard;
                    btnPay.IsEnabled = true;
                    txtBalance.Foreground = Brushes.White;
                    var involved = sender as Button;
                    involved.Background = (SolidColorBrush)Application.Current.Resources["Brush_Selected_Secondary"];
                    return;
                }
                else
                {
                    lblInfo.Content = "Insufficient funds on customer card";
                    lblInfo.Visibility = Visibility.Visible;
                    txtBalance.Foreground = Brushes.Red;
                }
            }
            _method = BLL.PaymentMethod.None;
            btnPay.IsEnabled = false;
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _reader.OnAvailabilityChanged -= Card_Changed;
            _CD.Shutdown();
        }
        private void ClearSelection()
        {
            foreach (Button btn in wpPaybuttons.Children)
            {
                SolidColorBrush bg = new SolidColorBrush((Color)Application.Current.Resources["Color_ElevationOverlay"]);
                bg.Opacity = (double)Application.Current.Resources["Elevation_01dp"];
                btn.Background = bg;
            }
            lblInfo.Visibility = Visibility.Hidden;
        }

    }
}
