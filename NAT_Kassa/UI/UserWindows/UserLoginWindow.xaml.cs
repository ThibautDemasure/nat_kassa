﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for UserLoginWindow.xaml
    /// </summary>
    public partial class UserLoginWindow : Window
    {
        public UserLoginWindow(List<BLL.User> users)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.WindowState = WindowState.Maximized;
            foreach (BLL.User u in users)
            {
                if (u.IsActive)
                {
                    UserboxControl ub = new UserboxControl();
                    ub.Height = 50;
                    ub.Margin = new Thickness(4);
                    ub.HorizontalAlignment = HorizontalAlignment.Stretch;
                    ub.txtUserName.Text = u.Username;
                    ub.txtOrganization.Text = u.OrganizationID.ToString();
                    ub.MouseDown += Ub_MouseDown;
                    switch (u.OrganizationID)
                    {
                        case BLL.OrganizationID.Nautic:
                            lvUsersNautic.Children.Add(ub);
                            break;
                        case BLL.OrganizationID.Flac:
                            lvUsersFlac.Children.Add(ub);
                            break;
                        case BLL.OrganizationID.Tribes:
                            lvUsersTribes.Children.Add(ub);
                            break;
                        case BLL.OrganizationID.NAT:
                            lvUsersNAT.Children.Add(ub);
                            break;
                    }
                }
            }
            pwControl.btnEnter.Click += BtnEnter_Click;
        }

        private string _password = "";
        public string Password { get => _password; set => _password = value; }
        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            _password = pwControl.txtNumberDisplay.Password;
            this.Close();
        }

        private void Ub_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var ub = sender as UserboxControl;
            ubSelectedUser.txtUserName.Text = ub.txtUserName.Text;
            ubSelectedUser.txtOrganization.Text = ub.txtOrganization.Text;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            _password = "";
            this.Close();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            _password = pwControl.txtNumberDisplay.Password;
            this.Close();
        }

        #region Dragable ScrollViewer
        private double _initOffset = 0.0;
        private double _initmouse = 0.0;
        private double _distance = 0.0;

        private void Scroll_Started(object sender, MouseButtonEventArgs e)
        {
            StackPanel lv = sender as StackPanel;
            ScrollViewer scrollvwr = lv.Parent as ScrollViewer;
            _initmouse = Mouse.GetPosition(scrollvwr).Y;
            _initOffset = scrollvwr.VerticalOffset;
            lv.MouseMove += Scroll_MouseMove;
            
        }

        private void Scroll_MouseMove(object sender, MouseEventArgs e)
        {
            StackPanel lv = sender as StackPanel;
            ScrollViewer scrollvwr = lv.Parent as ScrollViewer;
            _distance = _initmouse - Mouse.GetPosition(scrollvwr).Y;
            scrollvwr.ScrollToVerticalOffset(_initOffset + _distance);
            
        }

        private void Scroll_Done(object sender, MouseButtonEventArgs e)
        {
            StackPanel lv = sender as StackPanel;
            ScrollViewer scrollvwr = lv.Parent as ScrollViewer;
            lv.MouseMove -= Scroll_MouseMove;  
        }

        private void Scroll_Abort(object sender, MouseEventArgs e)
        {
            StackPanel lv = sender as StackPanel;
            ScrollViewer scrollvwr = lv.Parent as ScrollViewer;
            lv.MouseMove -= Scroll_MouseMove;
        }
        #endregion
    }
    public class HalveWidthValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //switch (value.ToString().ToLower())
            //{
            //    case "yes":
            //    case "oui":
            //        return true;
            //    case "no":
            //    case "non":
            //        return false;
            //}
            //return false;
            return (double)value - 310;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //if (value is bool)
            //{
            //    if ((bool)value == true)
            //        return "yes";
            //    else
            //        return "no";
            //}
            //return "no";
            return (double)value + 310;
        }
    }
}
