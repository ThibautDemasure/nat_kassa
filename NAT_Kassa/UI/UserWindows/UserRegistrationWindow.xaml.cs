﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for UserRegistrationWindow.xaml
    /// </summary>
    public partial class UserRegistrationWindow : Window
    {
        public UserRegistrationWindow(BLL.UserModule um, bool edit)
        {
            InitializeComponent();
            List<BLL.Userlevel> displayUserLevels = new List<BLL.Userlevel>(Enum.GetValues(typeof(BLL.Userlevel)).Cast<BLL.Userlevel>());
            displayUserLevels.RemoveAt(displayUserLevels.Count - 1);
            if (um.CurrentUser.Userlevel <= Userlevel.Guest) { displayUserLevels.RemoveAt(2); }
            displayUserLevels.RemoveAt(0);
            //cmbUL.ItemsSource = Enum.GetValues(typeof(BLL.Userlevel)).Cast<BLL.Userlevel>();
            cmbUL.ItemsSource = displayUserLevels;
            cmbUL.SelectedIndex = 0;
            List<BLL.OrganizationID> dpOrg = new List<OrganizationID>(Enum.GetValues(typeof(BLL.OrganizationID)).Cast<BLL.OrganizationID>());
            dpOrg.RemoveAt(0);
            cmbOrg.ItemsSource = dpOrg;

            _um = um;
            ppKeyboard.ButtonPressed += PpKeyboard_ButtonPressed;
            ppKeyboard.EnterPressed += PpKeyboard_EnterPressed;
            ppKeyboard.ShowNumbersPressed += PpKeyboard_ShowNumbersPressed;

            if (edit)
            {
                this.Title = "Edit user";
                btnAdd.Visibility = Visibility.Collapsed;
                btnAdd.IsEnabled = false;
                btnEdit.Visibility = Visibility.Visible;
                txtPwOld.Visibility = Visibility.Visible;
                lblOldPincode.Visibility = Visibility.Visible;
                lblInvalidOldpin.Visibility = Visibility.Visible;
                if (_um.CurrentUser != null)
                {
                    txtName.Text = _um.CurrentUser.Name;
                    txtSurname.Text = _um.CurrentUser.Surname;
                    txtEmail.Text = _um.CurrentUser.Email;
                    txtAddress.Text = _um.CurrentUser.Address;
                    cmbOrg.SelectedIndex = (int)_um.CurrentUser.OrganizationID - 1;
                    cmbUL.SelectedIndex = (int)_um.CurrentUser.Userlevel - 1;
                }
                else
                {
                    this.Close();
                }

            }
        }
        private BLL.User _newuser;
        private BLL.UserModule _um;
        private TextBox txtFocussed;
        private PasswordBox pwbFocussed;
        public User NewUser { get => _newuser; set => _newuser = value; }
        public bool Cancel { get => _cancel;}

        private bool _cancel = true;
        //popup keyboard eventhandlers
        private void PpKeyboard_ShowNumbersPressed(object sender, EventArgs e)
        {
            if (ppKeyboard.IsNumbers)
            {
                KeyboardDock.Height = 100;
            }
            if (ppKeyboard.IsNumbers == false)
            {
                KeyboardDock.Height = 200;
            }
        }
        private void PpKeyboard_EnterPressed(object sender, EventArgs e)
        {
            ppKeyboard.Visibility = Visibility.Collapsed;
        }
        private void PpKeyboard_ButtonPressed(object sender, EventArgs e)
        {
            if (txtFocussed != null)
            {
                txtFocussed.Text = ppKeyboard.Input;
                txtFocussed.CaretIndex = txtFocussed.Text.Length;
                txtFocussed.Focus();
            }
            if (pwbFocussed != null)
            {
                pwbFocussed.Password = ppKeyboard.Input;
                
            }

        }


        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            bool hasformaterrors = false;
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                lblInvalidName.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (string.IsNullOrWhiteSpace(txtSurname.Text))
            {
                lblInvalidSName.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (IsValidEmail(txtEmail.Text) == false)
            {
                lblInvalidEmail.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (txtPw.Password.Length < 4 || txtPw.Password.Length > 8)
            {
                lblInvalidPw.Foreground = new SolidColorBrush(Colors.Red);
                hasformaterrors = true;
            }
            else
            {
                try { int.Parse(txtPw.Password); }
                catch (FormatException fe) { lblInvalidPw.Foreground = new SolidColorBrush(Colors.Red); hasformaterrors = true; }
            }
            if (cmbOrg.SelectedIndex == -1) { lblInvalidClub.Visibility = Visibility.Visible;   hasformaterrors = true; }
            if(cmbUL.SelectedIndex == -1) { cmbUL.SelectedIndex = 0; }
            if (hasformaterrors == false)
            {
                _um.Register(txtName.Text, txtSurname.Text, txtEmail.Text, _um.HashPassword(txtPw.Password),
                    (BLL.OrganizationID)(cmbOrg.SelectedIndex + 1), (BLL.Userlevel)(cmbUL.SelectedIndex + 1),
                    txtAddress.Text);
                _cancel = false;
                this.Close();
            }

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            bool hasformaterrors = false;
            if (string.IsNullOrWhiteSpace(txtName.Text))
            {
                lblInvalidName.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (string.IsNullOrWhiteSpace(txtSurname.Text))
            {
                lblInvalidSName.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (IsValidEmail(txtEmail.Text) == false)
            {
                lblInvalidEmail.Visibility = Visibility.Visible;
                hasformaterrors = true;
            }
            if (txtPw.Password.Length < 4 || txtPw.Password.Length > 8)
            {
                lblInvalidPw.Foreground = new SolidColorBrush(Colors.Red);
                hasformaterrors = true;
            }
            else
            {
                try { int.Parse(txtPw.Password); }
                catch (FormatException fe) { lblInvalidPw.Foreground = new SolidColorBrush(Colors.Red); hasformaterrors = true; }
            }
            if (cmbOrg.SelectedIndex == -1) { lblInvalidClub.Visibility = Visibility.Visible; hasformaterrors = true; }
            if (cmbUL.SelectedIndex == -1) { cmbUL.SelectedIndex = 0; }
            if (_um.HashPassword(txtPwOld.Password) != _um.CurrentUser.Password)
            {
                lblInvalidOldpin.Foreground = new SolidColorBrush(Colors.Red);
                lblInvalidOldpin.Content = "Wrong pincode";
                hasformaterrors = true;
            }

            if (hasformaterrors == false)
            {
                User newu = new User(_um.CurrentUser.Id, txtName.Text, txtSurname.Text, txtAddress.Text, txtEmail.Text
                    , (BLL.OrganizationID)(cmbOrg.SelectedIndex + 1), (BLL.Userlevel)(cmbUL.SelectedIndex + 1), _um.HashPassword(txtPw.Password));
                if (_um.EditUser(newu))
                {
                    _cancel = false;
                    this.Close();
                }
                else
                {
                    lblInfo.Text = _um.Notifications.Last();
                    lblInfo.Visibility = Visibility.Visible;
                } 
            }
        }

        //Validate email
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private void CmbOrg_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbOrg.SelectedIndex != -1)
            {
                lblInvalidClub.Visibility = Visibility.Hidden;
            }
            txtFocussed = null;
            pwbFocussed = null;
            ppKeyboard.Visibility = Visibility.Collapsed;
        }

        private void TextElementGotFocus(object sender, RoutedEventArgs e)
        {
            if (txtFocussed != (sender as TextBox))
            {
                txtFocussed = sender as TextBox;
                pwbFocussed = null;
                ppKeyboard.Input = txtFocussed.Text;
                ppKeyboard.Visibility = Visibility.Visible;
                ClearTextErrors();
            } 
        }

        private void ClearTextErrors()
        {
            lblInvalidName.Visibility = Visibility.Hidden;
            lblInvalidSName.Visibility = Visibility.Hidden;
            lblInvalidEmail.Visibility = Visibility.Hidden;
        }
        private void PasswordGotFocus(object sender, RoutedEventArgs e)
        {
            pwbFocussed = sender as PasswordBox;
            txtFocussed = null;
            ppKeyboard.ActivateNumbers();
            ppKeyboard.Input = pwbFocussed.Password;
            ppKeyboard.Visibility = Visibility.Visible;
            lblInvalidPw.Foreground = new SolidColorBrush(Colors.White);
            lblInvalidOldpin.Foreground = new SolidColorBrush(Colors.White);
            lblInvalidOldpin.Content = "Validate by old pincode";
        }

        private void Other_GotFocus(object sender, RoutedEventArgs e)
        {
            txtFocussed = null;
            pwbFocussed = null;
            ppKeyboard.Visibility = Visibility.Collapsed;
            ResetErrorNotifications();
            
        }

        private void ResetErrorNotifications()
        {
            lblInvalidClub.Visibility = Visibility.Hidden;
            //lblInvalidEmail.Visibility = Visibility.Hidden;
            //lblInvalidName.Visibility = Visibility.Hidden;
            //lblInvalidOldpin.Content = "Validate by old pincode";
            //lblInvalidOldpin.Foreground = new SolidColorBrush(Colors.White);
            //lblInvalidPw.Foreground = new SolidColorBrush(Colors.White);
            //lblInvalidSName.Visibility = Visibility.Hidden;
            //lblInfo.Visibility = Visibility.Hidden;
        }

 
    }
}
