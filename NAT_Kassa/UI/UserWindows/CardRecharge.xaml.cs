﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NAT_Kassa.BLL;
namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for NewCardPopup.xaml
    /// </summary>
    public partial class CardRecharge : Window
    {
        public bool IsCancel { get; set; }
        private bool _isEdit;
        public string OwnerName { get; set; }
        public string OwnerSurname { get; set; }
        public string UID { get; set; }
        public double Balance { get; set; }
        public double Amount { get; set; }
        public double Payed { get; set; }
        private double ClickedAmount;
        public PaymentMethod Method {get; set;}
        public CardRecharge(Card card, bool isEdit)
        {
            InitializeComponent();
            txtBalance.Text = card.Credit.ToString("F2");
            txtName.Text = card.Name;
            txtSurname.Text = card.SurName;
            txtUID.Text = card.UID;
            txtAmount.Background = Brushes.Transparent;
            if (card.IsValid) { imgValid.Visibility = Visibility.Visible; }
            IsCancel = true;
            _isEdit = isEdit;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            //txtAmountPlaceHolder.Visibility = tb.Text == "" ? Visibility.Visible : Visibility.Hidden;
            tb.Background = tb.Text == "" ? Brushes.Transparent : Brushes.White;
            tb.Foreground = tb.Text.Length > tb.MaxLength ? Brushes.Red : Brushes.Black;
            if (double.TryParse(tb.Text, out double topay))
            {
                CalculateAmount(topay);
                tb.Foreground = Brushes.Black;
            }
            else
            {
                CalculateAmount(ClickedAmount);
                tb.Foreground = Brushes.Red;
                //txtInfo.Text = "";
            }
        }
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            IsCancel = true;
            this.Close();
        }
        private void Recharge_Click(object sender, RoutedEventArgs e)
        {
            if (Amount <= 0)
            {
                txtError.Text = "Geen bedrag geselecteerd.";
                return;
            }
            if (Method <= 0)
            {
                txtError.Text = "Geen betaalmethode geselecteerd";
                return;
            }
            IsCancel = false;
            this.Close();
        }
        private void CalculateAmount(double tebetalen)
        {
            if (_isEdit) { Amount = tebetalen; }
            else { Amount = tebetalen + Math.Floor(tebetalen / 20) * 2.5; }
            Payed = tebetalen;
            txtInfo.Text = Amount + " wordt opgeladen. " + Payed + " te betalen.";
            txtError.Text = "";
        }
        private void Amount_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ToggleButton;
            string content = btn.Content.ToString();
            //string space = " ";
            //string[] splits = content.Split(space.ToCharArray());
            ClickedAmount = (bool)btn.IsChecked ? double.Parse(content) : 0;
            CalculateAmount(ClickedAmount);
            foreach (ToggleButton tb in wpAmount.Children)
            {
                if (tb.IsChecked.HasValue && (bool)tb.IsChecked && tb != btn)
                {
                    tb.IsChecked = false;
                }
            }
        }
        private void Method_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ToggleButton;
            string content = btn.Content.ToString();
            Method = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), btn.Content.ToString());
            txtError.Text = "";
            foreach (ToggleButton tb in wpMethod.Children )
            {
                if (tb.IsChecked.HasValue && (bool)tb.IsChecked && tb != btn)
                {
                    tb.IsChecked = false;
                }
            }
        }
    }
}
