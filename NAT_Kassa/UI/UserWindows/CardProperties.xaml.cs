﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for NewCardPopup.xaml
    /// </summary>
    public partial class CardProperties : Window
    {
        public bool IsCancel {get; set;}
        public string OwnerName { get; set; }
        public string OwnerSurname { get; set; }
        public string UID { get; set; }
        public double Balance { get; set; }
        public CardProperties(Card card)
        {
            InitializeComponent();
            txtBalance.Text = card.Credit.ToString("F2");
            txtName.Text = card.Name;
            txtSurname.Text = card.SurName;
            txtUID.Text = card.UID;
            if (card.IsValid) { imgValid.Visibility = Visibility.Visible; }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb.Text.Length > tb.MaxLength)
            {
                tb.Foreground = Brushes.Red;
            }
            else
            {
                tb.Foreground = Brushes.Black;
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            IsCancel = true;
            this.Close();
        }
    }
}
