﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for EditProducts.xaml
    /// </summary>
    public partial class EditProducts : Window
    {
        private ProductsModule _PM;
        public EditProducts(ProductsModule pm)
        {
            InitializeComponent();
            _PM = pm;
            lstProducts.ItemsSource = _PM.Productpool;
            List<ProductCategory> displayvals = new List<ProductCategory>(Enum.GetValues(typeof(ProductCategory)).Cast<ProductCategory>());
            displayvals.RemoveAt(displayvals.Count - 1);
            cmbCategory.ItemsSource = displayvals;
            lblInfo.Text = "Choose product to edit in the left list.";
        }

        private void LstProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lstProducts.SelectedIndex >= 0)
            {
                Product p = _PM.Productpool[lstProducts.SelectedIndex];
                txtName.Text = p.Name;
                txtPrice.Text = p.Price.ToString();
                txtPurchasePrice.Text = p.Purchaseprice.ToString();
                cmbCategory.SelectedIndex = (int)p.Category;
                txtPrice.Background = new SolidColorBrush(Colors.White);
                txtPurchasePrice.Background = new SolidColorBrush(Colors.White);
                btnEdit.Background = (SolidColorBrush)Application.Current.Resources["Brush_Elevation_03dp"];
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            bool haserror = false;
            if (lstProducts.SelectedIndex < 0) { lblInfo.Text = "Choose product to change first."; haserror = true; }
            if (! double.TryParse(txtPrice.Text, out double price))
            {
                txtPrice.Background = new SolidColorBrush(Colors.Red);
                haserror = true;
            }
            else {txtPrice.Background = new SolidColorBrush(Colors.White);}
            if (! double.TryParse(txtPurchasePrice.Text, out double pprice))
            {
                txtPurchasePrice.Background = new SolidColorBrush(Colors.Red);
                haserror = true;
            }
            else { txtPurchasePrice.Background = new SolidColorBrush(Colors.White); }
            if (!haserror)
            {
                Product changedp = new Product(txtName.Text, price, pprice, (ProductCategory)cmbCategory.SelectedIndex)
                {
                    Id = _PM.Productpool[lstProducts.SelectedIndex].Id,
                    IsActive = _PM.Productpool[lstProducts.SelectedIndex].IsActive,
                    IsSynchronized = _PM.Productpool[lstProducts.SelectedIndex].IsSynchronized,
                    Quantity = _PM.Productpool[lstProducts.SelectedIndex].Quantity,
                    Warninglevel = _PM.Productpool[lstProducts.SelectedIndex].Warninglevel
                };
                ProductCategory oldcat = _PM.Productpool[lstProducts.SelectedIndex].Category;
                if (_PM.EditProduct(changedp, oldcat))
                {
                    _PM.Sort(ComparerType.NAME_ASC);
                    lstProducts.SelectedIndex = -1;
                    lstProducts.ItemsSource = null;
                    lstProducts.ItemsSource = _PM.Productpool;
                    lblInfo.Text = _PM.Notifications.Last();
                    btnEdit.Background = (SolidColorBrush)Application.Current.Resources["Brush_SolidGreen"];
                    return;
                }
                lblInfo.Text = _PM.Notifications.Last();
                btnEdit.Background = (SolidColorBrush)Application.Current.Resources["Brush_SolidRed"];
            }
            else
            {
                btnEdit.Background = (SolidColorBrush)Application.Current.Resources["Brush_SolidRed"];
            }
            
        }
    }
}
