﻿using NAT_Kassa.BLL;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Net.Http;

namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for InventoryWindow.xaml
    /// </summary>
    public partial class InventoryWindow : Window
    {
        private PrintDocument _doc;
        private InventoryModule _IM;
        private double _initOffset = 0.0;
        private double _initmouse = 0.0;
        private double _distance = 0.0;
        private double _itemheight = 1.0;
        private InventoryTransaction _selectedTransaction;
        private User _currentUser;
        public InventoryWindow(User currentUser)
        {
            InitializeComponent();
            _IM = new InventoryModule();
            _currentUser = currentUser;
            foreach (InventoryProduct item in _IM.LoadInventory)
            {
                UI.UserControls.ProductSurface prd = new UserControls.ProductSurface(item,0);
                spProductList.Children.Add(prd);
                _itemheight = 30;
            }
            cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            int newId = 1;
            if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
            _selectedTransaction = new InventoryTransaction()
            {
                Id = newId, NamedType = "New", Type = _IM.TypeDictionary["New"]
            };
            cmbFilter.Items.Add("All");
            foreach (string item in _IM.TypeDictionary.Keys)
            {
                if (item != "New")
                {
                    cmbFilter.Items.Add(item);
                }
            }
            //cmbFilter.SelectedIndex = 0;
            _doc = new PrintDocument();
            _doc.PrinterSettings.PrinterName = "Microsoft Print to PDF";
            _doc.PrintPage += _doc_PrintPage;
        }

        private void _doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            float colwidth = (e.PageBounds.Width - 50) / 5;
            float line = 20;
            float left = 25;
            float product = left;
            float qty = product + 2* colwidth;
            float unitprice = qty + colwidth;
            float packedqty = qty + 2* colwidth;
            Font myfont = new Font("Consolas", 13);
            Font empfont = new Font("Consolas", 16);

            //header
            RectangleF imagebox = new RectangleF(left, line, colwidth, colwidth);
            e.Graphics.DrawImage(Properties.Resources.Nautic_Logo, imagebox);
            imagebox.X = left + 2 * colwidth;
            e.Graphics.DrawImage(Properties.Resources.Flac_Logo, imagebox);
            imagebox.X = left + 4 * colwidth;
            e.Graphics.DrawImage(Properties.Resources.Tribes_Logo_kleur, imagebox);
            line += colwidth + 5;
            line += 40;
            if (_doc.DocumentName == "PurchaseOrder")
            {
                e.Graphics.DrawString("PO nr. " + _selectedTransaction.Id.ToString(), myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString("Delivery Note:", myfont, System.Drawing.Brushes.Black, qty, line);
                line += 20;
                e.Graphics.DrawString(_selectedTransaction.Timestamp.ToString(), myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString(_selectedTransaction.DeliveryNote, myfont, System.Drawing.Brushes.Black, qty, line);
                line += 20;
                e.Graphics.DrawString(_selectedTransaction.Remark, myfont, System.Drawing.Brushes.Black, product, line);
                line += 50;
                e.Graphics.DrawString("Producten", myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString("Bestelhoeveelheid (# bakken)", myfont, System.Drawing.Brushes.Black, qty, line);
                e.Graphics.DrawString("Aantal per bak", myfont, System.Drawing.Brushes.Black, packedqty, line);
                line += 20;
                e.Graphics.DrawLine(new System.Drawing.Pen(System.Drawing.SystemColors.ControlDark, 2), left, line, e.PageBounds.Width - 25, line);
                line += 5;
                foreach (UserControls.ProductSurface item in spProductList.Children)
                {
                    e.Graphics.DrawString(item.Product.Name, myfont, System.Drawing.Brushes.Black, product, line);
                    e.Graphics.DrawString(item.Product.OrderQ.ToString(), myfont, System.Drawing.Brushes.Black, qty, line);
                    e.Graphics.DrawString(item.Product.PackedQuantity.ToString(), myfont, System.Drawing.Brushes.Black, packedqty, line);
                    line += 20;
                }
            }
            if (_doc.DocumentName == "Issue")
            {
                e.Graphics.DrawString("Issue nr. " + _selectedTransaction.Id.ToString(), myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString("Remark:", myfont, System.Drawing.Brushes.Black, qty, line);
                line += 20;
                e.Graphics.DrawString(_selectedTransaction.Timestamp.ToString(), myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString(_selectedTransaction.Remark, myfont, System.Drawing.Brushes.Black, qty, line);
                line += 20;
                e.Graphics.DrawString(_selectedTransaction.DeliveryNote, myfont, System.Drawing.Brushes.Black, product, line);
                line += 50;
                e.Graphics.DrawString("Producten", myfont, System.Drawing.Brushes.Black, product, line);
                e.Graphics.DrawString("Hoeveelheid (#stuks)", myfont, System.Drawing.Brushes.Black, qty, line);
                e.Graphics.DrawString("EenheidsPrijs (€)", myfont, System.Drawing.Brushes.Black, unitprice, line);
                e.Graphics.DrawString("Prijs (€)", myfont, System.Drawing.Brushes.Black, packedqty, line);
                line += 20;
                e.Graphics.DrawLine(new System.Drawing.Pen(System.Drawing.SystemColors.ControlDark, 2), left, line, e.PageBounds.Width - 25, line);
                line += 5;
                double totaal = 0;
                foreach (UserControls.ProductSurface item in spProductList.Children)
                {
                    e.Graphics.DrawString(item.Product.Name, myfont, System.Drawing.Brushes.Black, product, line);
                    e.Graphics.DrawString(item.Product.Quantity.ToString(), myfont, System.Drawing.Brushes.Black, qty, line);
                    e.Graphics.DrawString(item.Product.Price.ToString(), myfont, System.Drawing.Brushes.Black, unitprice, line);
                    totaal += item.Product.Quantity * item.Product.Price;
                    e.Graphics.DrawString((item.Product.Price * item.Product.Quantity).ToString(), myfont, System.Drawing.Brushes.Black, packedqty, line);
                    line += 20;
                }
                e.Graphics.DrawLine(new System.Drawing.Pen(System.Drawing.SystemColors.ControlDark, 2), left, line, e.PageBounds.Width - 25, line);
                line += 5;
                e.Graphics.DrawString("Totaal", empfont, System.Drawing.Brushes.Black, qty, line);
                e.Graphics.DrawString(totaal.ToString("F2"), empfont, System.Drawing.Brushes.Black, packedqty, line);
                line += 20;
                e.Graphics.DrawString("Korting: 15% ", myfont, System.Drawing.Brushes.Black, qty, line);
                e.Graphics.DrawString((totaal * 0.15).ToString("F2"), myfont, System.Drawing.Brushes.Black, unitprice, line);
                line += 20;
                e.Graphics.DrawString("Te Betalen", empfont, System.Drawing.Brushes.Black, qty, line);
                e.Graphics.DrawString((totaal * 0.85).ToString("F2"), empfont, System.Drawing.Brushes.Black, packedqty, line);

            }
            
        }

        #region Dragable ScrollViewer

        private void spProductList_MouseDown(object sender, MouseButtonEventArgs e)
        {   
            _initmouse = Mouse.GetPosition(scrollvwr).Y;
            _initOffset = spProductList.VerticalOffset;
            spProductList.MouseMove += SpProductList_MouseMove;
        }

        private void SpProductList_MouseMove(object sender, MouseEventArgs e)
        {
            
            _distance = _initmouse - Mouse.GetPosition(scrollvwr).Y;
            spProductList.SetVerticalOffset(_initOffset + _distance / _itemheight);
        }

        private void spProductList_MouseUp(object sender, MouseButtonEventArgs e)
        {
            
            spProductList.MouseMove -= SpProductList_MouseMove;
        }

        private void spProductList_MouseLeave(object sender, MouseEventArgs e)
        {
            
            spProductList.MouseMove -= SpProductList_MouseMove;
        }
        #endregion


        private void cmbInventoryTransactions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            spProductList.Children.Clear();
            if (cmbInventoryTransactions.SelectedItem != null)
            {
                InventoryTransaction tr = (InventoryTransaction)cmbInventoryTransactions.SelectedItem;
                foreach (InventoryProduct item in _IM.SelectTransaction(tr.Id))
                {
                    UI.UserControls.ProductSurface prd = new UserControls.ProductSurface(item,tr.Type);
                    spProductList.Children.Add(prd);
                    _itemheight = 30;
                }
                txtDeliveryNote.Text = tr.DeliveryNote;
                txtRemark.Text = tr.Remark;
                _selectedTransaction = (InventoryTransaction)cmbInventoryTransactions.SelectedItem;
                txtSelectTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_SecundarySurface"];
                btnNewInventoryTransaction.BorderBrush = (SolidColorBrush)Application.Current.Resources["Brush_OnDarkurface"];
                btnNewInventoryTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_OnDarkSurface"];
            }
            else
            {
                btnNewInventoryTransaction.BorderBrush = (SolidColorBrush)Application.Current.Resources["Brush_SecundarySurface"];
                btnNewInventoryTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_SecundarySurface"];
                txtSelectTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_OnDarkSurface"];
                foreach (InventoryProduct item in _IM.LoadInventory)
                {
                    UI.UserControls.ProductSurface prd = new UserControls.ProductSurface(item,0);
                    spProductList.Children.Add(prd);
                    _itemheight = 30;
                }
                txtDeliveryNote.Text = "";
                txtRemark.Text = "";
                int newId = 1;
                if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
                _selectedTransaction = new InventoryTransaction()
                {
                    Id = newId,
                    Type = _IM.TypeDictionary["New"],
                    NamedType = "New"
                };
            }
            SetActions();
        }
        private void SetActions()
        {
            switch (_selectedTransaction.Type)
            {
                case 0:
                    spActions.IsEnabled = true;
                    btnInventoryCount.IsEnabled = true;
                    btnIssueTransaction.IsEnabled = true;
                    btnUpdateProducts.IsEnabled = true;
                    btnReceiveOrder.IsEnabled = false;
                    btnOrderTemplate.IsEnabled = true;
                    btnPurchaseOrder.IsEnabled = true;
                    hdrQuantity.Text = "Current Quantity";
                    break;
                case 1:
                    hdrQuantity.Text = "Counted Quantity";
                    spActions.IsEnabled = false;
                    break;
                case 2:
                    hdrQuantity.Text = "Received Quantity";
                    spActions.IsEnabled = false;
                    break;
                case 3:
                    hdrQuantity.Text = "Issued Quantity";
                    spActions.IsEnabled = false;
                    break;
                case 4:
                    hdrQuantity.Text = "Adjusted Quantity";
                    spActions.IsEnabled = false;
                    break;
                case 5:
                    spActions.IsEnabled = true;
                    btnInventoryCount.IsEnabled = false;
                    btnIssueTransaction.IsEnabled = false;
                    btnUpdateProducts.IsEnabled = false;
                    btnReceiveOrder.IsEnabled = true;
                    btnOrderTemplate.IsEnabled = false;
                    btnPurchaseOrder.IsEnabled = false;
                    hdrQuantity.Text = "Ordered Quantity";
                    break;
                case 6:
                    spActions.IsEnabled = true;
                    btnInventoryCount.IsEnabled = false;
                    btnIssueTransaction.IsEnabled = false;
                    btnUpdateProducts.IsEnabled = false;
                    btnReceiveOrder.IsEnabled = false;
                    btnOrderTemplate.IsEnabled = false;
                    btnPurchaseOrder.IsEnabled = true;
                    hdrQuantity.Text = "Order Quantity";
                    break;
                default:
                    spActions.IsEnabled = false;
                    hdrQuantity.Text = "Current Quantity";
                    break;
            }
        }
        private void btnNewInventoryTransaction_Click(object sender, RoutedEventArgs e)
        {
           btnNewInventoryTransaction.BorderBrush = (SolidColorBrush)Application.Current.Resources["Brush_SecundarySurface"];
           btnNewInventoryTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_SecundarySurface"];
            txtSelectTransaction.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_OnDarkSurface"];
            if (cmbInventoryTransactions.SelectedIndex != -1) { cmbInventoryTransactions.SelectedIndex = -1; }
            else {
                spProductList.Children.Clear();
                foreach (InventoryProduct item in _IM.LoadInventory)
                {
                    UI.UserControls.ProductSurface prd = new UserControls.ProductSurface(item, 0);
                    spProductList.Children.Add(prd);
                    _itemheight = 30;
                }
                txtDeliveryNote.Text = "";
                txtRemark.Text = "";
                int newId = 1;
                if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
                _selectedTransaction = new InventoryTransaction()
                {
                    Id = newId,
                    NamedType = "New",
                    Type = _IM.TypeDictionary["New"]
                };
                SetActions();
            }
           
           

        }

        private void btnInventoryCount_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Adding an inventory count will overwrite the current quantity of each activated product with the inputted quantity values. Are you sure to input the inventory count?", "Update current inventory", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                int newId = 1;
                if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
                _selectedTransaction = new InventoryTransaction()
                {
                    Id = newId,
                    NamedType ="Count",
                    Type = _IM.TypeDictionary["Count"],
                    Remark = txtRemark.Text,
                    DeliveryNote = "",
                    Timestamp = System.DateTime.Now,
                    UserName = _currentUser.Username
                };
                _IM.UpdateCurrentQuantity(_selectedTransaction);
                _IM.Inventorytransactions.Add(_selectedTransaction);
                cmbInventoryTransactions.ItemsSource = null;
                cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
                cmbInventoryTransactions.SelectedIndex = _IM.Inventorytransactions.Count - 1;
                SetActions();
            } 
        }

        private void btnReceiveOrder_Click(object sender, RoutedEventArgs e)
        {
            int newId = 1;
            if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
            _selectedTransaction = new InventoryTransaction()
            {
                Id = newId,
                Type = _IM.TypeDictionary["Receipt"],
                NamedType = "Receipt",
                Remark = txtRemark.Text,
                DeliveryNote = txtDeliveryNote.Text,
                Timestamp = System.DateTime.Now,
                UserName = _currentUser.Username
            };
            foreach  (UserControls.ProductSurface item in spProductList.Children)
            {
                _IM.ReceiveOrderProduct(_selectedTransaction,item.Product);
            }
            
            _IM.Inventorytransactions.Add(_selectedTransaction);
            cmbInventoryTransactions.ItemsSource = null;
            cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            cmbInventoryTransactions.SelectedIndex = _IM.Inventorytransactions.Count - 1;
            SetActions();
        }

        private void btnPurchaseOrder_Click(object sender, RoutedEventArgs e)
        {
            int newId = 1;
            if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
            _selectedTransaction = new InventoryTransaction()
            {
                Id = newId,
                NamedType = "PurchaseOrder",
                Type = _IM.TypeDictionary["PurchaseOrder"],
                Remark = txtRemark.Text,
                DeliveryNote = txtDeliveryNote.Text,
                Timestamp = System.DateTime.Now,
                UserName = _currentUser.Username
            };
            foreach (UserControls.ProductSurface item in spProductList.Children)
            {
                _IM.PurchaseOrderProduct(_selectedTransaction,item.Product);
            }
            _IM.Inventorytransactions.Add(_selectedTransaction);
            cmbInventoryTransactions.ItemsSource = null;
            cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            cmbInventoryTransactions.SelectedIndex = _IM.Inventorytransactions.Count - 1;
            SetActions();
            _doc.DocumentName = "PurchaseOrder";
            _doc.Print();

        }

        private void btnOrderTemplate_Click(object sender, RoutedEventArgs e)
        {
            int newId = 1;
            if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
            _selectedTransaction = new InventoryTransaction()
            {
                Id = newId,
                NamedType = "OrderTemplate",
                Type = _IM.TypeDictionary["OrderTemplate"],
                Remark = txtRemark.Text,
                DeliveryNote = "",
                Timestamp = System.DateTime.Now,
                UserName = _currentUser.Username
            };
            foreach (UserControls.ProductSurface item in spProductList.Children)
            {
                _IM.PurchaseOrderProduct(_selectedTransaction, item.Product);
            }
            _IM.Inventorytransactions.Add(_selectedTransaction);
            cmbInventoryTransactions.ItemsSource = null;
            cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            cmbInventoryTransactions.SelectedIndex = _IM.Inventorytransactions.Count - 1;
            SetActions();
        }

        private void btnIssueTransaction_Click(object sender, RoutedEventArgs e)
        {
            int newId = 1;
            if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
            _selectedTransaction = new InventoryTransaction()
            {
                Id = newId,
                NamedType = "Issue",
                Type = _IM.TypeDictionary["Issue"],
                Remark = txtRemark.Text,
                DeliveryNote = "",
                Timestamp = System.DateTime.Now,
                UserName = _currentUser.Username
            };
            _IM.IssueTransactionProduct(_selectedTransaction);
            _IM.Inventorytransactions.Add(_selectedTransaction);
            cmbInventoryTransactions.ItemsSource = null;
            cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            cmbInventoryTransactions.SelectedIndex = _IM.Inventorytransactions.Count - 1;
            SetActions();
            _doc.DocumentName = "Issue";
            _doc.Print();
        }

        private void btnUpdateProducts_Click(object sender, RoutedEventArgs e)
        {
            if (_selectedTransaction.Type == 0)
            {
                _IM.UpdateDefaultOrderParameters();
                spProductList.Children.Clear();
                foreach (InventoryProduct item in _IM.LoadInventory)
                {
                    UI.UserControls.ProductSurface prd = new UserControls.ProductSurface(item, 0);
                    spProductList.Children.Add(prd);
                    _itemheight = 30;
                }
                txtDeliveryNote.Text = "";
                txtRemark.Text = "";
                int newId = 1;
                if (_IM.Inventorytransactions.Count > 0) { newId = _IM.Inventorytransactions.Last().Id + 1; }
                _selectedTransaction = new InventoryTransaction()
                {
                    Id = newId,
                    Type = _IM.TypeDictionary["New"],
                    NamedType = "New"
                };
                SetActions();
            }
            
        }

        private void btnToggleAll_Checked(object sender, RoutedEventArgs e)
        {
            if (spProductList != null)
            {
                foreach (UserControls.ProductSurface item in spProductList.Children)
                {
                    if (item.chbActive.IsEnabled) { item.chbActive.IsChecked = true; }
                }
            }
        }

        private void btnToggleAll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (spProductList != null)
            {
                foreach (UserControls.ProductSurface item in spProductList.Children)
                {
                    if (item.chbActive.IsEnabled) { item.chbActive.IsChecked = false; }
                }
            }
        }
        private List<InventoryProduct> _lastInventory;

        public List<InventoryProduct> LastInventory { get => _lastInventory;}

        private void Window_closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _lastInventory = _IM.LoadInventory;

        }

        private void cmbFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbFilter.SelectedIndex > 0)
            {
                cmbInventoryTransactions.ItemsSource = _IM.FilteredTransactions(cmbFilter.SelectedItem.ToString());
            }
            else
            {
                cmbInventoryTransactions.ItemsSource = _IM.Inventorytransactions;
            }
        }
    }
}
