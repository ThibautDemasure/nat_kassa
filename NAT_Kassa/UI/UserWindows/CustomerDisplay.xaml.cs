﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for CustomerDisplay.xaml
    /// </summary>
    public partial class CustomerDisplay : Window
    {
        private NfcReader _reader;
        private BLL.Transaction _selectedTransaction;
        private double _newAmount;
        public CustomerDisplay()
        {
            InitializeComponent();
        }
        public CustomerDisplay(NfcReader reader)
        {
            InitializeComponent();
            _reader = reader;
            _selectedTransaction = new BLL.Transaction(999, new BLL.User("No Selection"));
            TransactionDetailsCustomerControl tr = new TransactionDetailsCustomerControl(_selectedTransaction);
            spSelectedTransaction.Children.Add(tr);
            _reader.OnAvailabilityChanged += _reader_OnAvailabilityChanged;
        }
        public BLL.Transaction Transaction
        {
            get
            {
                return _selectedTransaction;
            }
            set
            {
                _selectedTransaction = value;
                spSelectedTransaction.Children.Clear();
                spSelectedTransaction.Children.Add(new TransactionDetailsCustomerControl(_selectedTransaction));
            }
        }

        public double NewAmount { get => _newAmount;
            set { _newAmount = value; txtCredit.Text = "€" + _newAmount.ToString("F2"); spCreditBg.Background = _reader.ConnectedCard.IsValid ? Brushes.DodgerBlue : Brushes.DarkRed; }
        }

        private void _reader_OnAvailabilityChanged(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() => {
                if (_reader.IsCardConnected)
                {
                    Brush br = _reader.ConnectedCard.IsValid ? Brushes.DodgerBlue : Brushes.DarkRed;
                    NewAmount = _reader.ConnectedCard.Credit;
                    spCreditBg.Background = br;
                    spCreditBg.Visibility = Visibility.Visible;
                   
                    //NotificationPopup p = new NotificationPopup(true,"NFC Card : " + _reader.ConnectedCard.Owner, $"Saldo = € {_reader.ConnectedCard.Credit}");
                    //.OnRemove += P_OnRemove;
                    //spPopups.Children.Add(p);
                }
                else
                {
                   spCreditBg.Visibility = Visibility.Hidden;
                }
            });
        }

        private void P_OnRemove(object sender, EventArgs e)
        {
            NotificationPopup p = sender as NotificationPopup;
            Dispatcher.Invoke(() => { spPopups.Children.Remove(p); });
        }
    }
}
