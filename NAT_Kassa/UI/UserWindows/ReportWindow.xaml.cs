﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        private BLL.ReportModule _rm;
  
        public ReportWindow(BLL.ReportModule RM)
        {
            InitializeComponent();
            _rm = RM;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (dtBegin.SelectedDate != null) {_rm.BeginDate = (DateTime)dtBegin.SelectedDate; }
            if (dtEnd.SelectedDate != null)
            {
                DateTime dt = (DateTime)dtEnd.SelectedDate;
                dt = dt.AddDays(1);
                dt = dt.Subtract(new TimeSpan(10));
                _rm.EndDate = dt;
            }
            _rm.ReloadTransactions();
            lblNotification.Content = _rm.Notifications.Last();
            lblAmountTotal.Content = "Totaal: " +  _rm.TotaleAmount;
            lblPAmountTotal.Content = "Purchase Tot: " + _rm.TotalePurchaseAmount;
            lblAmountT.Content = _rm.FilteredAmount(BLL.OrganizationID.Tribes);
            lblAmountN.Content = _rm.FilteredAmount(BLL.OrganizationID.Nautic);
            lblAmountF.Content = _rm.FilteredAmount(BLL.OrganizationID.Flac);
            lblPAmountT.Content = _rm.FilteredPurchaseAmount(BLL.OrganizationID.Tribes);
            lblPAmountN.Content = _rm.FilteredPurchaseAmount(BLL.OrganizationID.Nautic);
            lblPAmountF.Content = _rm.FilteredPurchaseAmount(BLL.OrganizationID.Flac);
            lstTransactions.ItemsSource = null;
            lstProducts.ItemsSource = null;
            lstPayments.ItemsSource = null;
            lstTransactions.ItemsSource = _rm.ClosedTransactionpool;
            lstProducts.ItemsSource = _rm.Products;
            lstPayments.ItemsSource = _rm.Payments;

        }

        private void BtnBook_Click(object sender, RoutedEventArgs e)
        {
            _rm.BookTransactions();
        }


    }
}
