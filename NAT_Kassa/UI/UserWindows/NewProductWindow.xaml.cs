﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for NewProductWindow.xaml
    /// </summary>
    public partial class NewProductWindow : Window
    {
        //fields
        private BLL.ProductCategory _category;
        private string _name;
        private double _price;
        private double _pprice;
        private int _packq;
        private int _initq;
        private bool _cancel = true;
        private TextBox txtFocussed;

        //constructor
        public NewProductWindow()
        {
            InitializeComponent();
            List<BLL.ProductCategory> displayvals = new List<BLL.ProductCategory>(Enum.GetValues(typeof(BLL.ProductCategory)).Cast<BLL.ProductCategory>());
            displayvals.RemoveAt(displayvals.Count - 1);
            cmbCat.ItemsSource = displayvals;
            ppKeyboard.ButtonPressed += PpKeyboard_ButtonPressed;
            ppKeyboard.EnterPressed += PpKeyboard_EnterPressed;
            ppKeyboard.ShowNumbersPressed += PpKeyboard_ShowNumbersPressed;
        }

        //properties
        public ProductCategory Category { get => _category; set => _category = value; }
        public string ProductName { get => _name; set => _name = value; }
        public double Price { get => _price; set => _price = value; }
        public double PurchasePrice { get => _pprice; set => _pprice = value; }
        public bool IsCanceled { get => _cancel; set => _cancel = value; }
        public int PackQ { get => _packq; set => _packq = value; }
        public int InitQ { get => _initq; set => _initq = value; }

        //popup keyboard eventhandlers
        private void PpKeyboard_ShowNumbersPressed(object sender, EventArgs e)
        {
            if (ppKeyboard.IsNumbers)
            {
                KeyboardDock.Height = 100;
            }
            if (ppKeyboard.IsNumbers == false)
            {
                KeyboardDock.Height = 200;
            }
        }
        private void PpKeyboard_EnterPressed(object sender, EventArgs e)
        {
            ppKeyboard.Visibility = Visibility.Collapsed;
        }
        private void PpKeyboard_ButtonPressed(object sender, EventArgs e)
        {
            if (txtFocussed != null)
            {
                txtFocussed.Text = ppKeyboard.Input;
            }
            
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            
            _name = txtName.Text;
            bool haserrors = false;
            try { _price = double.Parse(txtPrice.Text); }
            catch(FormatException fe)
            {
                lblInvalidPrice.Visibility = Visibility.Visible;
                haserrors = true;
            }
            try { _pprice = double.Parse(txtPPrice.Text); }
            catch (FormatException fe)
            {
                lblInvalidPPrice.Visibility = Visibility.Visible;
                haserrors = true;
            }
            if (cmbCat.SelectedIndex < 0)
            {
                lblInvalidCat.Visibility = Visibility.Visible;
                haserrors = true;
            }
            else
            {
                _category = (BLL.ProductCategory)cmbCat.SelectedIndex;
            }
            try { _packq = int.Parse(txtPackQ.Text); }
            catch (FormatException fe)
            {
                lblInvalidPackQ.Visibility = Visibility.Visible;
                haserrors = true;
            }
            try { _initq = int.Parse(txtInitQ.Text); }
            catch (FormatException fe)
            {
                lblInvalidQ.Visibility = Visibility.Visible;
                haserrors = true;
            }
            if (haserrors == false)
            {
                _cancel = false;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            _cancel = true;
            this.Close();
        }

        private void TxtName_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txtFocussed = sender as TextBox;
            ppKeyboard.Input = "";
            ppKeyboard.Visibility = Visibility.Visible;
        }

        private void Other_GotFocus(object sender, RoutedEventArgs e)
        {
            txtFocussed = null;
            ppKeyboard.Visibility = Visibility.Collapsed;
        }

        private void Grid_GotFocus(object sender, RoutedEventArgs e)
        {
            txtFocussed = null;
            ppKeyboard.Visibility = Visibility.Collapsed;
        }

        private void TxtPrice_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            txtFocussed = sender as TextBox;
            ppKeyboard.ActivateNumbers();
            ppKeyboard.Input = "";
            ppKeyboard.Visibility = Visibility.Visible;
            if (lblInvalidPrice.Visibility == Visibility.Visible)
            {
                lblInvalidPrice.Visibility = Visibility.Hidden;
            }
        }

        private void CmbCat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbCat.SelectedIndex != -1)
            {
                lblInvalidCat.Visibility = Visibility.Hidden;
            }
        }
    }
}
