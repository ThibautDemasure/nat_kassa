﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.UI.UserWindows
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog(string titel, string desc1, string value1, string desc2, string value2)
        {
            InitializeComponent();
            txtTitel.Text = titel;
            txtDesc1.Text = desc1;
            txtDesc2.Text = desc2;
            txtInput1.Text = value1;
            txtInput2.Text = value2;
        }
        public string Value1 { get => txtInput1.Text; }
        public string Value2 { get => txtInput2.Text; }
        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(txtInput2.Text, out double result))
            {
                this.Close();
            }
            else
            {
                txtDesc2.Foreground = Brushes.Red;
            }
            
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (! double.TryParse(tb.Text, out double result))
            {
                tb.Foreground = Brushes.Red;
            }
            else
            {
                tb.Foreground = Brushes.Black;
            }
        }
    }
}
