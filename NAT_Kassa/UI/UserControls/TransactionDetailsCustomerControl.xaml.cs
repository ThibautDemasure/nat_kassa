﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for TransactionDetailsCustomerControl.xaml
    /// </summary>
    public partial class TransactionDetailsCustomerControl : UserControl
    {
        public TransactionDetailsCustomerControl(BLL.Transaction transaction)
        {
            InitializeComponent();
            _transaction = transaction;
            this.DataContext = _transaction;

            lvProducts.ItemsSource = _transaction.Products;
            lvPayments.ItemsSource = _transaction.Payments;
        }
        private BLL.Transaction _transaction;
        public BLL.Transaction Transaction { get => _transaction; set => _transaction = value; }

        public TransactionDetailsCustomerControl()
        {
            _transaction = new BLL.Transaction(9999, new BLL.User("Dummy User"));
            _transaction.AddProduct(new BLL.Product("Testp", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testpro", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct2", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct3", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct4", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.Pay(6454, BLL.PaymentMethod.Payconiq, 5);
            InitializeComponent();
            this.DataContext = _transaction;
            lvProducts.ItemsSource = _transaction.Products;
            lvPayments.ItemsSource = _transaction.Payments;
        }
    }
}
