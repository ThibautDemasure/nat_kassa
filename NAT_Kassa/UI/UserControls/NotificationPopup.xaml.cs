﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for NotificationPopup.xaml
    /// </summary>
    public partial class NotificationPopup : UserControl
    {
        private Storyboard _sb = new Storyboard();
        public NotificationPopup()
        {
            InitializeComponent();
        }
        public NotificationPopup(string titel, bool success, string msg)
        {
            InitializeComponent();
            gMain.Background = success ? Brushes.Green : Brushes.Red;
            txtTitel.Text = titel;
            txtMessage.Text = msg;
            DoubleAnimation animationIn = new DoubleAnimation(0.0, 300.0, TimeSpan.FromSeconds(0.6));
            animationIn.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseOut, Amplitude = 0.4 };
            DoubleAnimation animationOut = new DoubleAnimation(300, 0, TimeSpan.FromSeconds(0.6));
            animationOut.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseIn, Amplitude = 0.4 };
            animationOut.BeginTime = TimeSpan.FromSeconds(4);
            _sb.Children.Add(animationIn);
            _sb.Children.Add(animationOut);
            Storyboard.SetTargetName(animationIn, gMain.Name);
            Storyboard.SetTargetName(animationOut, gMain.Name);
            Storyboard.SetTargetProperty(animationIn, new PropertyPath(Border.WidthProperty));
            Storyboard.SetTargetProperty(animationOut, new PropertyPath(Border.WidthProperty));
        }
        public NotificationPopup(bool vertical, string titel, string msg)
        {
            InitializeComponent();
            gMain.Background = vertical ? Brushes.Green : Brushes.Red;
            txtTitel.Text = titel;
            txtMessage.Text = msg;
            DoubleAnimation animationIn = new DoubleAnimation(0.0, 200.0, TimeSpan.FromSeconds(0.6));
            animationIn.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseOut, Amplitude = 0.4 };
            DoubleAnimation animationOut = new DoubleAnimation(200.0, 0.0, TimeSpan.FromSeconds(0.6));
            animationOut.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseIn, Amplitude = 0.4 };
            animationOut.BeginTime = TimeSpan.FromSeconds(4);
            _sb.Children.Add(animationIn);
            _sb.Children.Add(animationOut);
            Storyboard.SetTargetName(animationIn, gMain.Name);
            Storyboard.SetTargetName(animationOut, gMain.Name);
            Storyboard.SetTargetProperty(animationIn, new PropertyPath(Border.HeightProperty));
            Storyboard.SetTargetProperty(animationOut, new PropertyPath(Border.HeightProperty));
        }
        public NotificationPopup(string titel, Brush colorbrush, string msg)
        {
            InitializeComponent();
            gMain.Background = colorbrush;
            txtTitel.Text = titel;
            txtMessage.Text = msg;
            DoubleAnimation animationIn = new DoubleAnimation(0.0, 300.0, TimeSpan.FromSeconds(0.6));
            animationIn.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseOut, Amplitude = 0.4 };
            DoubleAnimation animationOut = new DoubleAnimation(300, 0, TimeSpan.FromSeconds(0.6));
            animationOut.EasingFunction = new BackEase() { EasingMode = EasingMode.EaseIn, Amplitude = 0.4 };
            animationOut.BeginTime = TimeSpan.FromSeconds(3);
            _sb.Children.Add(animationIn);
            _sb.Children.Add(animationOut);
            Storyboard.SetTargetName(animationIn, gMain.Name);
            Storyboard.SetTargetName(animationOut, gMain.Name);
            Storyboard.SetTargetProperty(animationIn, new PropertyPath(Border.WidthProperty));
            Storyboard.SetTargetProperty(animationOut, new PropertyPath(Border.WidthProperty));
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            System.Timers.Timer shutdowntimer = new System.Timers.Timer(5000);
            shutdowntimer.Elapsed += Shutdowntimer_Elapsed;
            shutdowntimer.Start();
            _sb.Begin(this);
        }
        /// <summary>
        /// Event that fires when a Card is connected or disconnected. Get ConnectedCard to get card data.
        /// </summary>
        public event EventHandler OnRemove;
        protected virtual void Remove(EventArgs e)
        {
            EventHandler handler = OnRemove;
            handler?.Invoke(this, e);
        }
        private void Shutdowntimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Timers.Timer tmr = sender as System.Timers.Timer;
            tmr.Stop();
            tmr.Dispose();
            Remove(EventArgs.Empty);
        }
    }
}
