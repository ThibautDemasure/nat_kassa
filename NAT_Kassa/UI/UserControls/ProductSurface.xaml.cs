﻿using NAT_Kassa.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI.UserControls
{
    /// <summary>
    /// Interaction logic for ProductSurface.xaml
    /// </summary>
    public partial class ProductSurface : UserControl
    {
        private BLL.InventoryProduct _product;
        public ProductSurface(BLL.InventoryProduct product, int inventorytransactiontype)
        {
            InitializeComponent();
            _product = product;
            chbActive.IsChecked = _product.Used;
            spObject.IsEnabled = _product.Used;
            if (! _product.Used) { spObject.Opacity = 0.60; }
            txtName.Text = _product.Name;
            txtCurrentQuantity.Text = _product.Quantity.ToString();
            txtOrderPoint.Text = _product.MinQ.ToString() + " packs";
            txtOrderQuantity.Text = _product.OrderQ.ToString() + " packs";
            txtPackQuantity.Text = _product.PackedQuantity.ToString() + " units / pack";

            if (inventorytransactiontype == 1)
            {
                chbActive.IsEnabled = false;
                chbActive.Opacity = 0.6;
                spObject.IsEnabled = false;
                txtCurrentQuantity.Text = _product.Quantity.ToString();
                txtOrderPoint.Text = "N/A";
                txtOrderQuantity.Text = "N/A";
                txtPackQuantity.Text = "N/A";
            }
            if (inventorytransactiontype == 2)
            {
                chbActive.IsEnabled = false;
                chbActive.Opacity = 0.6;
                spObject.IsEnabled = false;
                txtCurrentQuantity.Text = _product.Quantity.ToString();
                txtOrderPoint.Text = "N/A";
                txtOrderQuantity.Text = _product.OrderQ.ToString() + " packs";
                txtPackQuantity.Text = _product.PackedQuantity.ToString() + " units / pack";
            }
            if (inventorytransactiontype == 3)
            {
                chbActive.IsEnabled = false;
                chbActive.Opacity = 0.6;
                spObject.IsEnabled = false;
                txtCurrentQuantity.Text = _product.Quantity.ToString();
                txtOrderPoint.Text = "N/A";
                txtOrderQuantity.Text = "N/A";
                txtPackQuantity.Text = "N/A";
            }
            if (inventorytransactiontype == 4)
            {
                chbActive.IsEnabled = false;
                chbActive.Opacity = 0.6;
                spObject.IsEnabled = false;
                txtCurrentQuantity.Text = _product.Quantity.ToString();
                txtOrderPoint.Text = "N/A";
                txtOrderQuantity.Text = "N/A";
                txtPackQuantity.Text = "N/A";
            }
            if (inventorytransactiontype == 5 || inventorytransactiontype == 6)
            {
                txtCurrentQuantity.Text = _product.Quantity.ToString();
                txtOrderPoint.Text = "N/A";
                txtOrderQuantity.Text = _product.OrderQ.ToString() + " packs";
                txtPackQuantity.Text = _product.PackedQuantity.ToString() + " units / pack";
            }
        }


        public InventoryProduct Product { get => _product; set => _product = value; }

        private void btnOPup_Click(object sender, RoutedEventArgs e)
        {
            _product.MinQ += 1;
            txtOrderPoint.Text = _product.MinQ.ToString() + " packs";
        }
        private void btnOPdown_Click(object sender, RoutedEventArgs e)
        {
            _product.MinQ -= 1;
            txtOrderPoint.Text = _product.MinQ.ToString() + " packs";
        }
        private void btnOQup_Click(object sender, RoutedEventArgs e)
        {
            _product.OrderQ += 1;
            txtOrderQuantity.Text = _product.OrderQ.ToString() + " packs";
        }
        private void btnOQdown_Click(object sender, RoutedEventArgs e)
        {
            _product.OrderQ -= 1;
            txtOrderQuantity.Text = _product.OrderQ.ToString() + " packs";
        }
        private void btnPQup_Click(object sender, RoutedEventArgs e)
        {
            _product.PackedQuantity += 1;
            txtPackQuantity.Text = _product.PackedQuantity.ToString() + " units / pack";
            
        }
        private void btnPQdown_Click(object sender, RoutedEventArgs e)
        {
            _product.PackedQuantity -= 1;
            txtPackQuantity.Text = _product.PackedQuantity.ToString() + " units / pack";
        }

        private void chbActive_Checked(object sender, RoutedEventArgs e)
        {
            _product.Used = true;
            spObject.IsEnabled = true;
            spObject.Opacity = 1;
        }

        private void chbActive_Unchecked(object sender, RoutedEventArgs e)
        {
            _product.Used = false;
            spObject.IsEnabled = false;
            spObject.Opacity = 0.60;
        }

        private void txtCurrentQuantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_product != null)
            {
                if (int.TryParse(txtCurrentQuantity.Text, out int inputvalue)) { _product.Quantity = inputvalue; txtCurrentQuantity.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_OnDarkSurface"]; }
                else { txtCurrentQuantity.Foreground = (SolidColorBrush)Application.Current.Resources["Brush_ErrorSurface"]; }
            }

        }
    }
}
