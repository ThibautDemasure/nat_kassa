﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for TransactionButton.xaml
    /// </summary>
    public partial class TransactionButton : UserControl
    {
        private BLL.Transaction _transaction;
        private bool _selected;

        public TransactionButton(BLL.Transaction transaction)
        {
            InitializeComponent();
            _transaction = transaction;
            txtId.Text = "ID: " + _transaction.Id.ToString();
            txtDesc.Text = _transaction.Description;
            txtRestAmount.Text = _transaction.AmountOpen.ToString();
            _selected = false;
            _transaction.OnDescriptionChanged += _transaction_OnDescriptionChanged;
            _transaction.OnAmountChanged += _transaction_OnAmountChanged;
        }

        private void _transaction_OnAmountChanged(object sender, EventArgs e)
        {
            txtRestAmount.Text = _transaction.AmountOpen.ToString();
        }

        private void _transaction_OnDescriptionChanged(object sender, EventArgs e)
        {
            txtDesc.Text = _transaction.Description;
        }

        public Transaction Transaction
        {
            get
            {
                return _transaction;
            }
            set
            {
                _transaction = value;
                txtId.Text = "ID: " + _transaction.Id.ToString();
                txtDesc.Text = _transaction.Description;
                txtRestAmount.Text = _transaction.AmountOpen.ToString();
            }
        }

        public bool IsSelected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                if (_selected)
                {
                    btnMain.Background = (SolidColorBrush)Application.Current.Resources["Brush_Selected_Secondary"];
                }
                else
                {
                    btnMain.Background = (SolidColorBrush)Application.Current.Resources["Brush_Elevation_03dp"];
                }
            }
        }


    }
}
