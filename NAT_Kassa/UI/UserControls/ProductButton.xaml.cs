﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAT_Kassa.BLL;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for ProductButton.xaml
    /// </summary>
    public partial class ProductButton : UserControl
    {
        public ProductButton(BLL.Product product)
        {
            InitializeComponent();
            _product = product;
            this.IsEnabled = _product.IsAvailable;
            if (_product.Quantity <= _product.Warninglevel) { this.notifyballoon.Visibility = Visibility.Visible; this.q.Text = _product.Quantity.ToString(); this.q.Visibility = Visibility.Visible; }
            _product.QuantityChanged += _product_QuantityChanged;
            _product.Refilled += _product_Refilled;
            _product.Unavailable += _product_Unavailable;
            _product.Available += _product_Available;
            btnMain.BorderBrush = new SolidColorBrush(CatColorConvertor(product.Category));
            pn.Text = _product.Name;
            pp.Text = "€" + _product.Price.ToString();
            q.Visibility = Visibility.Hidden;
            if (product.Quantity <= product.Warninglevel)
            {
                q.Visibility = Visibility.Visible;
            }
        }

        private void _product_Unavailable(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() => {
                this.IsEnabled = false;
            });
        }

        private BLL.Product _product;

        private void _product_Available(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() => {
                this.IsEnabled = true;
            });
        }

        private void _product_Refilled(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() => {
                this.notifyballoon.Visibility = Visibility.Hidden;
                this.q.Visibility = Visibility.Hidden;
            });
        }

        private void _product_QuantityChanged(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() => {
                this.notifyballoon.Visibility = Visibility.Visible;
                this.q.Text = _product.Quantity.ToString();
                this.q.Visibility = Visibility.Visible;
            });

        }

        public Product Product { get => _product; set => _product = value; }

        private Color CatColorConvertor (BLL.ProductCategory cat)
        {
            
            switch (cat)
            {
                case BLL.ProductCategory.Water:
                    return (Color)Application.Current.FindResource("HighlightBabyblue");
                case BLL.ProductCategory.Frisdrank:
                    return (Color)Application.Current.FindResource("SolidBlue");
                case BLL.ProductCategory.Sportdrank:
                    return (Color)Application.Current.FindResource("SolidBabyblue");

                case BLL.ProductCategory.Bier:
                    return Colors.Yellow;
                case BLL.ProductCategory.Bubbels:
                    return Colors.LightGoldenrodYellow;
                case BLL.ProductCategory.Wijn:
                    return Colors.MediumVioletRed;

                case BLL.ProductCategory.Fruitsap:
                    return (Color)Application.Current.FindResource("SolidOrange");
                case BLL.ProductCategory.Warme_Dranken:
                    return Colors.DarkRed;

                case BLL.ProductCategory.Snacks:
                    return (Color)Application.Current.FindResource("DiversePurple");
                case BLL.ProductCategory.Diverse:
                    return Colors.GhostWhite;
                
                
                
                case BLL.ProductCategory.None:
                    return (Color)Application.Current.FindResource("BackgroundLight");
                
                
                
                default:
                    return (Color)Application.Current.FindResource("SolidBlue");
            }
        }

        public event EventHandler Clicked;
        protected virtual void OnClicked(EventArgs e)
        {
            EventHandler handler = Clicked;
            handler?.Invoke(this, e);
        }

        private void btnMain_Click(object sender, RoutedEventArgs e)
        {
            OnClicked(EventArgs.Empty);
        }
    }
}
