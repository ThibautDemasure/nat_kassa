﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for LoginCodeControl.xaml
    /// </summary>
    public partial class LoginCodeControl : UserControl
    {
        public LoginCodeControl()
        {
            InitializeComponent();
        }
        private void NumberClicked(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn.Name == "btnDel")
            {
                txtNumberDisplay.Password = "";
                return;
            }
            if (btn.Name == "btnBack")
            {
                if (txtNumberDisplay.Password.Length >= 1)
                {
                    txtNumberDisplay.Password = txtNumberDisplay.Password.Substring(0, txtNumberDisplay.Password.Length - 1);
                    return;
                }
                return;
            }
            if (txtNumberDisplay.Password.Length < 8)
            {
                txtNumberDisplay.Password += btn.Content.ToString();
            }

        }
        private string _password = "";
        public string Password { get => _password; set => _password = value; }

        private void EnterClicked(object sender, RoutedEventArgs e)
        {
            _password = txtNumberDisplay.Password;
        }
    }
}
