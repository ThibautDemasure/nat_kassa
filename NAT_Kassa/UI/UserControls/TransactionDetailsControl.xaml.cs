﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for TransactionDetailsControl.xaml
    /// </summary>
    public partial class TransactionDetailsControl : UserControl
    {
        
        public TransactionDetailsControl()
        {
            _transaction = new BLL.Transaction(9999, new BLL.User("Dummy User"));
            _transaction.AddProduct(new BLL.Product("Testp", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testpro", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct2", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct3", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.AddProduct(new BLL.Product("Testproduct4", 1.5, 1, BLL.ProductCategory.Bier));
            _transaction.Pay(6454, BLL.PaymentMethod.Payconiq, 5);
            InitializeComponent();
            this.DataContext = _transaction;
            lvProducts.ItemsSource = _transaction.Products;
            lvPayments.ItemsSource = _transaction.Payments;
        }

        private void TxtEditDesc_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            btnEdit.IsEnabled = true;
            txtEditDesc.Visibility = Visibility.Collapsed;
            txtDesc.Visibility = Visibility.Visible;
        }

        public TransactionDetailsControl(BLL.Transaction transaction)
        {
            InitializeComponent();
            _transaction = transaction;
            this.DataContext = _transaction;

            lvProducts.ItemsSource = _transaction.Products;
            lvPayments.ItemsSource = _transaction.Payments;
            txtEditDesc.LostKeyboardFocus += TxtEditDesc_LostKeyboardFocus;
        }
        private BLL.Transaction _transaction;
        public BLL.Transaction Transaction { get => _transaction; set => _transaction = value; }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            txtEditDesc.Text = "";
            txtEditDesc.Visibility = Visibility.Visible;
            txtDesc.Visibility = Visibility.Collapsed;
            if (txtEditDesc.Focus())
            {
                btnEdit.IsEnabled = false;
            }
            
        }
        private void BtnEdit_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (txtEditDesc.Visibility == Visibility.Visible)
            {
                if (txtEditDesc.Text != "")
                {
                    _transaction.Description = txtEditDesc.Text;
                    txtDesc.Text = _transaction.Description;
                    OnDescriptionChanged(EventArgs.Empty);
                    return;
                }
            }
        }

        private void BtnEdit_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (txtEditDesc.Visibility == Visibility.Collapsed)
            {
                txtEditDesc.Text = "";
                txtEditDesc.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Collapsed;
                txtEditDesc.Focus();
                OnEditDescription(EventArgs.Empty);
                return;
            }
            txtEditDesc.Text = "";
            txtEditDesc.Visibility = Visibility.Collapsed;
            txtDesc.Visibility = Visibility.Visible;
            OnEditDescription(EventArgs.Empty);
        }
        
        public event EventHandler DescriptionChanged;
        protected virtual void OnDescriptionChanged(EventArgs e)
        {
            EventHandler handler = DescriptionChanged;
            handler?.Invoke(this, e);
        }

        public event EventHandler EditDescription;
        protected virtual void OnEditDescription(EventArgs e)
        {
            EventHandler handler = EditDescription;
            handler?.Invoke(this, e);
        }

        
    }
}
