﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for PopupKeyboard.xaml
    /// </summary>
    public partial class PopupKeyboard : UserControl
    {
        public PopupKeyboard()
        {
            InitializeComponent();
        }

        private string _input;
        private int _shiftpressed = -1;
        private bool _isnumber = false;
        public string Input { get => _input; set => _input = value; }
        public bool IsNumbers { get => _isnumber; set => _isnumber = value; }
        public void ActivateNumbers()
        {
            NumberKeys.Height = new GridLength(1, GridUnitType.Star);
            AlfaKeysRow1.Height = new GridLength(0);
            AlfaKeysRow2.Height = new GridLength(0);
            AlfaKeysRow3.Height = new GridLength(0);
            _isnumber = true;
            OnShowNumbersPressed(EventArgs.Empty);
        }

        private void TextButton_Click(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            _input = _input + b.Content.ToString();
            if (_shiftpressed == -1)
            {
                _shiftpressed = 0;
                UpdateUpperLowerCase();
            }
            OnButtonPressed(EventArgs.Empty);
        }
        private void BtnSpaceClick(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            _input = _input + " ";
            if (_shiftpressed != 1)
            {
                _shiftpressed = -1;
                UpdateUpperLowerCase();
            }
            OnButtonPressed(EventArgs.Empty);
        }
        private void btnBackspaceClicked(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            if (_input.Length > 1)
            {
                _input = _input.Substring(0, Input.Length - 1);
                OnButtonPressed(EventArgs.Empty);
            }
            if (_input.Length == 1)
            {
                _input = "";
                OnButtonPressed(EventArgs.Empty);
            }
            
        }

        public event EventHandler ButtonPressed;
        protected virtual void OnButtonPressed(EventArgs e)
        {
            EventHandler handler = ButtonPressed;
            handler?.Invoke(this, e);
        }
        public event EventHandler EnterPressed;
        protected virtual void OnEnterPressed(EventArgs e)
        {
            EventHandler handler = EnterPressed;
            handler?.Invoke(this, e);
        }
        public event EventHandler ShowNumbersPressed;
        protected virtual void OnShowNumbersPressed(EventArgs e)
        {
            EventHandler handler = ShowNumbersPressed;
            handler?.Invoke(this, e);
        }

        private void Shift_Click(object sender, RoutedEventArgs e)
        {
            if (_shiftpressed == 0)
            {
                _shiftpressed = -1;
                UpdateUpperLowerCase();
                return;
            }
            if(_shiftpressed == -1)
            {
                _shiftpressed = 1;
                UpdateUpperLowerCase();
                return;
            }
            if(_shiftpressed == 1)
            {
                _shiftpressed = 0;
                UpdateUpperLowerCase();
                return;
            }

        }

        private void UpdateUpperLowerCase()
        {
            if (_shiftpressed == 0)
            {
                btnLShift.Background = this.FindResource("UnselectedButtonBackground") as LinearGradientBrush;
                btnRShift.Background = this.FindResource("UnselectedButtonBackground") as LinearGradientBrush;
                btnLShiftShape.Fill = new SolidColorBrush(Colors.Black);
                btnRShiftShape.Fill = new SolidColorBrush(Colors.Black);
                Button tempbutton = new Button();
                foreach (Grid g in AlfaKeyboard.Children)
                {
                    foreach (var c in g.Children)
                    {
                        if (c.GetType() == tempbutton.GetType())
                        {
                            Button b = c as Button;
                            if (b.Content.ToString().Length == 1)
                            {
                                b.Content = b.Content.ToString().ToLower();
                            }
                        }
                    }
                }
            }
            else
            {
                if(_shiftpressed == 1)
                {
                    btnLShift.Background = this.FindResource("SelectedButtonBackground") as SolidColorBrush;
                    btnRShift.Background = this.FindResource("SelectedButtonBackground") as SolidColorBrush;
                    btnLShiftShape.Fill = new SolidColorBrush(Colors.White);
                    btnRShiftShape.Fill = new SolidColorBrush(Colors.White);
                }
                Button tempbutton = new Button();
                foreach (Grid g in AlfaKeyboard.Children)
                {
                    foreach (var c in g.Children)
                    {
                        if (c.GetType() == tempbutton.GetType())
                        {
                            Button b = c as Button;
                            if (b.Content.ToString().Length == 1)
                            {
                                b.Content = b.Content.ToString().ToUpper();
                            }
                        }
                    }
                }
            }
        }

        private void EnterClicked(object sender, RoutedEventArgs e)
        {
            _input = "";
            OnEnterPressed(EventArgs.Empty);
        }

        private void ShowNumbersClicked(object sender, RoutedEventArgs e)
        {
            if (NumberKeys.Height != new GridLength(1, GridUnitType.Star))
            {
                NumberKeys.Height = new GridLength(1,GridUnitType.Star);
                AlfaKeysRow1.Height = new GridLength(0);
                AlfaKeysRow2.Height = new GridLength(0);
                AlfaKeysRow3.Height = new GridLength(0);
                _isnumber = true;
                OnShowNumbersPressed(EventArgs.Empty);
            }
            else
            {
                NumberKeys.Height = new GridLength(0);
                AlfaKeysRow1.Height = new GridLength(1,GridUnitType.Star);
                AlfaKeysRow2.Height = new GridLength(1,GridUnitType.Star);
                AlfaKeysRow3.Height = new GridLength(1,GridUnitType.Star);
                _isnumber = false;
                OnShowNumbersPressed(EventArgs.Empty);
            }
        }
    }
}
