﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NAT_Kassa.UI
{
    /// <summary>
    /// Interaction logic for CalculatorControl.xaml
    /// </summary>
    public partial class CalculatorControl : UserControl
    {
        public CalculatorControl()
        {
            InitializeComponent();
        }
        public event EventHandler AmountInputChanged;
        protected virtual void OnAmountInputChanged(EventArgs e)
        {
            EventHandler handler = AmountInputChanged;
            handler?.Invoke(this, e);
        }
        public event EventHandler AmountInputDeleted;
        protected virtual void OnAmountInputDeleted(EventArgs e)
        {
            EventHandler handler = AmountInputDeleted;
            handler?.Invoke(this, e);
        }

        private void NumberClicked(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            if (btn.Name == "btnDel")
            {
                txtNumberDisplay.Text = "";
                OnAmountInputDeleted(EventArgs.Empty);
                return;
            }
            if (btn.Name == "btnBack")
            {
                if (txtNumberDisplay.Text.Length >= 1)
                {
                    txtNumberDisplay.Text = txtNumberDisplay.Text.Substring(0, txtNumberDisplay.Text.Length - 1);
                    if (txtNumberDisplay.Text.Length != 0)
                    { OnAmountInputChanged(EventArgs.Empty); }
                    else { OnAmountInputDeleted(EventArgs.Empty); }
                    return;
                }
                return;
            }
            if (txtNumberDisplay.Text.Length < 6)
            {
                txtNumberDisplay.Text += btn.Content.ToString();
                OnAmountInputChanged(EventArgs.Empty);
            }
            
        }
        public int ProductCount()
        {
            int result = 0;
            if (int.TryParse(txtNumberDisplay.Text, out result))
            {
                return result;
            }

            return 1;
        }
        public double Amount()
        {
           return double.Parse(txtNumberDisplay.Text);
        }
    }
}
