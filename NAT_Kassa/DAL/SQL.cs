﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.TextFormatting;
using NAT_Kassa.BLL;
using NAT_Kassa.NATIZEGEMDataSetTableAdapters;

namespace NAT_Kassa.DAL
{
    public class SQL
    {
        #region Fields

        private bool _connected = false;
        private System.Data.SqlClient.SqlConnection _cnn;
        private NATIZEGEMDataSet _ds = new NATIZEGEMDataSet();
        private OrganizationsTableAdapter _orgad = new OrganizationsTableAdapter();
        private PaymentsTableAdapter _payad = new PaymentsTableAdapter();
        private ProductsTableAdapter _prodad = new ProductsTableAdapter();
        private TransactionProductsTableAdapter _transprodad = new TransactionProductsTableAdapter();
        private TransactionsTableAdapter _transad = new TransactionsTableAdapter();
        private UsersTableAdapter _userad = new UsersTableAdapter();
        private InventoryTransactionTypesTableAdapter _invtranstypead = new InventoryTransactionTypesTableAdapter();
        private InventoryTransactionsTableAdapter _invtransad = new InventoryTransactionsTableAdapter();
        private MemberCardsTableAdapter _membercardad = new MemberCardsTableAdapter();
        private int _iRegisterID = 0;
        
        #endregion

        #region Constructors

        // Singleton class by privatising the constructor
        //constructor must only happen once
        private SQL()
        {
            _cnn = _transad.Connection;
            if (_cnn.State == System.Data.ConnectionState.Open || _cnn.State == System.Data.ConnectionState.Connecting)
            {
                _connected = true;
            }
            else
            {
                _connected = false;
            }
            //base code to access the records from a result datatable using data adapters
            //iterate through the table for multi row
            //use the adapter custom methods to execute corresponding queries, make methods in dataset designer
            //methods can also  be insert, update, delete, ... not just select.
            //NATIZEGEMDataSet dataSet = new NATIZEGEMDataSet();
            //var testvar = dataSet.Organizations.Rows[0].ItemArray[0];
            //NATIZEGEMDataSetTableAdapters.OrganizationsTableAdapter adapter = new NATIZEGEMDataSetTableAdapters.OrganizationsTableAdapter();
            //NATIZEGEMDataSet.OrganizationsDataTable organizations = adapter.LoadOrganizations();
            //int test = (int)organizations.Rows[0].ItemArray[0];
            //var row = organizations.Rows[0] as NATIZEGEMDataSet.OrganizationsRow;
            //test = row.Id;
            _iRegisterID = Properties.Settings.Default.iRegID;

        }
        //Singleton instance
        private static readonly SQL _instance = new SQL();
        //Instance property for access to DA Class
        public static SQL Instance { get { return _instance; } }



        #endregion

        #region Properties

        public bool IsConnected { get => _connected; }
        public int RegisterID { get => _iRegisterID; }

        #endregion

        #region Methods

        #region Users

        /// <summary>
        /// Loads all the users.
        /// </summary>
        /// <returns>List of type User</returns>
        public List<User> LoadUsers()
        {
            List<User> userpool = new List<User>();
            try
            {
                NATIZEGEMDataSet.UsersDataTable usertable = _userad.LoadUsers();
                foreach (System.Data.DataRow row in usertable.Rows)
                {
                    var user = row as NATIZEGEMDataSet.UsersRow;
                    userpool.Add(new User(user.Id, user.Name, user.Surname, user.Address, user.Email, (BLL.OrganizationID)user.OrganizationId, (Userlevel)Enum.Parse(typeof(Userlevel), user.Userlevel), user.Pw));
                    if (! user.IsActiveNull()) { userpool.Last().IsActive = user.Active; } else { userpool.Last().IsActive = true; }

                }
                _connected = true;
                return userpool;
            }
            catch
            {
                _connected = false;
                return null;
            }

        }
        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <param name="user">User to delete</param>
        public void DeleteUser(User user)
        {
            try { _userad.DeleteUser(user.Id); _connected = true; }
            catch { _connected = false; }
        }

        public void ChangeUserActivation(User user)
        {
            try { _userad.UpdateUserActivation(user.IsActive,user.Id); _connected = true; }
            catch { _connected = false; }
        }
        /// <summary>
        /// Adds a new user.
        /// </summary>
        /// <param name="user">user to add</param>
        public void AddUser(User user)
        {
            try
            {
                _userad.Insert(user.Name, user.Surname, user.Address, user.Email, (int)user.OrganizationID, user.Userlevel.ToString(), user.Password, user.IsActive);
                user.Id = (int)_userad.GetLastId();
                _connected = true;
            }
            catch { _connected = false; }
        }
        /// <summary>
        /// Changes the user entry in the users table.
        /// </summary>
        /// <param name="user"></param>
        public void ChangeUser(User user)
        {
            try
            {
                _userad.UpdateUser(user.Address, user.Password,user.IsActive, user.Name, user.Surname, user.Email,(int)user.OrganizationID,user.Userlevel.ToString(),user.Id);
                _connected = true;
            }
            catch { _connected = false; }
        }

        #endregion

        #region MemberCards
        public List<Card> LoadCards()
        {
            List<Card> cardpool = new List<Card>();
            try
            {
                NATIZEGEMDataSet.MemberCardsDataTable memberCards = _membercardad.GetData();
                foreach (System.Data.DataRow row in memberCards.Rows)
                {
                    var card = row as NATIZEGEMDataSet.MemberCardsRow;
                    cardpool.Add(new Card(){ Name = card.Voornaam, SurName = card.Naam, UID = card.UID, Credit = card.Saldo });
                }
                _connected = true;
                return cardpool;
            }
            catch
            {
                _connected = false;
                return null;
            }
        }
        public void UpdateCardSaldo(Card card)
        {
            try
            {
                int changecount = _membercardad.UpdateSaldo(card.Credit, card.UID);
                if (changecount == 0)
                {
                    _membercardad.InsertMemberCard(card.UID, card.Name, card.SurName, card.Credit);
                }
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void UpdateCardOwner(Card card)
        {
            try
            {
                int changecount = _membercardad.UpdateOwner(card.Name, card.SurName, card.UID);
                if (changecount == 0)
                {
                    _membercardad.InsertMemberCard(card.UID, card.Name, card.SurName, card.Credit);
                }
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void AddCard (Card card)
        {
            try
            {
                _membercardad.InsertMemberCard(card.UID, card.Name, card.SurName, card.Credit);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void DeleteCard(Card card)
        {
            try
            {
                _membercardad.DeleteCard(card.UID);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }

        #endregion
        #region Products
        //products

        /// <summary>
        /// Loads all the products.
        /// </summary>
        /// <returns>List of type Product</returns>
        public List<Product> LoadProducts()
        {
            try
            {
                List<Product> productpool = new List<Product>();
                NATIZEGEMDataSet.ProductsDataTable producttable = _prodad.LoadProducts();

                foreach (System.Data.DataRow row in producttable.Rows)
                {
                    var p = row as NATIZEGEMDataSet.ProductsRow;
                    productpool.Add(new Product(p.Name,
                        double.Parse(p.Price.ToString()),
                        double.Parse(p.PurchasePrice.ToString()),
                        (ProductCategory)Enum.Parse(typeof(ProductCategory), p.Category),
                        p.Id, 1, true, true));
                    if (! p.IsCurrentQuantityNull()) { productpool.Last().Quantity = p.CurrentQuantity; } else { productpool.Last().Quantity = 0; }
                    if (! p.IsActiveNull()) { productpool.Last().IsActive = Convert.ToBoolean(p.Active); } else { productpool.Last().IsActive = false; }
                    if (!p.IsWarningLevelNull()) { productpool.Last().Warninglevel = p.WarningLevel; } else { productpool.Last().Warninglevel = 48; }
                }
                _connected = true;
                return productpool;
            }
            catch { _connected = false; return null; }
        }
        /// <summary>
        /// Loads all info of all products.
        /// </summary>
        /// <returns>list of InventoryProducts</returns>
        public List<InventoryProduct> LoadInventory()
        {
            try
            {
                List<InventoryProduct> productpool = new List<InventoryProduct>();
                NATIZEGEMDataSet.ProductsDataTable producttable = _prodad.LoadProducts();

                foreach (System.Data.DataRow row in producttable.Rows)
                {
                    var p = row as NATIZEGEMDataSet.ProductsRow;
                    if (p.IsActiveNull() || p.Active == false) { continue; }
                    productpool.Add(new InventoryProduct(p.Name,
                        double.Parse(p.Price.ToString()),
                        double.Parse(p.PurchasePrice.ToString()),
                        (ProductCategory)Enum.Parse(typeof(ProductCategory), p.Category),
                        p.Id, 1, true, true, 1, 0, 0, 0));

                    //if (!p.IsActiveNull()) { productpool.Last().IsActive = Convert.ToBoolean(p.Active); } else { productpool.Last().IsActive = true; }
                    
                    if (!p.IsPackagingQuantityNull()) { productpool.Last().PackedQuantity = p.PackagingQuantity; } else { productpool.Last().PackedQuantity = 1; }
                    if (!p.IsReorderPointNull()) { productpool.Last().MinQ = p.ReorderPoint; } else { productpool.Last().MinQ = 0; }
                    if (!p.IsReorderQuantityNull()) { productpool.Last().OrderQ = p.ReorderQuantity; } else { productpool.Last().OrderQ = 0; }
                    if (!p.IsCurrentQuantityNull()) { productpool.Last().Quantity = p.CurrentQuantity; } else { productpool.Last().Quantity = 0; }
                    productpool.Last().TargetQ = productpool.Last().Quantity / productpool.Last().PackedQuantity + productpool.Last().OrderQ;
                    productpool.Last().Used = productpool.Last().IsLow;
                }
                _connected = true;
                return productpool;
            }
            catch { _connected = false; return null; }
        }
        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="product">Product to delete</param>
        public void DeleteProduct(Product product)
        {
            try { _prodad.DeleteProductById(product.Id); }
            catch { _connected = false; }
        }
        public void ChangeProductActivation(Product product)
        {
            try { _prodad.UpdateProductActivation(product.IsActive,product.Id); _connected = true; }
            catch { _connected = false; }
        }
        /// <summary>
        /// Adds a new product.
        /// </summary>
        /// <param name="product">product to add</param>
        public void Addproduct(Product product)
        {
            try
            {
                _prodad.Insert(product.Name, (float)product.Price, product.Category.ToString(), (float)product.Purchaseprice, product.IsActive, product.Quantity,product.Warninglevel / 2,2,3,product.Warninglevel);
                product.Id = (int)_prodad.GetLastId();
                _connected = true;
            }
            catch { _connected = false; }
        }
        public void ChangeProduct(Product prod)
        {
            try
            {
                _prodad.UpdateProduct(prod.Name, (float)prod.Price, prod.Category.ToString(), (float)prod.Purchaseprice, prod.IsActive, prod.Id);
                _connected = true;
            }
            catch { _connected = false; }
        }
        public List<string> LoadInventoryTransactionTypes()
        {
            List<string> types = new List<string>();

            try
            {
                NATIZEGEMDataSet.InventoryTransactionTypesDataTable tbl = _invtranstypead.GetData();
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    var t = row as NATIZEGEMDataSet.InventoryTransactionTypesRow;
                    types.Add(t.InventoryTransactionType);
                }
                _connected = true;
                return types;
            }
            catch
            {
                _connected = false;
                return null;
            }
        }
        public List<InventoryProduct> LoadInventoryTransaction(int Id)
        {
            List<InventoryProduct> products = new List<InventoryProduct>();
            try
            {
                NATIZEGEMDataSet.InventoryTransactionsDataTable tbl = _invtransad.GetDataById(Id);
                List<Product> prods = LoadProducts();
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    var t = row as NATIZEGEMDataSet.InventoryTransactionsRow;
                    Product p = GetProduct(prods, t.ProductId);
                    products.Add(new InventoryProduct(t.ProductName, p.Price, p.Purchaseprice, p.Category, t.ProductId, t.Qty, true, true, 0, 0, 0, 0));
                    if (! t.IsQtyPackNull()) { products.Last().OrderQ = t.QtyPack; }
                    if (! t.IsUnitsPerPackNull()) { products.Last().PackedQuantity = t.UnitsPerPack; }
                }
                _connected = true;
                return products;
            }
            catch
            {
                _connected = false;
                return null;
            }
        }
        public List<InventoryTransaction> LoadInventoryTransactions()
        {
            List<InventoryTransaction> transactions = new List<InventoryTransaction>();
            try
            {

                NATIZEGEMDataSet.InventoryTransactionsDataTable ids =_invtransad.GetDistinctIDs();
                NATIZEGEMDataSet.InventoryTransactionsDataTable tbl;
                foreach (System.Data.DataRow row in ids.Rows)
                {
                    int id = (int)row.ItemArray[1];
                    tbl = _invtransad.GetDataById(id);
                    var transrow = tbl.Rows[0] as NATIZEGEMDataSet.InventoryTransactionsRow;
                    transactions.Add(new InventoryTransaction() { Id = id, DeliveryNote = transrow.DeliveryNote, Remark = transrow.Remark, Timestamp = transrow.Timestamp, NamedType = transrow.Type, UserName = transrow.UserName });
                }
                _connected = true;
                return transactions;
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message, "error");
                _connected = false;
                return null;
            }
        }

        public void UpdateOrderParameters(InventoryProduct product)
        {
            try
            {
                _prodad.UpdateOrderParameters(product.PackedQuantity, product.MinQ, product.OrderQ, product.Id);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void UpdateCurrentQuantity(InventoryProduct p, InventoryTransaction t)
        {
            try
            {
                _invtransad.Insert(t.Id, t.NamedType, p.Quantity, null, null, p.Name, p.Id, t.Timestamp, t.DeliveryNote, t.Remark, p.Quantity, (decimal)(p.Quantity * p.Purchaseprice),_iRegisterID,t.UserName);
                _prodad.UpdateCurrentQuantity(p.Quantity, p.Id);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void PurchaseOrderProduct(InventoryProduct p, InventoryTransaction t)
        {
            try
            {
                _invtransad.Insert(t.Id, t.NamedType, p.PackedQuantity * p.OrderQ, p.OrderQ, p.PackedQuantity, p.Name, p.Id, t.Timestamp, t.DeliveryNote, t.Remark, null, null,_iRegisterID,t.UserName);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        /// <summary>
        /// Subtract a product from inventory list
        /// </summary>
        /// <param name="p"></param>
        /// <param name="t"></param>
        public void IssueTransactionProduct(Product p, InventoryTransaction t)
        {
            try
            {
                int q;
                if (t.Id == 0)
                {
                    q = (int)_prodad.SubtractFromProduct(p.Id,0);
                    NATIZEGEMDataSet.InventoryTransactionsDataTable ids = _invtransad.GetDistinctIDs();
                    if (ids.Rows.Count > 0)
                    {
                        t.Id = (int)ids.Rows[ids.Rows.Count - 1].ItemArray[1] + 1;
                    }
                    else
                    {
                        t.Id = 1;
                    }
                }
                else
                {
                    if (t.Type == 3)
                    {
                        q = (int)_prodad.SubtractFromProduct(p.Id, p.Quantity);
                    }
                    else
                    {
                        q = (int)_prodad.SubtractFromProduct(p.Id, 0);
                    }
                }

                decimal onhandvalue = (decimal)(q * p.Purchaseprice);
                _invtransad.Insert(t.Id, t.NamedType, p.Quantity, null, null, p.Name, p.Id, t.Timestamp, t.DeliveryNote, t.Remark, q, onhandvalue,_iRegisterID,t.UserName);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        public void ReceiveOrderProduct(InventoryProduct p, InventoryTransaction t)
        {
            try
            {
                int q = (int)_prodad.AddToProduct(p.Id, p.PackedQuantity * p.OrderQ);
                decimal onhandvalue = (decimal)(q * p.Purchaseprice);
                _invtransad.Insert(t.Id, t.NamedType, p.PackedQuantity * p.OrderQ, p.OrderQ, p.PackedQuantity, p.Name, p.Id, t.Timestamp, t.DeliveryNote, t.Remark, q, onhandvalue,_iRegisterID,t.UserName);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }

        public int SubtractProductQuantity(Product product)
        {
            try
            {
                var returnval = _prodad.SubtractFromProduct(product.Id, product.Quantity);
                if (returnval != null) { return (int)returnval; }

                return product.Quantity;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "exception");
                _connected = false;
                return product.Quantity;
            }
        }
        public int AddProductQuantity(Product product)
        {
            try
            {
                var returnval = _prodad.AddToProduct(product.Id, product.Quantity);
                if (returnval != null) { return (int)returnval; }
                return product.Quantity;
            }
            catch
            {
                _connected = false;
                return product.Quantity;
            }
        }
        
        #endregion

        #region Transactions

        //transaction 

        /// <summary>
        /// Loads all the transaction with unclosed state.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadTransactions()
        {
            try
            {
                List<User> users = LoadUsers();
                List<Product> prods = LoadProducts();
                List<Transaction> transactionpool = new List<Transaction>();
                NATIZEGEMDataSet.TransactionsDataTable transactiontable = _transad.LoadOpenTransactions(TransactionState.Closed.ToString(), TransactionState.Booked.ToString());
                foreach (System.Data.DataRow row in transactiontable.Rows)
                {
                    var t = row as NATIZEGEMDataSet.TransactionsRow;
                    if (t.RegisterId == (byte)_iRegisterID)
                    {
                        Transaction newt = new Transaction(t.Id, GetUser(users, t.UserOpenedId), t.Description);
                        newt.OpenedTimestamp = t.TimeOpened;
                        NATIZEGEMDataSet.TransactionProductsDataTable ptable = _transprodad.GetProductsByTransactionId(t.Id);
                        foreach (System.Data.DataRow prow in ptable.Rows)
                        {
                            var p = prow as NATIZEGEMDataSet.TransactionProductsRow;

                            newt.AddProduct(new Product(p.ProductName, double.Parse(p.Price.ToString()), double.Parse(p.PurchasePrice.ToString()), GetProduct(prods, p.ProductId).Category, p.ProductId, p.Count,true,true));
                        }
                        NATIZEGEMDataSet.PaymentsDataTable paytable = _payad.GetPaymentsByTransactionId(t.Id);
                        foreach (System.Data.DataRow payrow in paytable.Rows)
                        {
                            var p = payrow as NATIZEGEMDataSet.PaymentsRow;
                            newt.Pay(p.Id, (PaymentMethod)Enum.Parse(typeof(PaymentMethod), p.Method), double.Parse(p.Amount.ToString()));
                        }
                        transactionpool.Add(newt);
                    }
                }
                _connected = true;
                return transactionpool;
            }
            catch (Exception e)
            { 
                _connected = false;
                return null; 
            }
        }
        /// <summary>
        /// Loads all the closed transaction.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadClosedTransactions(DateTime begindate, DateTime enddate)
        {
            try
            {
                List<User> users = LoadUsers();
                List<Product> prods = LoadProducts();
                List<Transaction> transactionpool = new List<Transaction>();
                NATIZEGEMDataSet.TransactionsDataTable transactiontable = _transad.LoadClosedTransactions(TransactionState.Closed.ToString(),begindate,enddate);
                foreach (System.Data.DataRow row in transactiontable.Rows)
                {
                    var t = row as NATIZEGEMDataSet.TransactionsRow;

                    Transaction newt = new Transaction(t.Id, GetUser(users, t.UserOpenedId), t.Description);
                    newt.OpenedTimestamp = t.TimeOpened;
                    NATIZEGEMDataSet.TransactionProductsDataTable ptable = _transprodad.GetProductsByTransactionId(t.Id);
                    foreach (System.Data.DataRow prow in ptable.Rows)
                    {
                        var p = prow as NATIZEGEMDataSet.TransactionProductsRow;

                        newt.AddProduct(new Product(p.ProductName, double.Parse(p.Price.ToString()), double.Parse(p.PurchasePrice.ToString()), GetProduct(prods, p.ProductId).Category, p.ProductId, p.Count, true, true));
                    }
                    NATIZEGEMDataSet.PaymentsDataTable paytable = _payad.GetPaymentsByTransactionId(t.Id);
                    foreach (System.Data.DataRow payrow in paytable.Rows)
                    {
                        var p = payrow as NATIZEGEMDataSet.PaymentsRow;
                        newt.Pay(p.Id, (PaymentMethod)Enum.Parse(typeof(PaymentMethod), p.Method), p.Amount);
                    }
                    newt.CloseTransaction(GetUser(users, t.UserClosedId));
                    newt.ClosedTimestamp = t.TimeClosed;
                    transactionpool.Add(newt);
                }
                _connected = true;
                return transactionpool;
            }
            catch { _connected = false; return null; }
        }
        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="product">Transaction to delete</param>
        public void DeleteTransaction(Transaction t)
        {
            try
            {
                _transad.DeleteTransaction(t.Id);
                _payad.DeletePaymentsById(t.Id);
                _transprodad.DeleteProducts(t.Id);
                _connected = true;
            }
            catch
            {
                _connected = false;
            }
        }
        /// <summary>
        /// Adds a transaction.
        /// </summary>
        /// <param name="t"></param>
        public void AddTransaction(Transaction t)
        {
            try
            {
                _transad.InsertTransaction(t.Description, (float)t.Amount, (float)t.AmountPaid, t.State.ToString()
                    , t.Openedby.Id, null, t.OpenedTimestamp, null, (int)t.Openedby.OrganizationID
                    , (byte)_iRegisterID, t.Openedby.OrganizationID.ToString(), null, t.Openedby.Username);
                t.SetId((int)_transad.GetLastId((byte)_iRegisterID));
                _connected = true;
            }
            catch
            {
                _connected = false;
            }

        }
        /// <summary>
        /// Make changes to an existing transaction.
        /// </summary>
        /// <param name="t">transaction to change</param>
        public void ChangeTransaction(Transaction t)
        {
            try
            {
                //update products in transaction
                NATIZEGEMDataSet.TransactionProductsDataTable prodtable = _transprodad.GetProductsByTransactionId(t.Id);
                foreach (Product product in t.Products)
                {
                    if (_transprodad.UpdateProductsByTransactionId((short)product.Quantity, t.Id, product.Name, (float)product.Price,(float)product.Purchaseprice) == 0)
                    {
                        _transprodad.InsertProductsByTransactionId(t.Id, product.Id, product.Name, (float)product.Price, (short)product.Quantity, (float)product.Purchaseprice);
                    }
                }
                if (t.Products.Count < prodtable.Count)
                {
                    foreach (System.Data.DataRow prow in prodtable.Rows)
                    {
                        var p = prow as NATIZEGEMDataSet.TransactionProductsRow;
                        bool contains = false;
                        foreach (Product product in t.Products)
                        {
                            if ((p.ProductName == product.Name) && (double.Parse(p.PurchasePrice.ToString()) == product.Purchaseprice) && (double.Parse(p.Price.ToString()) == product.Price))
                            {
                                contains = true;
                                break;
                            }

                        }
                        if (contains == false)
                        {
                            _transprodad.DeleteSingleProduct(t.Id, p.ProductName, p.Price, p.PurchasePrice);
                        }
                    }
                }

                //update payments in transaction
                NATIZEGEMDataSet.PaymentsDataTable paytable = _payad.GetPaymentsByTransactionId(t.Id);
                if (t.Payments.Count > paytable.Count)
                {
                    foreach (Payment payment in t.Payments)
                    {
                        bool contains = false;
                        foreach (System.Data.DataRow prow in paytable.Rows)
                        {
                            var p = prow as NATIZEGEMDataSet.PaymentsRow;
                            if (payment.Id == p.Id)
                            {
                                contains = true;
                                break;
                            }
                        }
                        if (contains == false)
                        {
                            _payad.InsertPayment(payment.Method.ToString(), (float)payment.Amount, payment.TransactionTimestamp, t.Id, (int)t.Openedby.OrganizationID);
                            payment.SetId((int)_payad.GetLastId(t.Id));//getlastId should be of the correct transactionid
                        }
                    }
                }
                if (t.Payments.Count < paytable.Count)
                {
                    foreach (System.Data.DataRow prow in paytable.Rows)
                    {
                        var p = prow as NATIZEGEMDataSet.PaymentsRow;
                        bool contains = false;
                        foreach (Payment payment in t.Payments)
                        {
                            if (payment.Id == p.Id)
                            {
                                contains = true;
                                break;
                            }
                        }
                        if (contains == false)
                        {
                            _payad.DeleteSinglePayment(t.Id, p.Id);
                        }
                    }
                }

                //update general transaction info
                if (t.State == TransactionState.Closed || t.State == TransactionState.Booked)
                {
                    _transad.UpdateTransactionById(t.Description, (float)t.Amount, (float)t.AmountPaid, t.State.ToString(), t.Closedby.Id, t.ClosedTimestamp
                    , (int)t.Closedby.OrganizationID, (byte)_iRegisterID, t.Closedby.OrganizationID.ToString(), t.Closedby.Username, (float)t.AmountPurchase, t.Id);
                    _connected = true;
                    return;
                }
                _transad.UpdateUnclosedTById(t.Description, (float)t.Amount, (float)t.AmountPaid, t.State.ToString(), null, null
                    , (byte)_iRegisterID, null, (float)t.AmountPurchase, t.Id);
                _connected = true;
            }
            catch { _connected = false; }

        }
        #endregion

        #region Global

        /// <summary>
        /// Closes all connections and saves the data. Use on app shutdown.
        /// </summary>
        public void Shutdown()
        {
            _cnn.Close();
        }

        public void Connect()
        {

                _cnn.Close();
                try { _cnn.Open(); _userad.GetLastId(); }
                catch { _connected = false; return; }

            if (_cnn.State == System.Data.ConnectionState.Open || _cnn.State == System.Data.ConnectionState.Connecting)
            {
                _connected = true;
            }
            else
            {
                _connected = false;
            }

        }
        public bool TestConnection()
        {
            try { _userad.GetLastId(); _connected = true; return true; }
            catch { _connected = false; return false; }
        }

        #endregion

        #endregion

        #region Helpers
        private User GetUser(List<User> users, int userId)
        {
            foreach (User u in users)
            {
                if (u.Id == userId)
                {
                    return u;
                }
            }
            return null;
        }
        private Product GetProduct(List<Product> products, int pid)
        {
            foreach (Product p in products)
            {
                if (p.Id == pid)
                {
                    return p;
                }
            }
            return null;
        }

        #endregion



    }
}
