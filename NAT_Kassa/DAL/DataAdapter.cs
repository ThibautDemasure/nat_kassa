﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAT_Kassa.BLL;
namespace NAT_Kassa.DAL
{
    /// <summary>
    /// This Class provides access to the storage instances.
    /// </summary>
    public class DataAdapter
    {
        #region Fields

        //background worker -> creates a worker thread independent from main UI thread
        private System.Timers.Timer _bgworker = new System.Timers.Timer(60000);
        public static readonly object _syncLock = new object();
        private bool _Locked = false;
        private List<int> _oldtids = new List<int>();
        private List<int> _newtids = new List<int>();
        private List<Transaction> _TtoDel = new List<Transaction>();
        //storage classes
        private XML _xml = XML.Instance;
        private SQL _sql = SQL.Instance;
        private AdapterMode _mode = AdapterMode.None;
        private int _zeroUserId;
        private int _zeroProductId;
        #endregion

        #region Constructors

        // Singleton class by privatising the constructor
        //constructor must only happen once
        private DataAdapter()
        {
            _mode = (AdapterMode)Properties.Settings.Default.iOnlineMode;
            _mode = AdapterMode.Online;
            _bgworker.Elapsed += _bgworker_Elapsed;
            _bgworker.Start();
        }
        //Singleton instance
        private static readonly DataAdapter _instance = new DataAdapter();
        //Instance property for access to DA Class
        public static DataAdapter Instance { get { return _instance; } }

        #endregion

        #region Properties
        public bool IsConnected { get { return _sql.IsConnected; }}

        public bool Locked { get => _Locked; set => _Locked = value; }
        public int RegisterId { get => _sql.RegisterID; }
        public int ZeroUserId { get => _zeroUserId; set => _zeroUserId = value; }
        public int ZeroProductId { get => _zeroProductId; set => _zeroProductId = value; }
        #endregion

        #region Methods

        #region Users


        /// <summary>
        /// Loads all the users.
        /// </summary>
        /// <returns>List of type User</returns>
        public List<User> LoadUsers()
        {
            if (_mode == AdapterMode.Online)
            {
                List<User> users = _sql.LoadUsers();
                if (users != null)
                {
                    return users;
                }
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
            }
            return _xml.LoadUsers();
        }
        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <param name="user">User to delete</param>
        public void DeleteUser(User user)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.DeleteUser(user);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                }
                _xml.DeleteUser(user);
            }
            if (_mode == AdapterMode.Offline) { _xml.DeleteUser(user); }
            
        }
        public void ChangeUserActivation(User user)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.ChangeUserActivation(user);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                }
                _xml.DeleteUser(user);
            }
            if (_mode == AdapterMode.Offline) { _xml.DeleteUser(user); }
        }
        /// <summary>
        /// Adds a new user.
        /// </summary>
        /// <param name="user">user to add</param>
        public void AddUser(User user)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.AddUser(user);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        _xml.AddUser(user, false);
                        OnConnectionChanged(EventArgs.Empty);
                        return;
                    }
                    _xml.AddUser(user,true);
                }
                if (_mode == AdapterMode.Offline) { _xml.AddUser(user,false); }
            }
        }
        /// <summary>
        /// Change the corresponding user entry based on UID
        /// </summary>
        /// <param name="user">User to change</param>
        public void ChangeUser (User user)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.ChangeUser(user);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        _xml.ChangeUser(user, false);
                        OnConnectionChanged(EventArgs.Empty);
                        return;
                    }
                    _xml.ChangeUser(user,true);
                }
                if (_mode == AdapterMode.Offline) { _xml.ChangeUser(user,false); }
            }
        }

        #endregion
        #region MemberCards
        public List<Card> LoadCards()
        {
            if (_mode == AdapterMode.Online)
            {
                return _sql.LoadCards();
            }
            return null;
        }
        public bool UpdateCardSaldo(Card card)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.UpdateCardSaldo(card);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                    return false;
                }
                return true;
            }
            return false;
        }
        public bool UpdateCardOwner(Card card)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.UpdateCardOwner(card);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                    return false;
                }
                return true;
            }
            return false;
        }
        public bool AddCard(Card card)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.AddCard(card);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                    return false;
                }
                return true;
            }
            return false;
        }
        public bool DeleteCard(Card card)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.DeleteCard(card);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                    return false;
                }
                return true;
            }
            return false;
        }
        #endregion
        #region Products

        /// <summary>
        /// Loads all the products.
        /// </summary>
        /// <returns>List of type Product</returns>
        public List<Product> LoadProducts()
        {
            if (_mode == AdapterMode.Online)
            {
                return _sql.LoadProducts();
            }
            return _xml.LoadProducts();
        }
        /// <summary>
        /// Deletes a product.
        /// </summary>
        /// <param name="product">Product to delete</param>
        public void DeleteProduct(Product product)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.DeleteProduct(product);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    _xml.DeleteProduct(product);
                    OnConnectionChanged(EventArgs.Empty);
                    return;
                }
                return;
            }
            _xml.DeleteProduct(product);
        }

        public void ChangeProductActivation(Product product)
        {
            if (_mode == AdapterMode.Online)
            {
                _sql.ChangeProductActivation(product);
                if (_sql.IsConnected == false)
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    _xml.DeleteProduct(product);
                    OnConnectionChanged(EventArgs.Empty);
                    return;
                }
                return;
            }
            _xml.DeleteProduct(product);
        }

        /// <summary>
        /// Adds a new product.
        /// </summary>
        /// <param name="product">product to add</param>
        public void Addproduct(Product product)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.Addproduct(product);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        OnConnectionChanged(EventArgs.Empty);
                        _xml.Addproduct(product, false);
                        return;
                    }
                    _xml.Addproduct(product,true);
                    return;
                }
                _xml.Addproduct(product, false);
            }
        }
        public void ChangeProduct(Product product, BLL.ProductCategory oldcat)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.ChangeProduct(product);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        _xml.ChangeProduct(product , false, oldcat);
                        OnConnectionChanged(EventArgs.Empty);
                        return;
                    }
                    _xml.ChangeProduct(product, true, oldcat);
                }
                if (_mode == AdapterMode.Offline) { _xml.ChangeProduct(product, false, oldcat); }
            }
        }
        
        public List<InventoryProduct> LoadInventory()
        {
            if (_mode == AdapterMode.Online)
            {
                List<InventoryProduct> inv = _sql.LoadInventory();
                if (inv != null)
                {
                    return inv;
                }
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
            }
            return null;//_xml.LoadInventory();
        }
        public List<string> LoadInventoryTransactionTypes()
        {
           return _sql.LoadInventoryTransactionTypes();

        }
        public List<InventoryTransaction> LoadInventoryTransactions()
        {
            return _sql.LoadInventoryTransactions();
        }
        public List<InventoryProduct> LoadInventoryTransaction(int Id)
        {
            return _sql.LoadInventoryTransaction(Id);
        }
        public int SelProduct(Product product)
        {
            return _sql.SubtractProductQuantity(product);
        }
        public int BuyProduct(Product product)
        {
            return _sql.AddProductQuantity(product);
        }
        public bool UpdateOrderParameters(InventoryProduct product)
        {
            _sql.UpdateOrderParameters(product);
            if (!_sql.IsConnected)
            {
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
                //_xml.ChangeProduct(product, false, oldcat);
                OnConnectionChanged(EventArgs.Empty);
                return false;
            }
            return true;
        }
        public bool UpdateCurrentQuantity(InventoryProduct product, InventoryTransaction transaction)
        {
            _sql.UpdateCurrentQuantity(product,transaction);
            if (!_sql.IsConnected)
            {
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
                //_xml.ChangeProduct(product, false, oldcat);
                OnConnectionChanged(EventArgs.Empty);
                return false;
            }
            return true;
        }
        public bool PurchaseOrderProduct(InventoryProduct product, InventoryTransaction transaction)
        {
            _sql.PurchaseOrderProduct(product, transaction);
            if (!_sql.IsConnected)
            {
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
                //_xml.ChangeProduct(product, false, oldcat);
                OnConnectionChanged(EventArgs.Empty);
                return false;
            }
            return true;
        }
        public bool IssueTransactionProduct(Product product, InventoryTransaction transaction)
        {
            _sql.IssueTransactionProduct(product, transaction);
            if (!_sql.IsConnected)
            {
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
                //_xml.ChangeProduct(product, false, oldcat);
                OnConnectionChanged(EventArgs.Empty);
                return false;
            }
            return true;
        }
        public bool ReceiveOrderProduct(InventoryProduct product, InventoryTransaction transaction)
        {
            _sql.ReceiveOrderProduct(product, transaction);
            if (!_sql.IsConnected)
            {
                _mode = AdapterMode.Offline;
                Properties.Settings.Default.iOnlineMode = (int)_mode;
                Properties.Settings.Default.Save();
                //_xml.ChangeProduct(product, false, oldcat);
                OnConnectionChanged(EventArgs.Empty);
                return false;
            }
            return true;
        }

        #endregion
        #region Transactions

        /// <summary>
        /// Loads all the transaction with unclosed state.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadTransactions()
        {
            if (_mode == AdapterMode.Online)
            {
                return _sql.LoadTransactions();
            }
            _TtoDel = _xml.LoadRemovedTransactions();
            return _xml.LoadTransactions();
        }
        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="product">Transaction to delete</param>
        public void DeleteTransaction(Transaction t)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.DeleteTransaction(t);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        if (t.Synced == -1 || t.Synced == 1) { _TtoDel.Add(t); }
                        _xml.DeleteTransaction(t, false);
                        OnConnectionChanged(EventArgs.Empty);
                        return;
                    }
                    _xml.DeleteTransaction(t,true);
                    return;
                }
                if (t.Synced == -1 || t.Synced == 1)
                {
                    _TtoDel.Add(t);
                    _xml.DeleteTransaction(t, false);
                    return;
                }
                if (t.Synced == 0)
                {
                    _xml.DeleteTransaction(t, true);
                    return;
                }
                
                
            }
        }
        /// <summary>
        /// Adds a transaction.
        /// </summary>
        /// <param name="t">transaction to add</param>
        public void AddTransaction(Transaction t)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                {
                    _sql.AddTransaction(t);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        OnConnectionChanged(EventArgs.Empty);
                    }
                    _xml.AddTransaction(t);
                    return;
                }
                _xml.AddTransaction(t);
            }
        }
        /// <summary>
        /// Make changes to an existing transaction.
        /// </summary>
        /// <param name="t">transaction to change</param>
        public void ChangeTransaction(Transaction t)
        {
            lock (_syncLock)
            {
                if (_mode == AdapterMode.Online)
                { 
                    if (t.Openedby.Id == 0) { t.Openedby.Id = _zeroUserId; }
                    if (t.Closedby != null && t.Closedby.Id == 0) { t.Closedby.Id = _zeroUserId; }
                    _sql.ChangeTransaction(t);
                    if (_sql.IsConnected == false)
                    {
                        _mode = AdapterMode.Offline;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        _xml.ChangeTransaction(t, false);
                        OnConnectionChanged(EventArgs.Empty);
                        return;
                    }
                    _xml.ChangeTransaction(t, true);
                    return;
                }
                _xml.ChangeTransaction(t, false);
            }
        }
        /// <summary>
        /// Loads all the closed transactions in a given interval. Including beginday, excluding endday.
        /// </summary>
        /// <param name="begindate">Startdate of the interval. A day is starting at 00.00.00 time of day</param>
        /// <param name="enddate">Enddate of the interval. A day ends at 23.59.59 time of day</param>
        /// <returns></returns>
        public List<Transaction> LoadClosedTransactions(DateTime begindate, DateTime enddate)
        {
            if (_mode == AdapterMode.Online)
            {
               return _sql.LoadClosedTransactions(begindate, enddate);
            }
            return _xml.LoadClosedTransactions(begindate,enddate);

        }


        //global methods

        /// <summary>
        /// Closes all connections and saves the data. Use on app shutdown.
        /// </summary>
        public void Shutdown()
        {

            Properties.Settings.Default.iOnlineMode = (int)_mode;
            Properties.Settings.Default.Save();
            _bgworker.Stop();
            _bgworker.Close();
            _xml.Shutdown();
            _sql.Shutdown();
        }

        #endregion
        #endregion

        #region Events
        public event EventHandler ConnectionChanged;
        protected virtual void OnConnectionChanged(EventArgs e)
        {
            EventHandler handler = ConnectionChanged;
            handler?.Invoke(this, e);
        }
        public event EventHandler UpdateTransactionIds;
        protected virtual void OnUpdateTransactionIds(EventArgs e)
        {
            EventHandler handler = UpdateTransactionIds;
            handler?.Invoke(this, e);
        }
        public event EventHandler UpdateZeroUser;
        protected virtual void OnUpdateZeroUser(EventArgs e)
        {
            EventHandler handler = UpdateZeroUser;
            handler?.Invoke(this, e);
        }
        public event EventHandler UpdateZeroProduct;
        protected virtual void OnUpdateZeroProduct(EventArgs e)
        {
            EventHandler handler = UpdateZeroProduct;
            handler?.Invoke(this, e);
        }
        #endregion

        #region Helpers

        private void _bgworker_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _bgworker.Stop();
            if (_sql.IsConnected == false)
            {
                _sql.Connect();
                if (_sql.IsConnected)
                {
                    _Locked = true;
                    lock (_syncLock)
                    {
                        _Locked = true;
                        Sync();
                        _mode = AdapterMode.Online;
                        Properties.Settings.Default.iOnlineMode = (int)_mode;
                        Properties.Settings.Default.Save();
                        
                    }
                    if (_oldtids.Count > 0)
                    {
                        OnUpdateTransactionIds(EventArgs.Empty);
                        _oldtids.Clear();
                        _newtids.Clear();
                    }
                    OnConnectionChanged(EventArgs.Empty);
                    _Locked = false;
                }
            }
            else
            {
               if (! _sql.TestConnection())
                {
                    _mode = AdapterMode.Offline;
                    Properties.Settings.Default.iOnlineMode = (int)_mode;
                    Properties.Settings.Default.Save();
                    OnConnectionChanged(EventArgs.Empty);
                }
            }
            _bgworker.Start();
        }

        private void Sync()
        {
            //sync users
            List<User> xu = _xml.LoadUsers();
            List<User> su = _sql.LoadUsers();
            for (int i = 0;i < xu.Count; i++)
            {
                if (xu[i].Id == 0)
                {
                    _xml.DeleteUser(xu[i]);
                    _sql.AddUser(xu[i]);
                    _xml.AddUser(xu[i], true);
                    //sets the transactions with openedby Id = 0 to new Id in the XML file
                    XML.Instance.SetZeroUserId(xu[i].Id);
                    _zeroUserId = xu[i].Id;
                    //update user in memory of application
                    OnUpdateZeroUser(EventArgs.Empty);
                }
            }
            xu = _xml.LoadUsers();
            for (int i = 0; i < xu.Count; i++)
            {
                if (! xu[i].IsSynced)
                {
                    _sql.ChangeUser(xu[i]);
                    _xml.ChangeUser(xu[i], true);
                }
            }

            //sync products
            List<Product> xp = _xml.LoadProducts();
            List<Product> sp = _sql.LoadProducts();
            
            for (int i = 0; i < xp.Count; i++)
            {
                if (xp[i].Id == 0)
                {
                    _xml.DeleteProduct(xp[i]);
                    _sql.Addproduct(xp[i]);
                    _xml.Addproduct(xp[i],true);
                    //change products with 0 id to new id
                    XML.Instance.SetZeroProductId(xp[i].Id);
                    _zeroProductId = xp[i].Id;
                    //update user in memory of application
                    OnUpdateZeroProduct(EventArgs.Empty);
                }
                    
            }
            xp = _xml.LoadProducts();
            for (int i = 0; i < xp.Count; i++)
            {
                if (!xp[i].IsSynchronized)
                {
                    _sql.ChangeProduct(xp[i]);
                    _xml.ChangeProduct(xp[i], true, xp[i].Category);
                }
            }

            //sync transactions
            foreach (Transaction t in _TtoDel)
            {
                _sql.DeleteTransaction(t);
                _xml.DeleteTransaction(t, true);
            }
            _TtoDel.Clear();
            List<Transaction> xt = _xml.LoadAllTransactions();
            for (int i = 0; i < xt.Count; i++)
            {
                if (xt[i].Synced == -1)
                {
                    _sql.ChangeTransaction(xt[i]);
                    _xml.ChangeTransaction(xt[i], true);
                }
                if (xt[i].Synced == 0)
                {
                    try
                    {
                        _xml.DeleteTransaction(xt[i], true);
                        if (xt[i].State != TransactionState.Closed && xt[i].State != TransactionState.Booked) { _oldtids.Add(xt[i].Id); }
                        _sql.AddTransaction(xt[i]);
                        _sql.ChangeTransaction(xt[i]);
                        _xml.AddTransaction(xt[i]);
                        _xml.ChangeTransaction(xt[i], true);
                        if (xt[i].State != TransactionState.Closed && xt[i].State != TransactionState.Booked) { _newtids.Add(xt[i].Id); }
                    }
                    catch
                    {
                        ;
                    }
                }
            }


        }
  
        #endregion
    }
}
public enum AdapterMode
{
   None, Online, Offline
}
