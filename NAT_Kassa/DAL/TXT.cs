﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace NAT_Kassa.DAL
{
    class TXT
    {

        #region Fields
        
        #endregion

        #region Constructor
        // Singleton class by privatising the constructor
        //constructor must only happen once
        private TXT()
        {
            //_wr = new StreamWriter(Properties.Settings.Default.sXMLPath + "DrawerLogs.csv");
        }
        //Singleton instance
        private static readonly TXT _instance = new TXT();

        //Instance property for access to XML Class
        public static TXT Instance { get { return _instance; } }
        #endregion

        public void LogDrawerAction(DrawerAction action)
        {
            using (StreamWriter outputFile = new StreamWriter(Properties.Settings.Default.sXMLPath + "DrawerLogs.csv",true))
            {
                string output = action.Timestamp.ToString() + ";" + action.Action + ";" + action.User + ";";
                switch (action.Action)
                {
                    case "Betaling":
                        output += action.OnHandAmount.ToString("F2")+ ";" + action.DepositAmount.ToString("F2") + ";" + action.NewAmount.ToString("F2");
                        break;
                    case "Uitname":
                        output += action.OnHandAmount.ToString("F2") + ";-" + action.WithdrawAmount.ToString("F2") + ";" + action.NewAmount.ToString("F2");
                        break;
                    case "Telling":
                        output += action.OnHandAmount.ToString("F2") + ";" + action.CountedAmount.ToString("F2") + ";" + action.NewAmount.ToString("F2");
                        break;
                    case "Openen":
                        output += action.OnHandAmount.ToString("F2") + ";0,0;" + action.OnHandAmount.ToString("F2");
                        break;
                    default:
                        output += "Ongeldige entry;;";
                        break;
                }
                output += ";" + action.Valid + ";";
                outputFile.WriteLine(output);
            }
        }
    }
}
