﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NAT_Kassa.BLL;
namespace NAT_Kassa.DAL
{
    /// <summary>
    /// Used for data access to XML based storage of app data.
    /// Interfaces a Users, Products, Organizations and Settings File in a shared directory.
    /// This is a Singleton class. Use Instance property.
    /// </summary>
    public class XML
    {
        #region Fields

        private XmlDocument _users = new XmlDocument();
        private XmlDocument _products = new XmlDocument();
        private XmlDocument _settings = new XmlDocument();
        private XmlDocument _organizations = new XmlDocument();
        private XmlDocument _transactions = new XmlDocument();
        private string _dir = "";
        
        //debug
        private int _NodesAdded;
        private int _NodeErrors;
        private int _PossibleAds;
        private List<string> _Errors = new List<string>();
        #endregion
        #region Constructors
        // Singleton class by privatising the constructor
        //constructor must only happen once
        private XML()
        {
            _dir = Properties.Settings.Default.sXMLPath;
            Load();  
        }
        //Singleton instance
        private static readonly XML _instance = new XML();

        //Instance property for access to XML Class
        public static XML Instance { get { return _instance; } }

        public int NodesAdded { get => _NodesAdded; set => _NodesAdded = value; }
        public int NodeErrors { get => _NodeErrors; set => _NodeErrors = value; }
        public int PossibleAds { get => _PossibleAds; set => _PossibleAds = value; }
        public List<string> Errors { get => _Errors; set => _Errors = value; }

        #endregion
        #region Properties

        #endregion

        #region Methods


        //users

        /// <summary>
        /// Loads all the users from the Users.xml file.
        /// </summary>
        /// <returns>List of type User</returns>
        public List<User> LoadUsers()
        {
            XmlNodeList nodes;
            nodes = _users.LastChild.ChildNodes;
            List<User> userpool = new List<User>();
            for (int i = 0; i < nodes.Count; i++)
            {
                XmlNode node = nodes.Item(i);
                userpool.Add(new User(int.Parse(node.ChildNodes[7].InnerText),node.ChildNodes.Item(0).InnerText,
                    node.ChildNodes.Item(1).InnerText,
                    node.ChildNodes.Item(2).InnerText,
                    node.ChildNodes.Item(3).InnerText,
                    (OrganizationID)Enum.Parse(typeof(OrganizationID), node.ChildNodes.Item(4).InnerText),
                    (Userlevel)Enum.Parse(typeof(Userlevel), node.ChildNodes.Item(5).InnerText),
                    node.ChildNodes.Item(6).InnerText));
                userpool.Last().IsActive = bool.Parse(node.ChildNodes[8].InnerText);
                userpool.Last().IsSynced = bool.Parse(node.ChildNodes[9].InnerText);
            }
            return userpool;
        }
        /// <summary>
        /// Deletes a user from the XML. Simple deletion, no extra checks executed, these should already be done in UserModule.
        /// </summary>
        /// <param name="user">User to delete</param>
        public void DeleteUser(User user)
        {
            XmlNodeList nodes;
            nodes = _users.LastChild.ChildNodes;
            for (int i = 0; i < nodes.Count; i++)
            {
               if (nodes[i].ChildNodes[7].InnerText == user.Id.ToString())
                {
                    _users.LastChild.RemoveChild(nodes[i]);
                    break;
                }
            }
            _users.Save(_dir + "Users.xml");
        }
        /// <summary>
        /// Adds a new user to the file.
        /// </summary>
        /// <param name="user">user to add</param>
        public void AddUser(User user,bool isconnected)
        {
            XmlNode masternode = _users.LastChild;
            XmlNode newuser = masternode.LastChild.Clone();
            newuser.ChildNodes[0].InnerText = user.Name;
            newuser.ChildNodes[1].InnerText = user.Surname;
            newuser.ChildNodes[2].InnerText = user.Address;
            newuser.ChildNodes[3].InnerText = user.Email;
            newuser.ChildNodes[4].InnerText = user.OrganizationID.ToString();
            newuser.ChildNodes[5].InnerText = user.Userlevel.ToString();
            newuser.ChildNodes[6].InnerText = user.Password;
            //user.Id = int.Parse(masternode.LastChild.ChildNodes[7].InnerText) + 1;
            newuser.ChildNodes[7].InnerText = user.Id.ToString();
            newuser.ChildNodes[8].InnerText = user.IsActive.ToString();
            newuser.ChildNodes[9].InnerText = isconnected.ToString();
            masternode.AppendChild(newuser);
            _users.Save(_dir + "Users.xml");
        }
        /// <summary>
        /// Changes a user in the XML file.
        /// </summary>
        /// <param name="user">user to change. Selects the corrsponding user based on Id.</param>
        /// <param name="isconnected">Has the change been done online or offline. If offline change user wil be set to unsync state</param>
        public void ChangeUser(User user, bool isconnected)
        {
            XmlNodeList nodes;
            nodes = _users.LastChild.ChildNodes;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].ChildNodes[7].InnerText == user.Id.ToString())
                {
                    nodes[i].ChildNodes[0].InnerText = user.Name;
                    nodes[i].ChildNodes[1].InnerText = user.Surname;
                    nodes[i].ChildNodes[2].InnerText = user.Address;
                    nodes[i].ChildNodes[3].InnerText = user.Email;
                    nodes[i].ChildNodes[4].InnerText = user.OrganizationID.ToString();
                    nodes[i].ChildNodes[5].InnerText = user.Userlevel.ToString();
                    nodes[i].ChildNodes[6].InnerText = user.Password;
                    //nodes[i].ChildNodes[7].InnerText = user.Id.ToString();
                    nodes[i].ChildNodes[8].InnerText = user.IsActive.ToString();
                    nodes[i].ChildNodes[9].InnerText = isconnected.ToString();
                    break;
                }
            }
            _users.Save(_dir + "Users.xml");
        }
        
        //products

        /// <summary>
        /// Loads all the products from the Products.xml file.
        /// </summary>
        /// <returns>List of type Product</returns>
        public List<Product> LoadProducts()
        {
            XmlNodeList categorynodes;
            categorynodes = _products.LastChild.ChildNodes;
            List<Product> productpool = new List<Product>();
            for (int i = 1; i < categorynodes.Count; i++)
            {
                string categoryattribute = categorynodes[i].Attributes[0].Value;
                for (int j = 0; j < categorynodes[i].ChildNodes.Count; j++)
                {
                    XmlNode productnode = categorynodes[i].ChildNodes[j];

                    productpool.Add(new Product(productnode.ChildNodes[0].InnerText,
                        double.Parse(productnode.ChildNodes[1].InnerText),
                        double.Parse(productnode.ChildNodes[3].InnerText),
                        (ProductCategory)Enum.Parse(typeof(ProductCategory), categoryattribute),
                        int.Parse(productnode.ChildNodes[2].InnerText),
                        1, bool.Parse(productnode.ChildNodes[5].InnerText),
                        bool.Parse(productnode.ChildNodes[6].InnerText)));
                }
            }
            return productpool;
        }
        /// <summary>
        /// Deletes a product from the XML. Simple deletion, no extra checks executed
        /// </summary>
        /// <param name="product">Product to delete</param>
        public void DeleteProduct(Product product)
        {
            XmlNodeList nodes;
            nodes = _products.LastChild.ChildNodes;
            for (int i = 1; i < nodes.Count; i++)
            {
                if (nodes[i].Attributes[0].Value == product.Category.ToString())
                {

                    for (int j = 0; j < nodes[i].ChildNodes.Count; j++)
                    {
                        if (nodes[i].ChildNodes[j].ChildNodes[2].InnerText == product.Id.ToString())
                        {
                            nodes[i].RemoveChild(nodes[i].ChildNodes[j]);
                            break;
                        }
                    }
                    break;
                }
            }
            _products.Save(_dir + "Products.xml");
        }

        private void DeleteProductOnChange(Product product, BLL.ProductCategory oldcat)
        {
            XmlNodeList nodes;
            nodes = _products.LastChild.ChildNodes;
            for (int i = 1; i < nodes.Count; i++)
            {
                if (nodes[i].Attributes[0].Value == oldcat.ToString())
                {

                    for (int j = 0; j < nodes[i].ChildNodes.Count; j++)
                    {
                        if (nodes[i].ChildNodes[j].ChildNodes[2].InnerText == product.Id.ToString())
                        {
                            nodes[i].RemoveChild(nodes[i].ChildNodes[j]);
                            break;
                        }
                    }
                    break;
                }
            }
            _products.Save(_dir + "Products.xml");
        }
        /// <summary>
        /// Adds a product to the XML file
        /// </summary>
        /// <param name="product">product to add</param>
        public void Addproduct(Product product, bool isconnected)
        {
            XmlNode masternode = _products.LastChild;
            XmlNode catnode = masternode.ChildNodes[0];
            for (int i = 1; i < masternode.ChildNodes.Count; i++)
            {
                if (masternode.ChildNodes[i].Attributes[0].Value == product.Category.ToString())
                {
                    catnode = masternode.ChildNodes[i];
                    break;
                }
            }

            XmlNode newproduct = masternode.ChildNodes[0].ChildNodes[0].Clone();
            newproduct.ChildNodes[0].InnerText = product.Name;
            newproduct.ChildNodes[1].InnerText = product.Price.ToString();
            newproduct.ChildNodes[2].InnerText = product.Id.ToString();
            newproduct.ChildNodes[3].InnerText = product.Purchaseprice.ToString();
            newproduct.ChildNodes[4].InnerText = product.IsAvailable.ToString();
            newproduct.ChildNodes[5].InnerText = product.IsActive.ToString();
            newproduct.ChildNodes[6].InnerText = isconnected.ToString();
            catnode.AppendChild(newproduct);
            _products.Save(_dir + "Products.xml");
        }
        /// <summary>
        /// Changes a product in the XML file.
        /// </summary>
        /// <param name="product">product to change. Selects the corrsponding product based on Id.</param>
        /// <param name="isconnected">Has the change been done online or offline. If offline change user wil be set to unsync state</param>
        public void ChangeProduct(Product product, bool isconnected, BLL.ProductCategory oldcat)
        {
            DeleteProductOnChange(product, oldcat);
            Addproduct(product, isconnected);
        }

        //transaction 

        /// <summary>
        /// Loads all the transaction with open or paidpartial state from the Transaction.xml file.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadTransactions()
        {
            List<User> users = LoadUsers();
            List<Product> prods = LoadProducts();
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            List<Transaction> transactionpool = new List<Transaction>();
            for (int i = 0; i < tnodes.Count; i++)
            {
                
                if (tnodes[i].ChildNodes[4].InnerText == TransactionState.Open.ToString()
                 || tnodes[i].ChildNodes[4].InnerText == TransactionState.PaidPartial.ToString()
                 || tnodes[i].ChildNodes[4].InnerText == TransactionState.PaidFull.ToString())
                {
                    Transaction t = new Transaction(int.Parse(tnodes[i].ChildNodes[0].InnerText),
                         GetUser(users, int.Parse(tnodes[i].ChildNodes[6].ChildNodes[2].InnerText)),
                        tnodes[i].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[2])
                    {
                        
                        Product p = new Product(pnode.ChildNodes[0].InnerText, double.Parse(pnode.ChildNodes[1].InnerText), double.Parse(pnode.ChildNodes[3].InnerText),
                            GetProduct(prods, int.Parse(pnode.ChildNodes[4].InnerText)).Category, int.Parse(pnode.ChildNodes[4].InnerText),
                            int.Parse(pnode.ChildNodes[2].InnerText),true,true);
                        t.AddProduct(p);
                    }
                    //t.State = (TransactionState)Enum.Parse(typeof(TransactionState), tnodes[i].ChildNodes[4].InnerText);
                    t.OpenedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[6].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[5])
                    {
                        Payment p = new Payment(int.Parse(pnode.ChildNodes[0].InnerText)
                            , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), pnode.ChildNodes[1].InnerText)
                            , double.Parse(pnode.ChildNodes[2].InnerText));
                        p.TransactionTimestamp = DateTime.Parse(pnode.ChildNodes[3].InnerText);
                        t.Pay(p);
                    }
                    t.Synced = int.Parse(tnodes[i].ChildNodes[8].InnerText);
                    if(t.Synced != -2) {transactionpool.Add(t); }
                }
            }
            return transactionpool;
        }
        /// <summary>
        /// Loads all the transactions that are not removed permanently from the Transaction.xml file.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadRemovedTransactions()
        {
            List<User> users = LoadUsers();
            List<Product> prods = LoadProducts();
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            List<Transaction> transactionpool = new List<Transaction>();
            for (int i = 0; i < tnodes.Count; i++)
            {

                if (tnodes[i].ChildNodes[8].InnerText == "-2")
                {
                    Transaction t = new Transaction(int.Parse(tnodes[i].ChildNodes[0].InnerText),
                         GetUser(users, int.Parse(tnodes[i].ChildNodes[6].ChildNodes[2].InnerText)),
                        tnodes[i].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[2])
                    {
                        Product p = new Product(pnode.ChildNodes[0].InnerText, double.Parse(pnode.ChildNodes[1].InnerText), double.Parse(pnode.ChildNodes[3].InnerText),
                            GetProduct(prods, int.Parse(pnode.ChildNodes[4].InnerText)).Category, int.Parse(pnode.ChildNodes[4].InnerText),
                            int.Parse(pnode.ChildNodes[2].InnerText), true, true);
                        t.AddProduct(p);
                    }
                    //t.State = (TransactionState)Enum.Parse(typeof(TransactionState), tnodes[i].ChildNodes[4].InnerText);
                    t.OpenedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[6].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[5])
                    {
                        Payment p = new Payment(int.Parse(pnode.ChildNodes[0].InnerText)
                            , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), pnode.ChildNodes[1].InnerText)
                            , double.Parse(pnode.ChildNodes[2].InnerText));
                        p.TransactionTimestamp = DateTime.Parse(pnode.ChildNodes[3].InnerText);
                        t.Pay(p);
                    }
                    t.Synced = int.Parse(tnodes[i].ChildNodes[8].InnerText);
                    transactionpool.Add(t);
                }
            }
            return transactionpool;
        }
        /// <summary>
        /// Loads all the closed transactions in a given interval. Including beginday, excluding endday.
        /// </summary>
        /// <param name="begindate">Startdate of the interval. A day is starting at 00.00.00 time of day</param>
        /// <param name="enddate">Enddate of the interval. A day ends at 23.59.59 time of day</param>
        /// <returns></returns>
        public List<Transaction> LoadClosedTransactions(DateTime begindate, DateTime enddate)
        {
            List<User> users = LoadUsers();
            List<Product> prods = LoadProducts();
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            List<Transaction> transactionpool = new List<Transaction>();
            for (int i = 0; i < tnodes.Count; i++)
            {

                if (tnodes[i].ChildNodes[4].InnerText == TransactionState.Closed.ToString()
                    && DateTime.Parse(tnodes[i].ChildNodes[7].ChildNodes[1].InnerText) >= begindate
                    && DateTime.Parse(tnodes[i].ChildNodes[7].ChildNodes[1].InnerText) <= enddate)
                {
                    Transaction t = new Transaction(int.Parse(tnodes[i].ChildNodes[0].InnerText),
                         GetUser(users, int.Parse(tnodes[i].ChildNodes[6].ChildNodes[2].InnerText)),
                        tnodes[i].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[2])
                    {
                        Product p = new Product(pnode.ChildNodes[0].InnerText, double.Parse(pnode.ChildNodes[1].InnerText), double.Parse(pnode.ChildNodes[3].InnerText),
                             GetProduct(prods, int.Parse(pnode.ChildNodes[4].InnerText)).Category, int.Parse(pnode.ChildNodes[4].InnerText),
                             int.Parse(pnode.ChildNodes[2].InnerText), true, true);
                        t.AddProduct(p);
                    }
                    t.OpenedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[6].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[5])
                    {
                        Payment p = new Payment(int.Parse(pnode.ChildNodes[0].InnerText)
                            , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), pnode.ChildNodes[1].InnerText)
                            , double.Parse(pnode.ChildNodes[2].InnerText));
                        p.TransactionTimestamp = DateTime.Parse(pnode.ChildNodes[3].InnerText);
                        t.Pay(p);
                    }
                    //t.State = (TransactionState)Enum.Parse(typeof(TransactionState), tnodes[i].ChildNodes[4].InnerText);
                    t.CloseTransaction(GetUser(users, int.Parse(tnodes[i].ChildNodes[7].ChildNodes[2].InnerText)));
                    t.ClosedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[7].ChildNodes[1].InnerText);
                    t.Synced = int.Parse(tnodes[i].ChildNodes[8].InnerText);
                    transactionpool.Add(t);
                }
            }
            return transactionpool;
        }
        /// <summary>
        /// Loads all the transaction from the Transaction.xml file. Use to init DB.
        /// </summary>
        /// <returns>List of type Transaction</returns>
        public List<Transaction> LoadAllTransactions()
        {
            List<User> users = LoadUsers();
            List<Product> prods = LoadProducts();
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            List<Transaction> transactionpool = new List<Transaction>();
            for (int i = 0; i < tnodes.Count; i++)
            {

                if (tnodes[i].ChildNodes[4].InnerText == TransactionState.Open.ToString()
                 || tnodes[i].ChildNodes[4].InnerText == TransactionState.PaidPartial.ToString()
                 || tnodes[i].ChildNodes[4].InnerText == TransactionState.PaidFull.ToString()
                 || tnodes[i].ChildNodes[4].InnerText == TransactionState.Closed.ToString())
                {
                    Transaction t = new Transaction(int.Parse(tnodes[i].ChildNodes[0].InnerText),
                        GetUser(users, int.Parse(tnodes[i].ChildNodes[6].ChildNodes[2].InnerText)),
                        tnodes[i].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[2])
                    {
                        Product p = new Product(pnode.ChildNodes[0].InnerText, double.Parse(pnode.ChildNodes[1].InnerText), double.Parse(pnode.ChildNodes[3].InnerText),
                            GetProduct(prods, int.Parse(pnode.ChildNodes[4].InnerText)).Category, int.Parse(pnode.ChildNodes[4].InnerText),
                            int.Parse(pnode.ChildNodes[2].InnerText), true, true);
                        t.AddProduct(p);
                    }
                    t.OpenedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[6].ChildNodes[1].InnerText);
                    foreach (XmlNode pnode in tnodes[i].ChildNodes[5])
                    {
                        Payment p = new Payment(int.Parse(pnode.ChildNodes[0].InnerText)
                            , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), pnode.ChildNodes[1].InnerText)
                            , double.Parse(pnode.ChildNodes[2].InnerText));
                        p.TransactionTimestamp = DateTime.Parse(pnode.ChildNodes[3].InnerText);
                        t.Pay(p);
                    }
                    if (tnodes[i].ChildNodes[4].InnerText == TransactionState.Closed.ToString())
                    {
                        t.CloseTransaction(GetUser(users, int.Parse(tnodes[i].ChildNodes[7].ChildNodes[2].InnerText)));
                        t.ClosedTimestamp = DateTime.Parse(tnodes[i].ChildNodes[7].ChildNodes[1].InnerText);
                    }
                    t.Synced = int.Parse(tnodes[i].ChildNodes[8].InnerText);
                    transactionpool.Add(t);
                }
            }
            return transactionpool;
        }
        /// <summary>
        /// Deletes a Transaction from the XML. Simple deletion, no extra checks executed
        /// </summary>
        /// <param name="product">Transaction to delete</param>
        public void DeleteTransaction(Transaction t, bool removepermanent)
        {
            XmlNodeList nodes;
            nodes = _transactions.LastChild.ChildNodes;
            for (int i = 1; i < nodes.Count; i++)
            {
                if (nodes[i].ChildNodes[0].InnerText == t.Id.ToString())
                {
                    if (removepermanent)
                    {
                        _transactions.LastChild.RemoveChild(nodes[i]);
                    }
                    else
                    {
                        t.Synced = -2;//to remove int
                        nodes[i].ChildNodes[8].InnerText = t.Synced.ToString();
                    }
                    break;
                }
            }
            _transactions.Save(_dir + "Transactions.xml");
        }
        /// <summary>
        /// Adds a transaction to XML file
        /// </summary>
        /// <param name="t"></param>
        public void AddTransaction(Transaction t)
        {
            XmlNode masternode = _transactions.LastChild;
            XmlNode tnode = MakeTransactionNode(masternode, t);
            masternode.AppendChild(tnode);
            _transactions.Save(_dir + "Transactions.xml");
        }
        /// <summary>
        /// Make changes to an existing transaction and saves changes.
        /// </summary>
        /// <param name="t">transaction to change</param>
        /// <param name="synchronized"> is synchronized with DB or not</param>
        public void ChangeTransaction(Transaction t, bool synchronized)
        {
            XmlNode masternode = _transactions.LastChild;
            XmlNode tnode = MakeTransactionNode(masternode, t);
            for (int i = masternode.ChildNodes.Count - 1; i >= 0; i--)
            {
                if(masternode.ChildNodes[i].ChildNodes[0].InnerText == t.Id.ToString())
                {
                   if (synchronized)
                    {
                        tnode.ChildNodes[8].InnerText = "1";
                        t.Synced = 1;
                    }
                    else
                    {
                        if (masternode.ChildNodes[i].ChildNodes[8].InnerText == "1" || masternode.ChildNodes[i].ChildNodes[8].InnerText == "-1")
                        {
                            tnode.ChildNodes[8].InnerText = "-1";
                            t.Synced = -1;
                        }
                        else
                        {
                            tnode.ChildNodes[8].InnerText = "0";
                            t.Synced = 0;
                        }
                    }
                   masternode.ReplaceChild(tnode, masternode.ChildNodes[i]);
                   break;
                }
            }
            _transactions.Save(_dir + "Transactions.xml");
        }
        /// <summary>
        /// Gets the transaction and payment counters. Use on startup or TransactionModule load.
        /// </summary>
        /// <returns>integer array. 0 is transaction, 1 is payment</returns>
        public int[] TransactionCounters()
        {
            int[] counters = new int[2];
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            counters[0] = int.Parse(tnodes[tnodes.Count - 1].ChildNodes[0].InnerText) + 1;
            counters[1] = int.Parse(tnodes[0].ChildNodes[5].LastChild.ChildNodes[0].InnerText) + 1;
            return counters;
        }
        /// <summary>
        /// Sets the paymentcounter
        /// </summary>
        /// <param name="counter">value to set the counter to</param>
        public void SetPaymentCounter(int counter)
        {
            XmlNodeList tnodes = _transactions.LastChild.ChildNodes;
            tnodes[0].ChildNodes[5].LastChild.ChildNodes[0].InnerText = counter.ToString();
            _transactions.Save(_dir + "Transactions.xml");
        }

        public void SetZeroUserId(int newId)
        {
            //update transactions file;
            XmlNodeList tnodes;
            tnodes = _transactions.LastChild.ChildNodes;
            for (int i = 1; i < tnodes.Count; i++)
            {
                XmlNode node = tnodes.Item(i);
                if (node.ChildNodes[6].ChildNodes.Count >= 3)
                {
                    string username = node.ChildNodes[6].ChildNodes[0].InnerText;
                    string uid = node.ChildNodes[6].ChildNodes[2].InnerText;
                    if (username != "" && uid == "0")
                    {
                        node.ChildNodes[6].ChildNodes[2].InnerText = newId.ToString();
                    }
                }
                if (node.ChildNodes[7].ChildNodes.Count >= 3)
                {
                    string username = node.ChildNodes[7].ChildNodes[0].InnerText;
                    string uid = node.ChildNodes[7].ChildNodes[2].InnerText;
                    if (username != "" && uid == "0")
                    {
                        node.ChildNodes[7].ChildNodes[2].InnerText = newId.ToString();
                    }
                }
            }
            _transactions.Save(_dir + "Transactions.xml");

        }
        public void SetZeroProductId(int newId)
        {
            //update transactions file;
            XmlNodeList tnodes;
            tnodes = _transactions.LastChild.ChildNodes;
            for (int i = 1; i < tnodes.Count; i++)
            {
                XmlNode node = tnodes.Item(i);
                XmlNode pnodes = node.ChildNodes[2];
                for (int p = 0; p < pnodes.ChildNodes.Count; p++)
                {
                    XmlNode pnode = pnodes.ChildNodes[p];
                    if (pnode.ChildNodes.Count >= 5)
                    {
                        string uid = pnode.ChildNodes[4].InnerText;
                        if (uid == "0")
                        {
                            pnode.ChildNodes[4].InnerText = newId.ToString();
                        }
                    }
                }
            }
            _transactions.Save(_dir + "Transactions.xml");

        }
        //global methods

        /// <summary>
        /// Save the files on shutdown.
        /// </summary>
        public void Shutdown()
        {
            _users.Save(_dir + "Users.xml");
            _products.Save(_dir + "Products.xml");
            _transactions.Save(_dir + "Transactions.xml");
        }

        //debug methods
        public void UpdateUserFileLayout()
        {
            //update users file
            XmlNodeList unodes;
            unodes = _users.LastChild.ChildNodes;
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            for (int i = 0; i < unodes.Count; i++)
            {
                XmlNode node = unodes.Item(i);
                if (node.ChildNodes.Count < 10)
                {
                    _PossibleAds++;
                    _NodesAdded++;
                    XmlElement el = _users.CreateElement("Active");
                    el.InnerText = "True";
                    node.AppendChild(el);
                    el = _users.CreateElement("Syncronized");
                    el.InnerText = "True";
                    node.AppendChild(el);
                }
            }
            _users.Save(_dir + "Users.xml");
        }
        public void UpdateProductsFileLayout()
        {
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            XmlNodeList catnodes = _products.LastChild.ChildNodes;
            for (int i = 1; i < catnodes.Count; i++)
            {
                XmlNode catnode = catnodes.Item(i);
                for (int p = 0; p < catnode.ChildNodes.Count; p++)
                {
                    XmlNode pnode = catnode.ChildNodes[p];

                    if (pnode.ChildNodes.Count < 7)
                    {
                        PossibleAds++;
                        XmlElement el = _products.CreateElement("Available");
                        el.InnerText = "True";
                        pnode.AppendChild(el);
                        el = _products.CreateElement("Active");
                        el.InnerText = "True";
                        pnode.AppendChild(el);
                        el = _products.CreateElement("Syncronized");
                        el.InnerText = "True";
                        pnode.AppendChild(el);
                        _NodesAdded++;
                    }
                }
            }
            _products.Save(_dir + "Products.xml");
        }
        public void UpdateTransactionsFileLayoutU()
        { 
            //update transactions file;
            List<User> users = LoadUsers();
            XmlNodeList tnodes;
            tnodes = _transactions.LastChild.ChildNodes;
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            _Errors.Clear();
            for (int i = 1; i < tnodes.Count; i++)
            {
                XmlNode node = tnodes.Item(i);
                if (node.ChildNodes[6].ChildNodes.Count < 3)
                {
                    _PossibleAds++;
                    XmlElement newchild = _transactions.CreateElement("Id");
                    string username = node.ChildNodes[6].ChildNodes[0].InnerText;
                    if ( username != "")
                    {
                        if (GetUser(users, username) != null)
                        {
                            _NodesAdded++;
                            newchild.InnerText = GetUser(users, username).Id.ToString();
                            node.ChildNodes[6].AppendChild(newchild);
                        }
                        else
                        {
                            _NodeErrors++;
                            _Errors.Add("Transaction: " + node.ChildNodes[0].InnerText + " unknown user " + username);
                        }
                    }
                    else
                    {
                        _NodeErrors++;
                        _Errors.Add("Transaction: " + node.ChildNodes[0].InnerText + "empty openend user ");
                    }
                    
                    newchild = _transactions.CreateElement("Id");
                    username = node.ChildNodes[7].ChildNodes[0].InnerText;
                    if (username != "")
                    {
                        if (GetUser(users, username) != null)
                        {
                            newchild.InnerText = GetUser(users, username).Id.ToString();
                            node.ChildNodes[7].AppendChild(newchild);
                        }
                        else
                        {
                            node.ChildNodes[7].AppendChild(newchild);
                        }
                    }
                    
                }
            }
            _transactions.Save(_dir + "Transactions.xml");

        }
        public void UpdateTransactionsFileLayoutP()
        {
            //update transactions file;
            List<Product> prods = LoadProducts();
            XmlNodeList tnodes;
            tnodes = _transactions.LastChild.ChildNodes;
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            _Errors.Clear();
            for (int i = 1; i < tnodes.Count; i++)
            {
                XmlNode node = tnodes.Item(i);
                XmlNode pnodes = node.ChildNodes[2];
                for (int p = 0; p < pnodes.ChildNodes.Count; p++)
                {
                    XmlNode pnode = pnodes.ChildNodes[p];
                    if (pnode.ChildNodes.Count < 5)
                    {
                        _PossibleAds++;
                        string prodname = pnode.ChildNodes[0].InnerText;
                        Product prod = GetProduct(prods, prodname);
                        if (prod != null)
                        {
                            _NodesAdded++;
                            XmlElement el = _transactions.CreateElement("PPrice");
                            el.InnerText = prod.Purchaseprice.ToString();
                            pnode.AppendChild(el);
                            el = _transactions.CreateElement("Id");
                            el.InnerText = prod.Id.ToString();
                            pnode.AppendChild(el);
                        }
                        else
                        {
                            _NodeErrors++;
                            _Errors.Add("Transaction: " + node.ChildNodes[0].InnerText + " unknown product" + prodname);
                        }
                    }
                }
            }
            _transactions.Save(_dir + "Transactions.xml");
        }

        public void UpdateUsersFromDB()
        {
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            _Errors.Clear();
            List<User> su = SQL.Instance.LoadUsers();
            foreach (User u in su)
            {
                _NodesAdded++;
                _PossibleAds++;
                DeleteUser(u);
                AddUser(u,true);
            }
            List<User> tempU = LoadUsers();
            _NodeErrors = tempU.Count - su.Count;
            _Errors.Add("Errorcount are users that dont exist in DB and need to be deleted from file.");
            _users.Save(_dir + "Users.xml");
        }
        public void UpdateProductFromDB()
        {
            _NodesAdded = 0;
            _PossibleAds = 0;
            _NodeErrors = 0;
            _Errors.Clear();
            List<Product> pu = SQL.Instance.LoadProducts();

            foreach (Product p in pu)
            {
                _NodesAdded++;
                _PossibleAds++;
                DeleteProduct(p);
                Addproduct(p,true);
            }

            List<Product> tempP = LoadProducts();
            _NodeErrors = tempP.Count - pu.Count;
            _Errors.Add("Errorcount are products that dont exist in DB and need to be deleted from file.");
            _products.Save(_dir + "Products.xml");
        }
       
        public void UpdateTransactionsFromDB()
        {
           
        }
        #endregion


        #region Helpers

        /// <summary>
        /// Initial loading of the XML files
        /// </summary>
        private void Load()
        {
            try
            {
                _settings.Load(_dir + "UserSettings.xml");
                _users.Load(_dir + "Users.xml");
                _products.Load(_dir + "Products.xml");
                _organizations.Load(_dir + "Organizations.xml");
                _transactions.Load(_dir + "Transactions.xml");
            }
            catch (System.IO.FileNotFoundException)
            {
                //create a browse file dialog with xml filter. curly brackets to set properties directly instead of new (); and the dialog.prop
                Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog
                {
                    DefaultExt = ".xml",
                    Filter = "XML Files (*.xml)|*.xml",
                    Title = "Browse to the app XML files. The corresponding folder wil be selected."
                };
           
                //open the filedialog and cath result (OK, CANCEL)
                Nullable<bool> result = dialog.ShowDialog();

                //use result and file
                if (result == true)
                {
                    int indexof = dialog.FileName.LastIndexOf("\\");
                    _dir = dialog.FileName.Substring(0, indexof + 1);
                    Properties.Settings.Default.sXMLPath = _dir;
                }
                //reload the xml files
                _settings.Load(_dir + "UserSettings.xml");
                _users.Load(_dir + "Users.xml");
                _products.Load(_dir + "Products.xml");
                _organizations.Load(_dir + "Organizations.xml");
                _transactions.Load(_dir + "Transactions.xml");
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Method to make a transactionnode by cloning the first node of the xml file and changing values.
        /// </summary>
        /// <param name="masternode"> the rootnode of the xml file</param>
        /// <param name="t"> transaction that has the data for the node</param>
        /// <returns></returns>
        private XmlNode MakeTransactionNode(XmlNode masternode, Transaction t)
        {
            XmlNode tnode = masternode.FirstChild.Clone();
            if (t.Id == 0)
            {
                int[] cntrs = TransactionCounters();
                t.SetId(cntrs[0]);
                tnode.ChildNodes[8].InnerText = "0";
                t.Synced = 0;
            }
            tnode.ChildNodes[0].InnerText = t.Id.ToString();
            tnode.Attributes[0].Value = t.Id.ToString();
            tnode.ChildNodes[1].InnerText = t.Description;

            XmlNode newpclone = tnode.ChildNodes[2].FirstChild.Clone();
            tnode.ChildNodes[2].RemoveAll();
            foreach (Product p in t.Products)
            {
                XmlNode newpnode = newpclone.Clone();
                newpnode.ChildNodes[0].InnerText = p.Name;
                newpnode.ChildNodes[1].InnerText = p.Price.ToString();
                newpnode.ChildNodes[2].InnerText = p.Quantity.ToString();
                newpnode.ChildNodes[3].InnerText = p.Purchaseprice.ToString();
                newpnode.ChildNodes[4].InnerText = p.Id.ToString();
                tnode.ChildNodes[2].AppendChild(newpnode);
            }

            tnode.ChildNodes[3].InnerText = t.Amount.ToString();
            tnode.ChildNodes[4].InnerText = t.State.ToString();

            newpclone = tnode.ChildNodes[5].FirstChild.Clone();
            tnode.ChildNodes[5].RemoveAll();
            foreach (Payment p in t.Payments)
            {
                XmlNode newpnode = newpclone.Clone();
                if (p.Id == 0)
                {
                    int[] cntrs = TransactionCounters();
                    p.SetId(cntrs[1]);
                    //tnode.ChildNodes[8].InnerText = "-1";
                }
                newpnode.ChildNodes[0].InnerText = p.Id.ToString();
                newpnode.ChildNodes[1].InnerText = p.Method.ToString();
                newpnode.ChildNodes[2].InnerText = p.Amount.ToString();
                newpnode.ChildNodes[3].InnerText = p.TransactionTimestamp.ToString();
                SetPaymentCounter(p.Id);
                tnode.ChildNodes[5].AppendChild(newpnode);
            }

            tnode.ChildNodes[6].ChildNodes[0].InnerText = t.Openedby.ToString();
            tnode.ChildNodes[6].ChildNodes[1].InnerText = t.OpenedTimestamp.ToString();
            tnode.ChildNodes[6].ChildNodes[2].InnerText = t.Openedby.Id.ToString();
            if (t.Closedby != null) { tnode.ChildNodes[7].ChildNodes[0].InnerText = t.Closedby.ToString(); tnode.ChildNodes[7].ChildNodes[2].InnerText = t.Openedby.Id.ToString(); tnode.ChildNodes[7].ChildNodes[1].InnerText = t.ClosedTimestamp.ToString(); }

            return tnode;
        }
        
        private User GetUser(List<User> users, int userid)
        {
            foreach (User u in users)
            {
                if (u.Id == userid)
                {
                    return u;
                }
            }
            return null;
        }
        private User GetUser(List<User> users, string username)
        {
            foreach (User u in users)
            {
                if (u.Username == username)
                {
                    return u;
                }
            }
            return null;
        }
        private Product GetProduct(List<Product> products, int pid)
        {
            foreach (Product p in products)
            {
                if (p.Id == pid)
                {
                    return p;
                }
            }
            return null;
        }
        private Product GetProduct(List<Product> products, string pname)
        {
            foreach (Product p in products)
            {
                if (p.Name == pname)
                {
                    return p;
                }
            }
            return null;
        }
        #endregion
    }
}
