using NAT_Kassa.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Markup;
using NAT_Kassa.BLL;

namespace NAT_Kassa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields
        private bool _UseHashValidation = true;
        private bool _UseMouse;
        private BLL.UserModule _UM;
        private BLL.ProductsModule _PM;
        private BLL.TransactionsModule _TM;
        private BLL.ReportModule _RM;
        private BLL.InventoryModule _IM;
        private CashDrawer _CD;
        private TicketPrinter _TP;
        private NfcReader _RD;
        private DAL.DataAdapter _DA;
        private BLL.Transaction _selectedTransaction;
        private List<string> _notifications = new List<string>();
        private bool _xDebugMode = false;
        //--private List<Action> _RecentActions = new List<Action>();
        //MenuItem Fields
        //private DataTypes _lastCmdParam = DataTypes.None;
        private DataTypes _curCmdParam = DataTypes.None;
        private List<MenuItem> _lstMenuItemsSuper = new List<MenuItem>();
        private UI.UserWindows.CustomerDisplay _customerWindow;
        #endregion
        //Constructor on new window or start app
        public MainWindow()
        {
            //CommandBinding LogoutCommandBinding = new CommandBinding(LogoutCommand, ExecutedLogoutCommand, CanExecuteLogoutCommand);
            // attach CommandBinding to root window
            //this.CommandBindings.Add(LogoutCommandBinding);   --> deze code doet hetzelfde als in de XAML definieren van CommandBindings in CommandBindings Collection
            InitializeComponent();
            _DA = DAL.DataAdapter.Instance;
            _UM = new BLL.UserModule();
            _PM = new BLL.ProductsModule();
            _TM = new BLL.TransactionsModule();
            //_IM = new BLL.InventoryModule();
            _TP = new TicketPrinter();
            _CD = new CashDrawer();
            _RD = new NfcReader(_UseHashValidation);
            _xDebugMode = Properties.Settings.Default.xDebugMode;
            //overrides when debugmode not active
            if (_xDebugMode == false)
            {
                if (!_UseMouse) { Mouse.OverrideCursor = Cursors.None; } else { Mouse.OverrideCursor = Cursors.Arrow; }
                stbTestPopup.IsEnabled = false;
                stbTestPopup.Visibility = Visibility.Collapsed;
            }
            //overrides when debugmode active
            if (_xDebugMode)
            {
                //_RM = new BLL.ReportModule(_UM.Userpool);
                //UI.ReportWindow report = new UI.ReportWindow(_RM);
                //report.Show();
                //DBG.DebugWindow dbg = new DBG.DebugWindow();
                //dbg.ShowDialog();
                //Alphatesting();
            }
            //show customer display
            _customerWindow = new UI.UserWindows.CustomerDisplay(_RD);
            _customerWindow.Left = 2000.0;
            _customerWindow.ShowActivated = false;
            _customerWindow.Show();
            _customerWindow.WindowState = WindowState.Maximized;
            //init UI
            InitUserUI();
            _notifications.Add(_UM.Notifications.Last());
            InitProductsUI();
            _notifications.Add(_PM.Notifications.Last());
            InitTransactionsUI();
            _notifications.Add(_TM.Notifications.Last());
            lstAllNotifications.ItemsSource = null;
            stbNotifications.Text = "Click user icon in upper right corner to log in.";
            _notifications.Add(stbNotifications.Text);
            ToggleProductsUI(false);
            stbRegisterID.Text = _DA.RegisterId.ToString();
            CheckConnection(_DA.IsConnected);
            _DA.ConnectionChanged += DA_ConnectionChanged;
            //Initialise all DataContexts and ItemsSources for Data bindings
            cmbSortProducts.ItemsSource = Enum.GetValues(typeof(BLL.ComparerType)).Cast<BLL.ComparerType>();
            lstUserpool.ItemsSource = _UM.Users;
            lstProductpool.ItemsSource = _PM.Productpool;
            lstCardpool.ItemsSource = _DA.LoadCards();
            txtOnHandCash.Text = Properties.Settings.Default.CashOnHand.ToString();
            // TODO : change wpcategories to work with databinding
            //wpCategories.ItemsSource = Enum.GetNames(typeof(ProductCategory));
            //Populate menuItems that should only be visible for UserLevel Super
            _lstMenuItemsSuper.Add(_menu_Card_ChangeBalance); _lstMenuItemsSuper.Add(_menu_Card_UndoAction); _lstMenuItemsSuper.Add(_menu_Card_Remove); _lstMenuItemsSuper.Add(_menu_Settings_Validation);
            //set UI datacontext to self
            DataContext = this;
            _RD.OnAvailabilityChanged += _RD_OnAvailabilityChanged;
        }

        /// <summary>
        /// Handles the mainwindow closing. This event is raised before the actual close of the window. Shutdown the app!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (wpTransactions.Children.Count > 0)
            {
                var result = MessageBox.Show("Er staan nog onbetaalde transacties open. Laat dit niet herhaaldelijk gebeuren!\nOk om uit te loggen of af te sluiten.\nCancel om de transacties nog af te werken.", "Onbetaalde transacties", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }
            _CD.Shutdown();
            _TP.Shutdown();
            //_RM.Shutdown();
            //_IM.Shutdown();
            _TM.Shutdown();
            _PM.Shutdown();
            _UM.Shutdown();
            _DA.Shutdown();
            _RD.Shutdown();
            Properties.Settings.Default.Save();
            _customerWindow.Close();
        }

        #region UserActions

        /// <summary>
        /// Handles the register window closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Regwindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DAL.DataAdapter.Instance.IsConnected)
            {
                SetUserUI();
                InitTransactionsUI();
                stbNotifications.Text = "Open new transactions by clicking the add ticket icon in the upper left corner.";
                _notifications.Add(stbNotifications.Text);
            }
            else
            {
                SetUserUI();
                InitTransactionsUI();
                stbNotifications.Text = "Unable to get unique UserID. UserID will be set to 0. User and transactions will be updated on sync.";
                _notifications.Add(stbNotifications.Text);
            }

        }

        private void UbActiveUser_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            UI.UserLoginWindow login = new UI.UserLoginWindow(_UM.Userpool);
            login.Closing += Login_Closing;
            login.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            login.Show();
        }

        /// <summary>
        /// Handles the login window closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var loginwindow = sender as UI.UserLoginWindow;
            if (loginwindow.Password != "")
            {
                if (_UM.Login(loginwindow.ubSelectedUser.txtUserName.Text, _UM.HashPassword(loginwindow.Password)))
                {
                    SetUserUI();
                    InitTransactionsUI();
                    ToggleProductsUI(true);
                    stbNotifications.Text = "Open new transactions by clicking the add ticket icon in the upper left corner.";
                    _notifications.Add(stbNotifications.Text);
                    Visibility vis = _UM.CurrentUser.Userlevel >= Userlevel.Super ? Visibility.Visible : Visibility.Collapsed;
                    UpdateMenuItems(vis);
                    return;
                }
                SetUserUI();
            }
        }

        #endregion

        #region ProductActions

        private void ProductSold(BLL.Product product)
        {
            if (_PM.SelProduct(product) == true)
            {
                stbNotifications.Text = _PM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
            }
        }
        private void ProductReturned(BLL.Product product)
        {
            if (_PM.ReturnProduct(product) == true)
            {
                stbNotifications.Text = _PM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
            }

        }
        private void Pb_Clicked(object sender, EventArgs e)
        {
            if (_PM.ModState == BLL.ModuleState.Normal)
            {
                var pb = sender as UI.ProductButton;
                BLL.Product newproduct = new BLL.Product(pb.Product);
                newproduct.Quantity = ucCalculator.ProductCount();

                if (_selectedTransaction != null)
                {
                    _TM.AddSingleProduct(_selectedTransaction.Id, newproduct);
                    SetTransactionsUI();
                    stbNotifications.Text = _TM.Notifications.Last();
                    _notifications.Add(stbNotifications.Text);
                    ProductSold(newproduct);
                }
            }
            if (_PM.ModState == BLL.ModuleState.Deleting)
            {
                var pb = sender as UI.ProductButton;
                if (_PM.DeactivateProduct(pb.Product.Name))
                {
                    DockPanel dpCat = wpProducts.Children[(int)pb.Product.Category] as DockPanel;
                    WrapPanel wpCat = dpCat.Children[1] as WrapPanel;
                    //wpCat.Children.Remove(pb);
                    pb.Visibility = Visibility.Collapsed;
                    stbNotifications.Text = _PM.Notifications.Last();
                    _notifications.Add(stbNotifications.Text);
                }
            }
            ucCalculator.txtNumberDisplay.Text = "";
        }
        #endregion

        #region TransactionActions



        /// <summary>
        /// Handles details view change description.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Transactiondetails_DescriptionChanged(object sender, EventArgs e)
        {
            var td = sender as UI.TransactionDetailsControl;
            DAL.DataAdapter.Instance.ChangeTransaction(_selectedTransaction);
        }

        #endregion

        #region Notifications

        /// <summary>
        /// Opens and closes all notification list from current session.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnShowAllNotifications_Click(object sender, RoutedEventArgs e)
        {
            if (lstAllNotifications.ItemsSource != null)
            {
                lstAllNotifications.ItemsSource = null;
                btnShowAllNotifications.Content = "Show All";
                lstAllNotifications.Visibility = Visibility.Collapsed;
            }
            else
            {
                lstAllNotifications.ItemsSource = _notifications;
                btnShowAllNotifications.Content = "Hide All";
                lstAllNotifications.Visibility = Visibility.Visible;
            }

        }

        /// <summary>
        /// Auto close all notifications list on lostfocus.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstAllNotifications_LostFocus(object sender, RoutedEventArgs e)
        {
            lstAllNotifications.ItemsSource = null;
            btnShowAllNotifications.Content = "Show All";
            lstAllNotifications.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Handles the connection changed event from DA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DA_ConnectionChanged(object sender, EventArgs e)
        {
            stbConnected.Dispatcher.Invoke(() => { CheckConnection(DAL.DataAdapter.Instance.IsConnected); });
        }
        private void CheckConnection(bool isconnected)
        {
            if (isconnected)
            {
                InitTransactionsUI();
                _PM.Sort(_PM.CurrentSortMethod);
                SetProductsUI();
                stbConnected.Fill = new SolidColorBrush(Colors.Green);
                stbContext.Text = "ONLINE";
            }
            else
            {
                stbConnected.Fill = new SolidColorBrush((Color)Application.Current.Resources["CokeRed"]);
                stbContext.Text = "OFFLINE";
            }
        }
        /// <summary>
        /// Whenever a new card is detected -> show a popup of card details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RD_OnAvailabilityChanged(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() => {
                if (_RD.IsAvailable && _RD.IsCardConnected)
                {
                    Brush br = _RD.ConnectedCard.IsValid ? Brushes.DodgerBlue : Brushes.DarkRed;
                    NotificationPopup p = new NotificationPopup("NFC Card : " + _RD.ConnectedCard.Owner, br, "Saldo = €" + _RD.ConnectedCard.Credit.ToString("F2"));
                    p.OnRemove += Notification_Removal;
                    spNotificationPopups.Children.Add(p);
                }
            });
        }
        //When a popup can close, remove it from the container element
        private void Notification_Removal(object sender, EventArgs e)
        {
            NotificationPopup p = sender as NotificationPopup;
            Dispatcher.Invoke(() => { spNotificationPopups.Children.Remove(p); });
        }
        #endregion

        #region Visuals

        private void ToggleProductsUI(bool onoff)
        {
            wpProducts.IsEnabled = onoff;
            ucCalculator.IsEnabled = onoff;
        }
        private void SetUserUI()
        {
            ubActiveUser.txtUserName.Text = _UM.CurrentUser.Username;
            ubActiveUser.txtOrganization.Text = _UM.CurrentUser.OrganizationID.ToString();
            stbNotifications.Text = _UM.Notifications.Last();
            _notifications.Add(stbNotifications.Text);
        }
        private void InitUserUI()
        {
            ubActiveUser.txtUserName.Text = _UM.CurrentUser.Username;
            ubActiveUser.txtOrganization.Text = _UM.CurrentUser.OrganizationID.ToString();
        }
        private void SetProductsUI()
        {
            //clear all content from wpProducts
            wpProducts.Children.Clear();

            //reload cat containers
            foreach (ToggleButton btncat in wpCategories.Children)
            {
                string cat = btncat.Content.ToString();
                if (cat != "None" && cat != "All")
                {
                    DockPanel dpCat = new DockPanel();
                    WrapPanel stpCatContainer = new WrapPanel();
                    TextBlock tbcat = new TextBlock();
                    tbcat.Text = cat;
                    tbcat.FontSize = 12;
                    tbcat.Foreground = new SolidColorBrush(Colors.White);
                    dpCat.Children.Add(tbcat);
                    dpCat.Children.Add(stpCatContainer);
                    DockPanel.SetDock(tbcat, Dock.Top);
                    if (btncat.IsChecked == false)
                    {
                        dpCat.Visibility = Visibility.Collapsed;
                    }
                    wpProducts.Children.Add(dpCat);
                }

            }

            //reload product buttons
            foreach (BLL.Product p in _PM.Productpool)
            {
                UI.ProductButton pb = new UI.ProductButton(p);
                //pb.PreviewMouseDown += Pb_PreviewMouseDown;
                pb.Clicked += Pb_Clicked;
                pb.Width = 84;
                pb.Height = 70;
                DockPanel dpcat = wpProducts.Children[(int)p.Category] as DockPanel;
                WrapPanel stpcat = dpcat.Children[1] as WrapPanel;
                stpcat.Children.Add(pb);
                if (!p.IsActive) { pb.Visibility = Visibility.Collapsed; }
            }

            //hide empty categories
            foreach (DockPanel dpcat in wpProducts.Children)
            {
                WrapPanel stpcat = dpcat.Children[1] as WrapPanel;
                if (stpcat.Children.Count == 0)
                {
                    dpcat.Visibility = Visibility.Collapsed;
                    wpCategories.Children[wpProducts.Children.IndexOf(dpcat) + 1].Visibility = Visibility.Collapsed;
                }
            }
        }
        private void SetTransactionsUI()
        {
            if (spTransactionDetails.Children.Count > 0) { spTransactionDetails.Children.RemoveAt(0); }
            TransactionDetailsControl transactiondetails = new TransactionDetailsControl(_selectedTransaction);
            transactiondetails.DescriptionChanged += Transactiondetails_DescriptionChanged;
            //transactiondetails.EditDescription += Transactiondetails_EditDescription;
            spTransactionDetails.Children.Add(transactiondetails);
            _customerWindow.Transaction = _selectedTransaction;
            foreach (TransactionButton item in wpTransactions.Children)
            {
                if (item.Transaction.Id == _selectedTransaction.Id)
                {
                    item.IsSelected = true;
                }
                else
                {
                    item.IsSelected = false;
                }
            }

        }
        private void InitProductsUI()
        {
            //load categories
            ToggleButton btncat = new ToggleButton();
            btncat.Content = "All";
            btncat.Width = 100;
            btncat.Height = 26;
            btncat.Margin = new Thickness(4, 2, 4, 2);
            btncat.Style = (Style)Application.Current.Resources["FlatToggleButton"];
            btncat.IsChecked = true;
            btncat.Click += Btncat_Click;
            wpCategories.Children.Add(btncat);
            
            foreach (string cat in Enum.GetNames(typeof(BLL.ProductCategory)))
            {
                //load cat button
                btncat = new ToggleButton();
                btncat.Content = cat;
                btncat.Width = 100;
                btncat.Height = 26;
                btncat.Margin = new Thickness(4, 2, 4, 2);
                btncat.Style = (Style)Application.Current.Resources["FlatToggleButton"];
                btncat.IsChecked = cat == "None" ? false : true;
                btncat.Click += Btncat_Click;
                wpCategories.Children.Add(btncat);
                //load cat container
                if (cat != "None")
                {
                    DockPanel dpCat = new DockPanel();
                    WrapPanel stpCatContainer = new WrapPanel();
                    TextBlock tbcat = new TextBlock();
                    tbcat.Text = cat;
                    tbcat.FontSize = 12;
                    tbcat.Foreground = new SolidColorBrush(Colors.White);
                    dpCat.Children.Add(tbcat);
                    dpCat.Children.Add(stpCatContainer);
                    DockPanel.SetDock(tbcat, Dock.Top);
                    wpProducts.Children.Add(dpCat);
                }
                
            }
           
            //load product buttons
            foreach (BLL.Product p in _PM.Productpool)
            {
                
                UI.ProductButton pb = new UI.ProductButton(p);
                //pb.PreviewMouseDown += Pb_PreviewMouseDown;
                pb.Clicked += Pb_Clicked;
                pb.Width = 84;
                pb.Height = 70;
                DockPanel dpcat = wpProducts.Children[(int)p.Category] as DockPanel;
                WrapPanel stpcat = dpcat.Children[1] as WrapPanel;
                stpcat.Children.Add(pb);
                if (!p.IsActive) { pb.Visibility = Visibility.Collapsed; }
            }

            //hide empty categories
            foreach (DockPanel dpcat in wpProducts.Children)
            {
                WrapPanel stpcat = dpcat.Children[1] as WrapPanel;
                if (stpcat.Children.Count == 0)
                {
                    dpcat.Visibility = Visibility.Collapsed;
                    wpCategories.Children[wpProducts.Children.IndexOf(dpcat) + 1].Visibility = Visibility.Collapsed;
                }
            }

        } 
        private void InitTransactionsUI()
        {
            _selectedTransaction = null;
            if (spTransactionDetails.Children.Count > 0) { spTransactionDetails.Children.RemoveAt(0); }

            if (wpTransactions.Children.Count > 0) { wpTransactions.Children.Clear(); }
            foreach (BLL.Transaction t in _TM.Transactionpool)
            {
                if (_xDebugMode && _UM.CurrentUser.Userlevel == Userlevel.Super)
                {
                    TransactionButton tb = new TransactionButton(t);
                    wpTransactions.Children.Add(tb);
                    _selectedTransaction = tb.Transaction;
                    continue;
                }
                if (t.Openedby.Id == _UM.CurrentUser.Id)
                {
                    TransactionButton tb = new TransactionButton(t);
                    wpTransactions.Children.Add(tb);
                    _selectedTransaction = tb.Transaction;
                }
            }
            if (_selectedTransaction != null)
            {
                UI.TransactionDetailsControl transactiondetails = new UI.TransactionDetailsControl(_selectedTransaction);
                transactiondetails.DescriptionChanged += Transactiondetails_DescriptionChanged;
                //transactiondetails.EditDescription += Transactiondetails_EditDescription;
                spTransactionDetails.Children.Add(transactiondetails);
                _customerWindow.Transaction = _selectedTransaction;
                foreach (TransactionButton item in wpTransactions.Children)
                {
                    if (item.Transaction.Id == _selectedTransaction.Id)
                    {
                        item.IsSelected = true;
                        break;
                    }
                }
            }
            else { _customerWindow.Transaction = new Transaction(0,_UM.CurrentUser,"No transaction selected"); }
            wpTransactions.IsEnabled = true;
            spTransactionDetails.IsEnabled = true;

        }
        //MenuItems
        private void UpdateMenuItems(Visibility visibility) { foreach (MenuItem item in _lstMenuItemsSuper) { item.Visibility = visibility;} }

        #endregion

        #region CommonControls

        /// <summary>
        /// Handles paywindow closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PayWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var paywindow = sender as UI.PayWindow;
            if (paywindow.Cancel == false)
            {
                if (paywindow.Method == PaymentMethod.MemberCard)
                {
                    if (_RD.Pay(paywindow.Amount))
                    {
                        _TM.PayTransaction(_selectedTransaction.Id, paywindow.Amount, paywindow.Method);
                        stbNotifications.Text = _TM.Notifications.Last();
                        _notifications.Add(stbNotifications.Text);
                        SetTransactionsUI();
                        _DA.UpdateCardSaldo(_RD.ConnectedCard);
                        stbNotifications.Text = "Payment with customer card " + _RD.ConnectedCard.UID + " successful. Current balance = €" + _RD.ConnectedCard.Credit.ToString("F2");
                        _notifications.Add(stbNotifications.Text);
                        _selectedTransaction.Payments.Last().CreditAfter = _RD.ConnectedCard.Credit;
                        _customerWindow.NewAmount = _RD.ConnectedCard.Credit;
                        lstCardpool.ItemsSource = _DA.LoadCards();
                        return;
                    }
                    else
                    {
                        stbNotifications.Text = "Fout tijdens betaling met NFC kaart, kaart afnemen en probeer opnieuw! " + _RD.Error;
                        _notifications.Add(stbNotifications.Text);
                        return;
                    }
                }
                if (paywindow.Method == PaymentMethod.Cash)
                {
                    DrawerAction action = new DrawerAction()
                    {
                        Action = "Betaling",
                        User = _UM.CurrentUser.Username,
                        OnHandAmount = Properties.Settings.Default.CashOnHand,
                        DepositAmount = paywindow.Amount,
                        Timestamp = DateTime.Now
                    };
                    action.Valid = "NA";
                    action.NewAmount = action.OnHandAmount + action.DepositAmount;
                    DAL.TXT.Instance.LogDrawerAction(action);
                    Properties.Settings.Default.CashOnHand = action.NewAmount;
                    txtOnHandCash.Text = action.NewAmount.ToString("F2");
                }
                _TM.PayTransaction(_selectedTransaction.Id, paywindow.Amount, paywindow.Method);
                stbNotifications.Text = _TM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
                SetTransactionsUI();
            }

        }

        /// <summary>
        /// Handles closing of the split transaction window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SplitWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var splitwindow = sender as UI.SplitTransactionWindow;
            if (splitwindow.Cancel == false)
            {
                _TM.SplitTransaction(_selectedTransaction.Id, splitwindow.ProductsToSplit, _UM.CurrentUser);
                stbNotifications.Text = _TM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
                
                UI.TransactionButton tb = new UI.TransactionButton(_TM.Transactionpool.Last());
                
                wpTransactions.Children.Add(tb);
                _selectedTransaction = tb.Transaction;
                SetTransactionsUI();
                
            }
        }

        #endregion

        #region Menu
        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (dpMenu.ActualWidth < 100)
            {
                dpMenu.Width = 162;
                btn.Content = (Image)Application.Current.Resources["ImgBack"];
            }
            else
            {
                dpMenu.Width = 48;
                btn.Content = (Image)Application.Current.Resources["ImgMenu"];
            }
        }
        //Products

        /// <summary>
        /// Handles product window closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var productwindow = sender as UI.NewProductWindow;
            if (productwindow.IsCanceled)
            {
                return;
            }
            if (_PM.AddProduct(productwindow.ProductName, productwindow.Price, productwindow.PurchasePrice, productwindow.PackQ,productwindow.InitQ, productwindow.Category))
            {
                //UI.ProductButton pb = new UI.ProductButton(_PM.Productpool.Last());
                //pb.PreviewMouseDown += Pb_PreviewMouseDown;
                //pb.Width = 84;
                //pb.Height = 70;
                //DockPanel dpcat = wpProducts.Children[(int)pb.Product.Category] as DockPanel;
                //WrapPanel wpcat = dpcat.Children[1] as WrapPanel;
                //wpcat.Children.Add(pb);
                _PM.Sort(_PM.CurrentSortMethod);
                SetProductsUI();
                stbNotifications.Text = _PM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
            }

        }


        /// <summary>
        /// Change the viewable product categories
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btncat_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ToggleButton;

            //view multiple categories
            if ((bool)btn.IsChecked)
            {
                if (btn.Content.ToString() == "All")
                {

                    //show all categories
                    foreach (DockPanel dp in wpProducts.Children)
                    {
                        WrapPanel wp = dp.Children[1] as WrapPanel;
                        if (wp.Children.Count > 0)
                        {
                            dp.Visibility = Visibility.Visible;
                        }
                    }
                    foreach (ToggleButton b in wpCategories.Children)
                    {
                        b.IsChecked = true;
                    }
                    ToggleButton pbnone1 = wpCategories.Children[wpCategories.Children.Count - 1] as ToggleButton;
                    pbnone1.IsChecked = false;
                    return;
                }
                if (btn.Content.ToString() == "None")
                {
                    //hide all categories
                    foreach (DockPanel dp in wpProducts.Children)
                    {
                        dp.Visibility = Visibility.Collapsed;
                    }
                    foreach (ToggleButton b in wpCategories.Children)
                    {
                        b.IsChecked = false;
                    }
                    btn.IsChecked = true;
                    return;
                }
                //load product buttons per category
                wpProducts.Children[(int)Enum.Parse(typeof(BLL.ProductCategory), btn.Content.ToString())].Visibility = Visibility.Visible;

                ToggleButton pbnone2 = wpCategories.Children[wpCategories.Children.Count - 1] as ToggleButton;
                pbnone2.IsChecked = false;
                btn.IsChecked = true;
            }
            else
            {
                if (btn.Content.ToString() == "All")
                {
                    //hide all categories
                    foreach (DockPanel dp in wpProducts.Children)
                    {
                        dp.Visibility = Visibility.Collapsed;
                    }
                    foreach (ToggleButton b in wpCategories.Children)
                    {
                        b.IsChecked = false;
                    }
                    ToggleButton pbnone1 = wpCategories.Children[wpCategories.Children.Count - 1] as ToggleButton;
                    pbnone1.IsChecked = true;
                    return;
                }
                if (btn.Content.ToString() == "None")
                {
                    //show all categories
                    foreach (DockPanel dp in wpProducts.Children)
                    {
                        WrapPanel wp = dp.Children[1] as WrapPanel;
                        if (wp.Children.Count > 0)
                        {
                            dp.Visibility = Visibility.Visible;
                        }
                    }
                    foreach (ToggleButton b in wpCategories.Children)
                    {
                        b.IsChecked = true;
                    }
                    btn.IsChecked = false;
                    return;
                }
                //delete products from view
                wpProducts.Children[(int)Enum.Parse(typeof(BLL.ProductCategory), btn.Content.ToString())].Visibility = Visibility.Collapsed;
                btn.IsChecked = false;
            }
        }

        /// <summary>
        /// Toggles the calculator quantity input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddDelcalculator_Click(object sender, RoutedEventArgs e)
        {
            
            if (ucCalculator.Visibility == Visibility.Collapsed)
            {
                ucCalculator.Visibility = Visibility.Visible;
            }
            else
            {
                ucCalculator.Visibility = Visibility.Collapsed;
            }       
        }

        /// <summary>
        /// Sorts the products and re-init the Products panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbSortProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbSortProducts.SelectedIndex >= 0 )
            {
                _PM.Sort((BLL.ComparerType)cmbSortProducts.SelectedIndex);
                SetProductsUI();
            }
        }


        private void Ep_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _PM.Sort(_PM.CurrentSortMethod);
            SetProductsUI();
        }

        //User

        

        private void Useredit_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var editwndw = sender as UI.UserRegistrationWindow;
            if (! editwndw.Cancel)
            {
                SetUserUI();
                InitTransactionsUI();
                stbNotifications.Text = _UM.Notifications.Last();
                _notifications.Add(stbNotifications.Text);
            }
          
        }

        //Inventory
        private void Iw_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UI.UserWindows.InventoryWindow w = (UI.UserWindows.InventoryWindow)sender;
            foreach (InventoryProduct item in w.LastInventory)
            {
                _PM.UpdateCurrentQuantity(item);
            }
        }

        //Cards
        private void Chargewindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UI.UserWindows.CardRecharge cr = sender as UI.UserWindows.CardRecharge;
            if (cr.IsCancel) { return; }
            if (_RD.Recharge(cr.Amount))
            {
                if (_curCmdParam == DataTypes.Card)
                {
                    _TM.AddTransaction(_UM.CurrentUser, "Drankkaart Herladen");
                    Transaction t = _TM.Transactionpool.Last();
                    _TM.AddSingleProduct(t.Id, new Product("Drankkaart Herladen", cr.Amount, 0, ProductCategory.Diverse));
                    _TM.PayTransaction(t.Id, cr.Payed, cr.Method);
                    _TM.CloseTransaction(t.Id, _UM.CurrentUser, true);
                    if (cr.Method == PaymentMethod.Cash)
                    {
                        _CD.Open();
                        DrawerAction action = new DrawerAction()
                        {
                            Action = "Betaling",
                            User = _UM.CurrentUser.Username,
                            OnHandAmount = Properties.Settings.Default.CashOnHand,
                            DepositAmount = cr.Payed,
                            Timestamp = DateTime.Now
                        };
                        action.Valid = "NA";
                        action.NewAmount = action.OnHandAmount + action.DepositAmount;
                        DAL.TXT.Instance.LogDrawerAction(action);
                        Properties.Settings.Default.CashOnHand = action.NewAmount;
                        txtOnHandCash.Text = action.NewAmount.ToString("F2");
                    }
                }
                stbNotifications.Text = "Added " + cr.Amount.ToString("F2") + " to " + _RD.ConnectedCard.UID + " current balance = €" + _RD.ConnectedCard.Credit.ToString("F2");
                _DA.UpdateCardSaldo(_RD.ConnectedCard);
                _notifications.Add(stbNotifications.Text);
                _customerWindow.NewAmount = _RD.ConnectedCard.Credit;
                lstCardpool.ItemsSource = _DA.LoadCards();            
            }
            else
            {
                stbNotifications.Text = _RD.Error;
                _notifications.Add(stbNotifications.Text);
            }
        }

        //Settings

        //public static event EventHandler LanguageChanged; //geen idee wat nut hiervan is.
        //protected virtual void OnLanguageChanged(EventArgs e) //geen idee wat nut hiervan is.
        //{
        //    LanguageChanged?.Invoke(this, e);//geen idee wat nut hiervan is.
        //}
        // ik denk dat dit een gewone event is die geraised wordt en er kunnen dan om het even waar handlers subscriben hierop om bvb nog wat extra code uit te voeren als de taal is aangepast (of dus command is executed).
        
        // PATTERN Commands
        //Commanding using direct command classes
        public LanguangeCommand DirectSetLangCmdStrongTyped => new LanguangeCommand(); //Normal command class created and used with the canexecute and execute logic in it. Does not use delegates because code to execute is declared in command class itself.
        public ICommand DirectSetLangCmd => new LanguangeCommand(); //Weak typed version using the ICommand interface as the property type. Although fields can be used, binding it to a XAML (or WPF) UI control requires a property. 
        //Commanding using a relay command class
        //The relay class allows to define the code for execute and canexecute anywhere else in the codespace and have the relaycommand execute it through delegate functions (delegate types).
        private Action<object> setLangAction = new Action<object>(p => SetLanguageToStatic(p)); //because this is a field, the delegate function (method to execute) needs to be static. the fields are constructed befor non static methods are defined. Statics are defined and constructed before everything
        private Action<object> setLangActionAsExpressionBody = p => SetLanguageToStatic(p); //Lambda expressions like p => Func(p) are implicitly converted to delegate types like Action Predicate Func and can be used as delegates directly.
        private static void SetLanguageToStatic(object parameters) //Statics are part of the type itself, not of the instance of the type and thus can be accessed before instantiation and are shared for all instances.
        {
            MenuItem itm = (MenuItem)parameters;
            string lang = itm.Header is TextBlock hdr ? hdr.Text : (string)itm.Header;
            Properties.Settings.Default.Language = lang;
            Properties.Settings.Default.Save();
            //OnLanguageChanged(EventArgs.Empty);
            LocalizationProvider.UpdateAllObjects();
        }
        public Action<object> SetLangActionProp //Defining the delegate as a property allows for non static functions to be passed.
        {
            get
            {
                return p => SetLanguageTo(p);
            }
        }
        private Action<object> SetLangActionPropAsExpression => p => SetLanguageTo(p); //private properties can be used if its never needed to change the propery (read as never let the delgate do something else by setting the value to point to some other function (use other lambda)).
        //above are al valid delegate declaration -> use any of the above as preferred pattern
        private void SetLanguageTo(object parameters) //The actual method to execute, which is passed as a delegate. In this case it is an Action because it takes 1 parameter and returns nothing (void) and it needs to be this for usage in commands. Could be something else for custom delegate usage.
        {
            MenuItem itm = (MenuItem)parameters;
            string lang = itm.Header is TextBlock hdr ? hdr.Text : (string)itm.Header;
            Properties.Settings.Default.Language = lang;
            Properties.Settings.Default.Save();
            //OnLanguageChanged(EventArgs.Empty);
            LocalizationProvider.UpdateAllObjects();
        }
        //Decleration of the relay commands. No CanExecute relay is used because in this example the command can always be executed. Add a CanExecute delgate in the same way as the Execute delegate but it should be of type Predicate (returns a bool and has 1 argument) instead of Action.
        public RelayCommand RelaySetLangCmdStrongTyped => new RelayCommand(setLangAction); //uses 1 of the above delegates
        public ICommand RelaySetLangCmdVerbose { get { return new RelayCommand(SetLangActionProp); } } //weak typed using ICommand Interface
        public ICommand RelaySetLangCmd { get { return new RelayCommand(p => SetLanguageTo(p)); } } //weak typed + uses a lambda expression as delegate directly(see above)
        public ICommand RelaySetLangCmdAsExpression => new RelayCommand(p => SetLanguageTo(p)); // shortest/quickest way to achieve the relay command functionality (lambda + expression body + possibly weak typed but strong typed is same). 
        //see also xaml for binding with the command dependencyproperty of a control.

        #endregion

        #region Dragable ScrollViewer
        private double _initOffset = 0.0;
        private double _initmouse = 0.0;
        private double _distance = 0.0;
        private void spProductList_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _initmouse = Mouse.GetPosition(scrollvwr).Y;
            _initOffset = scrollvwr.VerticalOffset;
            wpProducts.MouseMove += SpProductList_MouseMove;
        }
        private void SpProductList_MouseMove(object sender, MouseEventArgs e)
        {
            _distance = _initmouse - Mouse.GetPosition(scrollvwr).Y;
            scrollvwr.ScrollToVerticalOffset(_initOffset + _distance);
        }
        private void spProductList_MouseUp(object sender, MouseButtonEventArgs e)
        {
            wpProducts.MouseMove -= SpProductList_MouseMove;
        }
        private void spProductList_MouseLeave(object sender, MouseEventArgs e)
        {
            wpProducts.MouseMove -= SpProductList_MouseMove;
        }
        #endregion

        #region Command Handlers
        //Application Commands
        private void New_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                param = DataTypes.Transaction;
            }
            //Can Execute based on parameter
            switch (param)
            {
                case DataTypes.User:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null);
                    break;
                case DataTypes.Product:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin) && _DA.IsConnected;
                    break;
                case DataTypes.Payment:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;

                case DataTypes.Transaction:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest);
                    break;

                case DataTypes.InventoryProduct:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;

                case DataTypes.Card:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest) && _RD.IsAvailable && _RD.IsCardConnected;
                    break;

                case DataTypes.Organization:
                    break;

                default:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;
            }
        }
        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                param = DataTypes.Transaction;
            }
            //Execute based on parameter
            switch (param)
            {
                case DataTypes.User:
                    if (_DA.IsConnected)
                    {
                        UI.UserRegistrationWindow regwindow = new UI.UserRegistrationWindow(_UM, false);
                        regwindow.Closing += Regwindow_Closing;
                        regwindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        regwindow.Show();
                        return;
                    }
                    stbNotifications.Text = "System is OFFLINE. Users can only be added when ONLINE, unable to get unique UserID.";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Product:
                    if (_DA.IsConnected)
                    {
                        UI.NewProductWindow productWindow = new UI.NewProductWindow();
                        productWindow.Closing += ProductWindow_Closing;
                        productWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        productWindow.Show();
                        return;
                    }
                    stbNotifications.Text = "System is offline. Can't add products";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Payment:

                    break;

                case DataTypes.Transaction:
                    if (_TM.AddTransaction(_UM.CurrentUser, "New"))
                    {
                        UI.TransactionButton tb = new UI.TransactionButton(_TM.Transactionpool.Last());
                        
                        wpTransactions.Children.Add(tb);
                        _selectedTransaction = tb.Transaction;
                        SetTransactionsUI();
                        
                    }
                    stbNotifications.Text = _TM.Notifications.Last();
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.InventoryProduct:
                   
                    break;

                case DataTypes.Card:
                    UI.UserWindows.NewCardPopup NC = new UI.UserWindows.NewCardPopup();
                    bool cancel = (bool)NC.ShowDialog();
                    if (! cancel)
                    {
                        if (_RD.InitCard(NC.OwnerName, NC.OwnerSurname))
                        {
                            if (_DA.AddCard(_RD.ConnectedCard))
                            {
                                stbNotifications.Text = _RD.ConnectedCard.UID + " is created for " + NC.OwnerName + " " + NC.OwnerSurname;
                                _notifications.Add(stbNotifications.Text);
                                _customerWindow.NewAmount = _RD.ConnectedCard.Credit;
                                lstCardpool.ItemsSource = _DA.LoadCards();
                            }
                            else
                            {
                                stbNotifications.Text = _RD.ConnectedCard.UID + " is created for " + NC.OwnerName + " " + NC.OwnerSurname + ". Data base not available, will be added on next use";
                                _notifications.Add(stbNotifications.Text);
                                _customerWindow.NewAmount = _RD.ConnectedCard.Credit;
                            }
                        }
                        else
                        {
                            stbNotifications.Text = _RD.Error;
                            _notifications.Add(stbNotifications.Text);
                        }
                    }
                    else
                    {
                        stbNotifications.Text = "New card canceled by user";
                        _notifications.Add(stbNotifications.Text);
                    }
                    break;
                case DataTypes.Organization:
                    break;

                default:

                    break;
            }

        }

        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                e.CanExecute = false;
                return;
            }
            //Can Execute based on parameter
            TransactionDetailsControl td = null;

           switch (param)
           {
                case DataTypes.User:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest;
                    break;
                case DataTypes.Product:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin) && _DA.IsConnected;
                    break;
                case DataTypes.Payment:
                    if (spTransactionDetails.Children.Count > 0)
                    {
                        td = spTransactionDetails.Children[0] as TransactionDetailsControl;
                    }
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest) && _selectedTransaction != null && td != null && td.lvPayments.SelectedIndex >= 0;
                    break;
                case DataTypes.Transaction:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest) && _selectedTransaction != null;
                    break;
                case DataTypes.TransactionProduct:
                case DataTypes.TransactionProductLine:
                    if (spTransactionDetails.Children.Count > 0)
                    {
                        td = spTransactionDetails.Children[0] as TransactionDetailsControl;
                    }
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest) && _selectedTransaction != null && td != null && td.lvProducts.SelectedIndex >= 0 ;
                   break;
                case DataTypes.Card:
                case DataTypes.Card_Credit:
                case DataTypes.Card_Credit_Debug:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && _UM.CurrentUser.Userlevel >= Userlevel.Super && _RD != null && _RD.IsAvailable && _RD.IsCardConnected;
                    break;
                default:
                    e.CanExecute = false;
                   break;
           }
        }
        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                return;
            }
            //Execute based on parameter
            switch (param)
            {
                case DataTypes.TransactionProduct:
                    if (_selectedTransaction != null)
                    {
                        var td = spTransactionDetails.Children[0] as UI.TransactionDetailsControl;
                        var p = td.lvProducts.SelectedItem as BLL.Product;
                        //newp is product clone with quantity = 1
                        BLL.Product newp = null;
                        if (p != null) { newp = new BLL.Product(p.Name, p.Price, p.Purchaseprice, p.Category, p.Id, 1, p.IsActive, p.IsSynchronized); }
                        _TM.DeleteSingleProduct(_selectedTransaction.Id, newp);
                        stbNotifications.Text = _TM.Notifications.Last();
                        _notifications.Add(stbNotifications.Text);
                        SetTransactionsUI();
                        if (newp != null) { ProductReturned(newp); }
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.TransactionProductLine:
                    if (_selectedTransaction != null)
                    {
                        var td = spTransactionDetails.Children[0] as UI.TransactionDetailsControl;
                        BLL.Product prod = (BLL.Product)td.lvProducts.SelectedItem;
                        BLL.Product p = null;
                        if (prod != null) { p = new BLL.Product(prod); }
                        _TM.DeleteSingleProduct(_selectedTransaction.Id, p);
                        stbNotifications.Text = _TM.Notifications.Last();
                        _notifications.Add(stbNotifications.Text);
                        SetTransactionsUI();
                        if (p != null) { ProductReturned(p); }
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Product:
                    //add a application state to all the user modules. use this state in eventhandlers to do different actions on a (for instance) product button click depending on the state. the state is set or changed by other events.
                    if (_PM.ModState == BLL.ModuleState.Deleting)
                    {
                        _PM.ModState = BLL.ModuleState.Normal;
                        stbNotifications.Text = "Normal product state active. Click a product to add it to the selected transaction.";
                        _notifications.Add(stbNotifications.Text);
                        wpProducts.Background = new SolidColorBrush((Color)Application.Current.Resources["BackgroundDark"]);
                        return;
                    }
                    if (_PM.ModState != BLL.ModuleState.Deleting && (_UM.CurrentUser.Userlevel == BLL.Userlevel.Admin || _UM.CurrentUser.Userlevel == BLL.Userlevel.Super))
                    {
                        _PM.ModState = BLL.ModuleState.Deleting;
                        stbNotifications.Text = "Product deletion state. Click a product to deactivate it.";
                        _notifications.Add(stbNotifications.Text);
                        wpProducts.Background = new SolidColorBrush(Colors.Red);
                        return;
                    }
                    stbNotifications.Text = "Userlevel does not allow product deletion.";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.User:
                    _UM.DeactivateUser(_UM.CurrentUser);
                    stbNotifications.Text = _UM.Notifications.Last();
                    _notifications.Add(stbNotifications.Text);
                    _UM.Logout();
                    SetUserUI();
                    ToggleProductsUI(false);
                    wpTransactions.Children.Clear();
                    wpTransactions.IsEnabled = false;
                    spTransactionDetails.Children.Clear();
                    spTransactionDetails.IsEnabled = false;
                    stbNotifications.Text = "Click user icon in upper right corner to log in.";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Payment:
                    if (_selectedTransaction != null)
                    {
                        var td = spTransactionDetails.Children[0] as UI.TransactionDetailsControl;
                        var p = td.lvPayments.SelectedItem as BLL.Payment;
                        _TM.DeletePayment(_selectedTransaction.Id, p);
                        stbNotifications.Text = _TM.Notifications.Last();
                        _notifications.Add(stbNotifications.Text);
                        SetTransactionsUI();
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Transaction:
                    if (_selectedTransaction != null)
                    {
                        var result = MessageBox.Show("De volledige transactie zal verwijderd worden uit de database. WEG IS WEG!!! Wilt u dit zeker doen?","Transactie Verwijderen?" , MessageBoxButton.YesNo);
                        if (result == MessageBoxResult.Yes)
                        {
                            if (_TM.DeleteTransaction(_selectedTransaction.Id))
                            {
                                foreach (Product p in _selectedTransaction.Products)
                                {
                                    ProductReturned(p);
                                }
                                foreach (UI.TransactionButton tb in wpTransactions.Children)
                                {
                                    if (tb.Transaction.Id == _selectedTransaction.Id)
                                    {
                                        wpTransactions.Children.Remove(tb);
                                        spTransactionDetails.Children.RemoveAt(0);
                                        _selectedTransaction = null;
                                        stbNotifications.Text = _TM.Notifications.Last();
                                        _notifications.Add(stbNotifications.Text);
                                        return;
                                    }
                                }
                            }
                        }
                        stbNotifications.Text = "Deletion canceled by user.";
                        _notifications.Add(stbNotifications.Text);
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Card:
                case DataTypes.Card_Credit:
                case DataTypes.Card_Credit_Debug:
                    MessageBoxResult res = MessageBox.Show("Zeker dat je deze kaart wil verwijderen? WEG IS WEG!! Deze actie kan niet meer ontdaan worden.", "Permanente Actie", MessageBoxButton.OKCancel);
                    if (res != MessageBoxResult.OK && res != MessageBoxResult.Yes) { break; }
                    if (_RD.Reset())
                    {
                        _DA.DeleteCard(_RD.ConnectedCard);
                        stbNotifications.Text = "Card with UID " + _RD.ConnectedCard.UID + " is cleared";
                        _notifications.Add(stbNotifications.Text);
                        _customerWindow.NewAmount = _RD.ConnectedCard.Credit;
                    }
                    else
                    {
                        stbNotifications.Text = _RD.Error;
                        _notifications.Add(stbNotifications.Text);
                    }
                    lstCardpool.ItemsSource = _DA.LoadCards();
                    break;
                default:
                    break;
            }

            
        }

        private void Print_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_selectedTransaction != null && _TP.IsAvailable  && _selectedTransaction.Amount > 0);// && !_TP.IsPrinting && _selectedTransaction.AmountOpen == 0);
        }
        private void Print_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //do printcode
            _TP.Print(_selectedTransaction);
        }

        private void Close_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                e.CanExecute = false;
                return;
            }
            //Can Execute based on parameter
            switch (param)
            {
                case DataTypes.Transaction:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _selectedTransaction != null && _selectedTransaction.AmountOpen == 0 && _selectedTransaction.Products.Count > 0;
                    break;
                default:
                    e.CanExecute = false;
                    break;
            }
        }
        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                return;
            }
            //Execute based on parameter
            switch (param)
            {
                case DataTypes.Transaction:
                    if (_selectedTransaction != null)
                    {
                        if (btnAutoPrint.IsChecked == true)
                        {
                            _TP.Print(_selectedTransaction);
                        }
                        if (_xDebugMode && _UM.CurrentUser.Userlevel == Userlevel.Super)
                        {
                            _TM.CloseTransaction(_selectedTransaction.Id, _selectedTransaction.Openedby);
                            InitTransactionsUI();
                        }
                        else 
                        {
                            _TM.CloseTransaction(_selectedTransaction.Id, _UM.CurrentUser);
                            InitTransactionsUI();
                        }
                        stbNotifications.Text = _TM.Notifications.Last();
                        _notifications.Add(stbNotifications.Text);
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                default:
                    break;
            }
        }

        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                e.CanExecute = false;
                return;
            }
            //Can Execute based on parameter
            switch (param)
            {
                case DataTypes.Inventory:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && _UM.CurrentUser.Userlevel >= Userlevel.Admin && _DA.IsConnected;
                    break;
                case DataTypes.Product:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin) && _DA.IsConnected;
                    break;
                case DataTypes.User:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_CD != null) && (_RD != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _DA.IsConnected;
                    break;
                case DataTypes.Payment:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_CD != null) && (_RD != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _DA.IsConnected && _selectedTransaction != null && _selectedTransaction.AmountOpen > 0;
                    break;
                case DataTypes.Reader:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_CD != null) && (_RD != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _DA.IsConnected;
                    break;
                case DataTypes.Transaction:
                    e.CanExecute = false;
                    break;
                case DataTypes.Card:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_CD != null) && (_RD != null) && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _RD.IsCardConnected;
                    break;
                case DataTypes.Drawer:
                    e.CanExecute = _UM != null && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _CD != null;
                    break;
                default:
                    e.CanExecute = false;
                    break;
            }

        }
        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                return;
            }
            //Execute based on parameter
            switch (param)
            {
                case DataTypes.User:
                    UI.UserRegistrationWindow useredit = new UI.UserRegistrationWindow(_UM, true);
                    useredit.Closing += Useredit_Closing;
                    useredit.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    useredit.Show();
                    break;
                case DataTypes.Inventory:
                    UI.UserWindows.InventoryWindow iw = new UI.UserWindows.InventoryWindow(_UM.CurrentUser);
                    iw.Closing += Iw_Closing;
                    iw.Show();
                    break;
                case DataTypes.Product:
                    if (_UM.CurrentUser.Userlevel >= BLL.Userlevel.Admin)
                    {
                        UI.UserWindows.EditProducts ep = new UI.UserWindows.EditProducts(_PM);
                        ep.Closing += Ep_Closing;
                        ep.Show();
                    }
                    break;
                case DataTypes.Payment:
                    if (_selectedTransaction != null)
                    {
                        UI.PayWindow payWindow = new UI.PayWindow(_selectedTransaction,_RD);
                        payWindow.Closing += PayWindow_Closing;
                        payWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        if (_RD.ConnectedCard != null)
                        {
                            payWindow.CustomerCard = _RD.ConnectedCard;
                        }
                        payWindow.ShowDialog();
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Transaction:
                    if (_selectedTransaction != null)
                    {
                        UI.SplitTransactionWindow splitWindow = new UI.SplitTransactionWindow(_selectedTransaction);
                        splitWindow.Closing += SplitWindow_Closing;
                        splitWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        splitWindow.Show();
                        return;
                    }
                    stbNotifications.Text = "No transaction selected";
                    _notifications.Add(stbNotifications.Text);
                    break;
                case DataTypes.Reader:
                case DataTypes.Card:
                    new UI.UserWindows.CardProperties(_RD.ConnectedCard).ShowDialog();
                    break;
                case DataTypes.Drawer:
                    _CD.Open();
                    DrawerAction action = new DrawerAction()
                    {
                        Action = "Openen",
                        User = _UM.CurrentUser.Username,
                        OnHandAmount = Properties.Settings.Default.CashOnHand,
                        Timestamp = DateTime.Now,
                        Valid = "NA",
                    };
                    //DAL.XML.Instance.LogDrawerAction(action);
                    DAL.TXT.Instance.LogDrawerAction(action);
                    break;
                default:
                    break;
            }
        }
        
        private void Properties_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = _UM != null && _UM.CurrentUser.Userlevel >= Userlevel.Guest && _CD != null;
        }
        private void Properties_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            _CD.Open();
            UI.UserWindows.InputDialog input = new UI.UserWindows.InputDialog("Telling", "Verwacht bedrag", Properties.Settings.Default.CashOnHand.ToString(), "Geteld bedrag", "");
            input.ShowDialog();
            DrawerAction action = new DrawerAction()
            {
                Action = "Telling",
                User = _UM.CurrentUser.Username,
                OnHandAmount = Properties.Settings.Default.CashOnHand,
                CountedAmount = double.Parse(input.Value2),
                Timestamp = DateTime.Now
            };
            action.Valid = action.OnHandAmount == action.CountedAmount ? "Yes" : "No";
            action.NewAmount = action.CountedAmount;
            DAL.TXT.Instance.LogDrawerAction(action);
            Properties.Settings.Default.CashOnHand = action.NewAmount;
            txtOnHandCash.Text = action.NewAmount.ToString("F2");
        }

        private void Redo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

        }
        private void Redo_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void Undo_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

        }
        private void Undo_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        //Component Commands
        private void ScrollPage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            ScrollViewer scvwr = e.Source as ScrollViewer;
            string param = "";
            if (e.Parameter != null) { param = e.Parameter as string; }

            if (param == "") { e.CanExecute = false; }
            if (param == "L") { e.CanExecute = (scvwr != null) && (scvwr.HorizontalOffset > 0); }
            if (param == "R") { e.CanExecute = (scvwr != null) && (scvwr.ExtentWidth > scvwr.ViewportWidth); }
        }
        private void ScrollPage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ScrollViewer scvwr = e.Source as ScrollViewer;
            string param = "";
            if (e.Parameter != null) { param = e.Parameter as string; }
            double pagewidth = 0;
            if (scvwr != null)
            {
                var p = scvwr.Content as WrapPanel;
                if (p.Children.Count > 0)
                {
                    var el = p.Children[0] as UserControl;
                    pagewidth = el.ActualWidth;
                    if (param == "L") { scvwr.ScrollToHorizontalOffset(scvwr.HorizontalOffset - pagewidth); }
                    if (param == "R") { scvwr.ScrollToHorizontalOffset(scvwr.HorizontalOffset + pagewidth); }
                }
            }
        }

        //Custom Commands
        private void Logout_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_UM != null) && _UM.IsLoggedOn;
        }
        private void Logout_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (wpTransactions.Children.Count > 0)
            {
                var result = MessageBox.Show("Er staan nog onbetaalde transacties open. Laat dit niet herhaaldelijk gebeuren!\nOk om uit te loggen of af te sluiten.\nCancel om de transacties nog af te werken.", "Onbetaalde transacties",MessageBoxButton.OKCancel,MessageBoxImage.Exclamation,MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Cancel)
                    return;
            }
            _UM.Logout();
            SetUserUI();
            UpdateMenuItems(Visibility.Collapsed);
            ToggleProductsUI(false);
            wpTransactions.Children.Clear();
            wpTransactions.IsEnabled = false;
            spTransactionDetails.Children.Clear();
            spTransactionDetails.IsEnabled = false;
            stbNotifications.Text = "Click user icon in upper right corner to log in.";
            _notifications.Add(stbNotifications.Text);
            _customerWindow.Transaction = new Transaction(0, _UM.CurrentUser, "No transaction selected");
        }

        private void Login_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_UM != null);
        }
        private void Login_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            UI.UserLoginWindow login = new UI.UserLoginWindow(_UM.Userpool);
            login.Closing += Login_Closing;
            login.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            login.ShowDialog();
        }
        
        private void Shutdown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void Shutdown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void Edit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //Get command parameter
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                param = DataTypes.Description;
            }
            //Can Execute based on parameter
            switch (param)
            {
                case DataTypes.User:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin) && _DA.IsConnected;
                    break;
                case DataTypes.Product:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;
                case DataTypes.Payment:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;
                case DataTypes.Transaction:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest);
                    break;
                case DataTypes.InventoryProduct:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;
                case DataTypes.Card:
                case DataTypes.Card_Owner:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest) && _RD != null && _RD.IsAvailable && _RD.IsCardConnected;
                    break;
                case DataTypes.Card_Credit:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Super) && _RD != null && _RD.IsAvailable && _RD.IsCardConnected;
                    break;
                case DataTypes.Organization:
                    break;
                case DataTypes.Description:
                    TextBox input = e.OriginalSource as TextBox;
                    if (input != null)
                    {
                        e.CanExecute = input.IsKeyboardFocused;
                    }
                    else
                    {
                        TextBox focussed = Keyboard.FocusedElement as TextBox;

                        e.CanExecute = (focussed != null);
                    }
                    break;
                case DataTypes.Drawer:
                    e.CanExecute = _UM != null && _UM.CurrentUser.Userlevel >= Userlevel.Admin && _CD != null;
                    break;
                default:
                    e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Admin);
                    break;
            }
        }
        private void Edit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                param = DataTypes.Description;
            }
            _curCmdParam = param;
            switch (param)
            {
                case DataTypes.User:
                    break;
                case DataTypes.Product:
                    
                    break;
                case DataTypes.Payment:

                    break;

                case DataTypes.Transaction:

                    break;

                case DataTypes.InventoryProduct:
                    break;

                case DataTypes.Card:
                case DataTypes.Card_Credit:
                    UI.UserWindows.CardRecharge chargewindow = new UI.UserWindows.CardRecharge(_RD.ConnectedCard, false);
                    chargewindow.Closing += Chargewindow_Closing;
                    chargewindow.ShowDialog();
                    break;
                case DataTypes.Card_Credit_Debug:
                    UI.UserWindows.CardRecharge chargewindowdbg = new UI.UserWindows.CardRecharge(_RD.ConnectedCard, true);
                    chargewindowdbg.Closing += Chargewindow_Closing;
                    chargewindowdbg.ShowDialog();
                    break;
                case DataTypes.Card_Owner:
                    UI.UserWindows.NewCardPopup NC = new UI.UserWindows.NewCardPopup();
                    bool? cancel = NC.ShowDialog();
                    if (!(bool)cancel)
                    {
                        if (_RD.ChangeOwner(NC.OwnerName, NC.OwnerSurname))
                        {
                            _DA.UpdateCardOwner(_RD.ConnectedCard);
                            stbNotifications.Text = "Owner of card " + _RD.ConnectedCard.UID + " updated to " + _RD.ConnectedCard.Owner;
                            _notifications.Add(stbNotifications.Text);
                            lstCardpool.ItemsSource = _DA.LoadCards();
                        }
                        else
                        {
                            stbNotifications.Text = _RD.Error;
                            _notifications.Add(stbNotifications.Text);
                        }
                    }
                    break;
                case DataTypes.Organization:
                    break;
                case DataTypes.Description:
                    TextBox input = e.OriginalSource as TextBox;
                    if (input != null)
                    {
                        var td = e.Source as UI.TransactionDetailsControl;
                        if (input.Text != "")
                        {
                            td.txtDesc.Text = input.Text;
                            td.Transaction.Description = input.Text;
                            DAL.DataAdapter.Instance.ChangeTransaction(_selectedTransaction);
                        }
                        td.btnEdit.IsEnabled = true;
                        td.txtEditDesc.Visibility = Visibility.Collapsed;
                        td.txtDesc.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        TextBox focussed = Keyboard.FocusedElement as TextBox;
                        if (focussed != null && spTransactionDetails.Children[0] != null && spTransactionDetails.Children[0].GetType() == typeof(UI.TransactionDetailsControl))
                        {
                            var td = spTransactionDetails.Children[0] as UI.TransactionDetailsControl;
                            if (focussed.Text != "")
                            {
                                td.txtDesc.Text = focussed.Text;
                                td.Transaction.Description = focussed.Text;
                                DAL.DataAdapter.Instance.ChangeTransaction(_selectedTransaction);
                            }
                            td.btnEdit.IsEnabled = true;
                            td.txtEditDesc.Visibility = Visibility.Collapsed;
                            td.txtDesc.Visibility = Visibility.Visible;
                        }
                    }
                    break;
                case DataTypes.Drawer:
                    _CD.Open();
                    UI.UserWindows.InputDialog inputdlg = new UI.UserWindows.InputDialog("Uitname", "Huidig bedrag", Properties.Settings.Default.CashOnHand.ToString(), "Uitname bedrag", "");
                    inputdlg.ShowDialog();
                    DrawerAction action = new DrawerAction()
                    {
                        Action = "Uitname",
                        User = _UM.CurrentUser.Username,
                        OnHandAmount = Properties.Settings.Default.CashOnHand,
                        WithdrawAmount = Math.Abs(double.Parse(inputdlg.Value2)),
                        Timestamp = DateTime.Now
                    };
                    action.Valid = "NA";
                    action.NewAmount = action.OnHandAmount - action.WithdrawAmount;
                    DAL.TXT.Instance.LogDrawerAction(action);
                    Properties.Settings.Default.CashOnHand = action.NewAmount;
                    txtOnHandCash.Text = action.NewAmount.ToString("F2");
                    break;
                default:
                    break;
            }
        }

        private void Select_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (_UM != null) && (_PM != null) && (_TM != null) && (_UM.CurrentUser.Userlevel >= Userlevel.Guest);
        }
        private void Select_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DataTypes param;
            if (e.Parameter != null)
            {
                param = (DataTypes)Enum.Parse(typeof(DataTypes), e.Parameter.ToString());
            }
            else
            {
                return;
            }
            switch (param)
            {
                case DataTypes.Transaction:
                    var tb = e.Source as TransactionButton;
                    if (tb != null)
                    {
                        _selectedTransaction = tb.Transaction;
                        foreach (TransactionButton item in wpTransactions.Children) { item.IsSelected = false; }
                        tb.IsSelected = true;
                        SetTransactionsUI();
                    }
                    break;
                default:

                    break;
            }
        }

        //private void Collaps_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        //{
        //    e.CanExecute = true;
        //}
        //private void Collaps_Executed(object sender, ExecutedRoutedEventArgs e)
        //{
        //    FrameworkElement el = e.Source as FrameworkElement;
        //    if (el != null) { el.Visibility = Visibility.Collapsed; }
        //}

        #endregion

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("View init");
        }
        private void lstProductpool_MouseLeave(object sender, MouseEventArgs e)
        {
            _PM.Sort(_PM.CurrentSortMethod);
            SetProductsUI();
        }
        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //FrameworkElement originsource = e.OriginalSource as FrameworkElement;
            //FrameworkElement source = e.Source as FrameworkElement;
            //string message = string.Format("Sender: {0}\nParameter: {1}\nSource: {2}\nOriginalSource: {3}", sender, e.Parameter, e.Source, e.OriginalSource);
            //MessageBox.Show(message);

            //Use no more than one assignment when you test this code.
            //string target = "http://www.microsoft.com";
            //string target = "ftp://ftp.microsoft.com";
            //string target = "C:\\Program Files\\Microsoft Visual Studio\\INSTALL.HTM";
            string filepath = System.IO.Directory.GetCurrentDirectory();
            filepath = filepath.Replace("\\", "/");
            filepath = filepath.Substring(0, filepath.IndexOf("/bin"));
            string target = "file:///" + filepath + "/Docs/output/NAT_Kassa_Handleiding.html" + e.Parameter.ToString(); //needs to have the file:/// path prefix in order to work with the #, otherwise it gets parsed to a %23
            //target = "file:///C:/Tdmasure/Source/repos/NAT_Kassa/NAT_Kassa/Docs/output/NAT_Kassa_Handleiding.html" + e.Parameter.ToString();
            try
            {
                System.Diagnostics.Process.Start("chrome",target);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

        private void Popup_Test(object sender, RoutedEventArgs e)
        {
            NotificationPopup p = new NotificationPopup("NFC Card : Test Gebruiker", Brushes.DodgerBlue, $"Saldo = € 105.33");
            p.OnRemove += Notification_Removal;
            spNotificationPopups.Children.Add(p);
        }
        private void HashValidationToggle(object sender, RoutedEventArgs e)
        {
            _UseHashValidation = (bool)btnValidateHashToggle.IsChecked;
            _RD.UseValidation = _UseHashValidation;
        }

        private void Show_Cursor_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton tb = sender as ToggleButton;
            _UseMouse = (bool)tb.IsChecked;
            if (!_UseMouse) { Mouse.OverrideCursor = Cursors.None; } else { Mouse.OverrideCursor = Cursors.Arrow; }
        }
    }


    /// <summary>
    /// Custom Commands Definitions 
    /// </summary>
    public static class MyCommands
    {
        public static RoutedCommand Shutdown = new RoutedCommand("Shutdown", typeof(MyCommands), new InputGestureCollection() { new KeyGesture(Key.F4,ModifierKeys.Alt)});
        public static RoutedCommand Logout = new RoutedCommand("Logout", typeof(MyCommands), new InputGestureCollection() { new KeyGesture(Key.Q, ModifierKeys.Control) });
        public static RoutedCommand Login = new RoutedCommand("Login", typeof(MyCommands), new InputGestureCollection() { new KeyGesture(Key.S, ModifierKeys.Control) });
        public static RoutedCommand Remove = new RoutedCommand("Remove", typeof(MyCommands), new InputGestureCollection() { new KeyGesture(Key.X, ModifierKeys.Alt) });
        public static RoutedCommand Edit = new RoutedCommand("Edit", typeof(MyCommands), new InputGestureCollection() { new KeyGesture(Key.Enter) });
        public static RoutedCommand Select = new RoutedCommand("Select", typeof(MyCommands));
        public static RoutedCommand Collapse = new RoutedCommand("Collaps", typeof(MyCommands));
        //add and undo and redo action
        //define a model to store all actions that can be undone and redone with all necesary data and methods to restore data in the application.
        //make backlogs of these actions
        //store them in a list.
        //each undo or redo restores data or destroys data and make sure the corresponding data in the databases is altered.

    }
    /// <summary>
    /// Class implementing the ICommand Interface. Can be used to create custom commands. Classes like this can be used in all command patterns.
    /// Add custom logic here which acts as the actions that are executed by the command.
    /// </summary>
    public class LanguangeCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        /// <summary>
        /// Checks if the command can be executed based on the source of the command.
        /// </summary>
        /// <param name="parameter">Source of the command request</param>
        /// <returns>Can be executed?</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }
        /// <summary>
        /// The actual code that will be executed if a command has been triggerd an can be executed.
        /// </summary>
        /// <param name="parameter">Source of the command request</param>
        public void Execute(object parameter)
        {
            switch (parameter.GetType().Name)
            {
                case "MenuItem":
                    MenuItem itm = (MenuItem)parameter;
                    string lang = itm.Header is TextBlock hdr ? hdr.Text : (string)itm.Header;
                    Properties.Settings.Default.Language = lang;
                    Properties.Settings.Default.Save();
                    LocalizationProvider.UpdateAllObjects();
                    return;
                default:
                    throw new ArgumentException("Command Parameter is of incorrect type");
            }
        }
    }
    /// <summary>
    /// Class can be used as a command for which the execute and canexecute code can be specified anywhere else in the project (maybe same namespace?) through a delegate type object for a function Func/Action/Predicate/...
    /// The delegate can be specified through a lambda expression p => Named_Method(p) or create a local scope and add statements at the constructor of the RelayCommand.
    /// RelayCommands can be used for all commanding patterns like a normal command.
    /// </summary>
    public class RelayCommand : ICommand
    {
        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        public RelayCommand(Action<object> execute)
            : this(execute, null)
        { }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            _execute = execute ?? throw new ArgumentNullException("execute");
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters); 
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }
    }
    /// <summary>
    /// Enumeration of the project custom datatypes 
    /// </summary>
    public enum DataTypes
    {
        None, User, Product, Payment, Transaction, InventoryProduct, Card, Reader, Printer, Drawer, Organization, TransactionProduct, TransactionProductLine, TransactionPayment, Description, Inventory, Notification, Card_Credit, Card_Credit_Debug, Card_Owner
    }
}
