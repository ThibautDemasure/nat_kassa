﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NAT_Kassa.DBG
{
    /// <summary>
    /// Interaction logic for DebugWindow.xaml
    /// </summary>
    public partial class DebugWindow : Window
    {
        private CashDrawer CD;
        private List<BLL.InventoryProduct> inventory = new List<BLL.InventoryProduct>();
        private BLL.InventoryProduct testprod = new BLL.InventoryProduct("testprod", 1.5, 0.8, BLL.ProductCategory.Bier, 12, 25, true, true, 24,555,3,3);
        private System.Timers.Timer _tmr;
        public DebugWindow()
        {
            InitializeComponent();
             CD = new CashDrawer();
            _tmr = new System.Timers.Timer(5000);
            _tmr.Elapsed += _tmr_Elapsed;
            _tmr.Start();
            List<string> s = new List<string>();
            s.Add("Users");
            s.Add("TransactionsU");
            s.Add("Products");
            s.Add("TransactionsP");
            cmbFile.ItemsSource = s;
            CD.Closed += CD_Closed;
            CD.Tick += CD_Tick;
            txtSetClosedStateCD.Text = Properties.Settings.Default.iCashDrawerClosedState.ToString();

            UI.UserControls.ProductSurface productSurface = new UI.UserControls.ProductSurface(testprod,0);
            productSurface.Width = 75;
            productSurface.Height = 75;
            spProductContainer.Children.Add(productSurface);
        }

        

        private void BtnUpdateLayout_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFile.SelectedIndex == 0)
            {
                DAL.XML.Instance.UpdateUserFileLayout();
                txtUpdateLayout.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "      Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
            if (cmbFile.SelectedIndex == 1)
            {
                DAL.XML.Instance.UpdateTransactionsFileLayoutU();
                txtUpdateLayout.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "      Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
            if (cmbFile.SelectedIndex == 2)
            {
                DAL.XML.Instance.UpdateProductsFileLayout();
                txtUpdateLayout.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "      Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
            if (cmbFile.SelectedIndex == 3)
            {
                DAL.XML.Instance.UpdateTransactionsFileLayoutP();
                txtUpdateLayout.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "      Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
        }

        private void BtnUpdateFiles_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFile.SelectedIndex == 0)
            {
                DAL.XML.Instance.UpdateUsersFromDB();
                txtUpdateFiles.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "       Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
            if (cmbFile.SelectedIndex == 1)
            {
               
            }
            if (cmbFile.SelectedIndex == 2)
            {
                DAL.XML.Instance.UpdateProductFromDB();
                txtUpdateFiles.Text = "Updated : " + DAL.XML.Instance.NodesAdded + "       Errors : " + DAL.XML.Instance.NodeErrors + " in possible nodes : " + DAL.XML.Instance.PossibleAds;
                lstErrors.ItemsSource = null;
                lstErrors.ItemsSource = DAL.XML.Instance.Errors;
            }
            if (cmbFile.SelectedIndex == 3)
            {

            }
        }

        private void BtnOpenCD_Click(object sender, RoutedEventArgs e)
        {
            CD.Open();
            btnOpenCD.Content = CD.Exitcode;
            lstErrors.Items.Add(CD.Errormessage);
        }

        private void BtnGetStateCD_Click(object sender, RoutedEventArgs e)
        {
            CD.CheckClosed();
            btnGetStateCD.Content = CD.Exitcode;
            lstErrors.Items.Add(CD.Errormessage);
        }

        private void BtnSetClosedStateCD_Click(object sender, RoutedEventArgs e)
        {
            CD.SetOpenedValue(int.Parse(txtSetClosedStateCD.Text));
            btnSetClosedStateCD.Content = Properties.Settings.Default.iCashDrawerClosedState;
        }

        private void CD_Tick(object sender, EventArgs e)
        {
            lstStateCD.Dispatcher.Invoke(() => { lstStateCD.Items.Add("open " + CD.Exitcode.ToString()); });
            
        }

        private void CD_Closed(object sender, EventArgs e)
        {
            lstStateCD.Dispatcher.Invoke(() => { lstStateCD.Items.Add("closed " + CD.Exitcode.ToString()); });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CD.Shutdown();
            _tmr.Stop();
            _tmr.Close();
        }

        private void btnTestInventoryLoad_Click(object sender, RoutedEventArgs e)
        {
            
            inventory = DAL.SQL.Instance.LoadInventory();

        }

        private void btnAddToProduct_Click(object sender, RoutedEventArgs e)
        {
            BLL.Product prod = inventory[1];
            prod.Quantity = 3;
            var btn = sender as Button;
            btn.Content =  DAL.SQL.Instance.AddProductQuantity(prod).ToString();
            testprod.Quantity = testprod.Quantity + 10;

        }

        private void btnSubtractFromProduct_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            inventory[2].Quantity = 11;
            btn.Content = DAL.SQL.Instance.SubtractProductQuantity(inventory[2]).ToString();
            testprod.Quantity = testprod.Quantity - 1;
        }

        private void _tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            testprod.Quantity -= 1;
            if (testprod.Quantity == 1)
            {
                _tmr.Stop();
            }
        }
    }
}
