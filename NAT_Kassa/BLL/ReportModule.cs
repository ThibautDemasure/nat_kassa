﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class ReportModule
    {

        #region Fields

        private List<Transaction> _closedTransactions = new List<Transaction>();
        private List<Transaction> _tribestransactions = new List<Transaction>();
        private List<Transaction> _nautictransactions = new List<Transaction>();
        private List<Transaction> _flactransactions = new List<Transaction>();
        private List<User> _userpool = new List<User>();
        private List<string> _notifications = new List<string>();
        private ModuleState _modstate = ModuleState.Normal;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;

        private DateTime _beginDate = new DateTime();
        private DateTime _endDate = new DateTime();
        
        
        

        #endregion

        #region Constructor

        public ReportModule(List<User> userpool)
        {
            _userpool = userpool;
            _beginDate = _beginDate.AddYears(2000);
            _endDate = DateTime.Now;
            _closedTransactions = _da.LoadClosedTransactions(_beginDate, _endDate);
            FilterOrganizations();
            _notifications.Add("Closed transaction loaded until " + _endDate.ToString());
        }



        #endregion

        #region Properties
        /// <summary>
        /// Set or get the begin date. 
        /// </summary>
        public DateTime BeginDate { get => _beginDate; set => _beginDate = value; }
        /// <summary>
        /// set or get the end date.
        /// </summary>
        public DateTime EndDate { get => _endDate; set => _endDate = value; }

        /// <summary>
        /// Gets the module notifications.
        /// </summary>
        public List<string> Notifications { get => _notifications; }

        /// <summary>
        /// Get or set the module state.
        /// ModuleState makes it possible to have different actions (add, delete, restore) for the same touch action handler.
        /// </summary>
        public ModuleState ModState { get => _modstate; set => _modstate = value; }

        /// <summary>
        /// Gets all the selected transactions.
        /// </summary>
        public List<Transaction> ClosedTransactionpool { get => _closedTransactions; }


        public List<Product> Products
        {
            get
            {
                List<Product> products = new List<Product>();
                foreach (Transaction t in _closedTransactions)
                {
                    foreach (Product p in t.Products)
                    {
                        products.Add(p);
                    }
                }
                return products;
            }
        }

        public List<Payment> Payments
        {
            get
            {
                List<Payment> payments = new List<Payment>();
                foreach (Transaction t in _closedTransactions)
                {
                    foreach (Payment p in t.Payments)
                    {
                        payments.Add(new Payment(p.Id,p.Method,p.Amount));
                    }
                }
                return payments;
            }
        }

        public double TotaleAmount
        {
            get
            {
                double amount = 0.0;
                foreach (Transaction t in _closedTransactions)
                {
                    foreach (Payment payment in t.Payments)
                    {
                        if (payment.Method != PaymentMethod.Voucher)
                        {
                            amount += payment.Amount;
                        }
                    }
                }
                return amount;
            }
        }
        public double TotalePurchaseAmount
        {
            get
            {
                double amount = 0.0;
                foreach (Transaction t in _closedTransactions)
                {
                    foreach (Product p in t.Products)
                    {
                        amount += (p.Purchaseprice * p.Quantity);
                    }
                }
                return amount;
            }
        }

        public List<Transaction> Tribestransactions { get => _tribestransactions; set => _tribestransactions = value; }
        public List<Transaction> Nautictransactions { get => _nautictransactions; set => _nautictransactions = value; }
        public List<Transaction> Flactransactions { get => _flactransactions; set => _flactransactions = value; }

        #endregion

        #region Methods

        public void BookTransactions()
        {
            foreach (Transaction t in _closedTransactions)
            {
                t.State = TransactionState.Booked;
                _da.ChangeTransaction(t);

            }
        }

        public void ReloadTransactions()
        {
            if (BeginDate < EndDate)
            {
                _closedTransactions = _da.LoadClosedTransactions(_beginDate, _endDate);
                FilterOrganizations();
                _notifications.Add("Closed transaction loaded from " +_beginDate.ToString() + " until " + _endDate.ToString());
            }
        }

        public double FilteredAmount(OrganizationID org)
        {
            double amount = 0.0;
            foreach (Transaction t in _closedTransactions)
            {
                if (GetOrg(t.Closedby.Username) == org)
                {
                    foreach (Payment payment in t.Payments)
                    {
                        if (payment.Method != PaymentMethod.Voucher)
                        {
                            amount += payment.Amount;
                        }
                    }
                }   
            }
            return amount;
        }
        public double FilteredPurchaseAmount(OrganizationID org)
        {
            double amount = 0.0;
            foreach (Transaction t in _closedTransactions)
            {
                if (GetOrg(t.Closedby.Username) == org)
                {
                    foreach (Product p in t.Products)
                    {
                        amount += (p.Purchaseprice * p.Quantity);
                    }
                }
            }
            return amount;
        }

        /// <summary>
        /// Performs all the neccesary action for a safe shutdown of the module. Call on application shutdown.
        /// </summary>
        public void Shutdown()
        {

        }

        #endregion

        #region Helpers

        private void FilterOrganizations()
        {
            foreach (Transaction t in _closedTransactions)
            {
                if (t.Closedby != null)
                {
                    if (GetOrg(t.Closedby.Username) == OrganizationID.Tribes)
                    {
                        _tribestransactions.Add(t);
                    }
                    if (GetOrg(t.Closedby.Username) == OrganizationID.Nautic)
                    {
                        _nautictransactions.Add(t);
                    }
                    if (GetOrg(t.Closedby.Username) == OrganizationID.Flac)
                    {
                        _flactransactions.Add(t);
                    }
                }
                else
                {
                    ;
                }
            }
        }

        private OrganizationID GetOrg(string username)
        {
            foreach (User user in _userpool)
            {
                if (user.Username == username)
                {
                    return user.OrganizationID;
                }
            }
            return OrganizationID.None;
        }
        #endregion

    }
}
