using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace NAT_Kassa.BLL
{
    /// <summary>
    /// Datamodel of a User.
    /// </summary>
    public class User : INotifyPropertyChanged
    {
        #region Fields
        private int _id = 0;
        private string _name = "Dummy";
        private string _surname = "Test";
        private string _address = "";
        private string _email = "dummy@test.be";
        private OrganizationID _organizationid = OrganizationID.None;
        private Userlevel _userlevel = Userlevel.None;
        private string _pw;
        private bool _isactive = true;
        private bool _issynced = true;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        #endregion
        #region Constructors
        public User(string username)
        {
            _name = username.Split(' ')[0];
            _surname = username.Split(' ')[1];

        }
        public User(string name, string surname, string address, string email, OrganizationID organizationid, Userlevel userlevel, string pw)
        {
            _name = name;
            _surname = surname;
            _address = address;
            _email = email;
            _organizationid = organizationid;
            _userlevel = userlevel;
            _pw = pw;
        }
        public User(int id, string name, string surname, string address, string email, OrganizationID organizationid, Userlevel userlevel, string pw)
        {
            _id = id;
            _name = name;
            _surname = surname;
            _address = address;
            _email = email;
            _organizationid = organizationid;
            _userlevel = userlevel;
            _pw = pw;
        }
        #endregion
        #region Properties
        /// <summary>
        /// Get the username of the user. Username = "name surname".
        /// </summary>
        public string Username { get => _name+" "+_surname; }
        /// <summary>
        /// Get the Organisation enum.
        /// </summary>
        public OrganizationID OrganizationID { get => _organizationid; }
        public string Email { get => _email; }
        /// <summary>
        /// Gets the userlevel enum of the user.
        /// </summary>
        public Userlevel Userlevel { get => _userlevel; }
        /// <summary>
        /// Gets the HASHED password of the user.
        /// </summary>
        public string Password { get => _pw; }
        public string Name { get => _name; }
        /// <summary>
        /// Gets the surname (achternaam) of the user
        /// </summary>
        public string Surname { get => _surname; }
        public string Address { get => _address; }
        /// <summary>
        /// Gets or sets the UNIQUE identifier of the user.
        /// </summary>
        public int Id { get => _id; set => _id = value; }
        /// <summary>
        /// Gets or sets the activated property of the user. All user will be added to the userpool on application startup. This property can be used to filter the userpool or disable/enable a user without removing him/her.
        /// </summary>
        public bool IsActive
        {
            get => _isactive;
            set
            {
                if (_isactive != value)
                {
                    _isactive = value;
                    _da.ChangeUserActivation(this);
                    OnPropertyChanged("IsActive");
                }
                
            }
        }
        /// <summary>
        /// Indicates if the user data has been changed when the application was in offline state. Can be used for synchronising the database entry to the offline data.
        /// </summary>
        public bool IsSynced { get => _issynced; set => _issynced = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        #endregion
        #region Methods

        //String representation of the object
        public override string ToString()
        {
            return this.Username;
        }

        #endregion
        #region Helpers

        #endregion
    }

    /// <summary>
    /// Determines the rights a user has.
    /// </summary>
    public enum Userlevel
    {
        None, Guest, Admin, Super
    }
    public class UserComparer : IComparer<User>
    {
        private ComparerType _type;
        public UserComparer(ComparerType type)
        {
            _type = type;
        }
        public int Compare(User x, User y)
        {
            if (x == null || y == null)
            {
                return 0;
            }
            switch (_type)
            {
                case ComparerType.NAME_ASC:
                    return x.Username.CompareTo(y.Username);
                case ComparerType.NAME_DESC:
                    return y.Username.CompareTo(x.Username);
                case ComparerType.ID_ASC:
                    return x.Id.CompareTo(y.Id);
                case ComparerType.ID_DESC:
                    return y.Id.CompareTo(x.Id);
                default:
                    return x.Username.CompareTo(y.Username);
            }
        }
    }
}
