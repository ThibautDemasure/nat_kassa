﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    class Organization
    {
        #region Fields
        private string _name;
        private OrganizationID _id;
        private string _vat;
        private string _accountnr;
        private string _email;
        private User _president;
        #endregion
        #region Constructors
        public Organization(string name, OrganizationID id, string vat, string accountnr, string email, User president)
        {
            _name = name;
            _id = id;
            _vat = vat;
            _accountnr = accountnr;
            _email = email;
            _president = president;
        }
        #endregion
        #region Properties
        public OrganizationID OrganizationID { get => _id; }
        public string Name { get => _name; }
        public string Accountnr { get => _accountnr; }
        #endregion
        #region Methods

        #endregion
        #region Helpers

        #endregion
    }
    public enum OrganizationID
    {
        None, Tribes, Nautic, Flac, NAT
    }

}
