﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace NAT_Kassa.BLL
{
    /// <summary>
    /// Datamodel of a product.
    /// </summary>
    public class Product :INotifyPropertyChanged
    {

        #region Fields
        private int _id;
        private string _name;
        private double _price;
        private double _purchaseprice;
        private int _quantity;
        private ProductCategory _category; 
        private bool _active;
        private bool _available;
        private bool _synchronized;
        private int _warninglevel = 24;
        private bool _refilled;
        private bool _used = true;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor for creating a new product with an unknown ID, based on user input. The product is not synchronized by default. Quantity = 1. On synchronization ID is set from DB.
        /// </summary>
        /// <param name="name">Name of the product</param>
        /// <param name="price">Selling price of the product</param>
        /// <param name="purchaseprice">Puchase price of the product</param>
        /// <param name="category">Product category</param>
        public Product(string name, double price, double purchaseprice, ProductCategory category)
        {
            _id = 0;
            _name = name;
            _price = price;
            _purchaseprice = purchaseprice;
            _quantity = 1;
            _category = category;
            _active = true;
            _available = true;
            _synchronized = true;
            _refilled = true;
        }

        /// <summary>
        /// Constructor for creating a previously defined product from a datasource when quantity != 1. Use for productpool or when loading open transactions from datasource.
        /// </summary>
        /// <param name="name">name of the product</param>
        /// <param name="price">selling price</param>
        /// <param name="purchaseprice">purchase price</param>
        /// <param name="category"> category enum</param>
        /// <param name="id"> unique identifier of the product</param>
        /// <param name="quantity">quantity of products</param>
        /// <param name="active">enabled or disabled product</param>
        /// <param name="synchronized">synchronized product or only defined local</param>
        public Product(string name, double price, double purchaseprice, ProductCategory category, int id, int quantity, bool active, bool synchronized)
        {
            _id = id;
            _name = name;
            _price = price;
            _purchaseprice = purchaseprice;
            _quantity = quantity;
            _category = category;
            _active = active;
            if (quantity <= 0) { _available = false; _refilled = false; }
            else { _available = true; _refilled = true; }
            _synchronized = synchronized;

        }

        /// <summary>
        /// Constructor to clone a Product instance.
        /// </summary>
        /// <param name="p">product to clone</param>
        public Product(Product p)
        {
            _id = p.Id;
            _name = p.Name;
            _price = p.Price;
            _purchaseprice = p.Purchaseprice;
            _quantity = p.Quantity;
            _category = p.Category;
            _active = p.IsActive;
            if (p.Quantity <= 0) { _available = false; _refilled = false; }
            else { _available = true; _refilled = true; }
            _synchronized = p.IsSynchronized;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the unique identifier of the product.
        /// </summary>
        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; }
        /// <summary>
        /// Gets the price of the product in euro.
        /// </summary>
        public double Price { get => _price; }
        public ProductCategory Category { get => _category; set => _category = value; }
        /// <summary>
        /// Gets or sets the current product quantity
        /// </summary>
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (_quantity != value)
                {
                    _quantity = value;
                    if (_quantity <= _warninglevel)
                    {
                        _refilled = false;
                        OnQuantityChanged(EventArgs.Empty);
                    }
                    if (_quantity > _warninglevel && _refilled == false)
                    {
                        _refilled = true;
                        OnRefilled(EventArgs.Empty);
                    }
                    if (_quantity <= 0 && _available == true)
                    {
                        _available = false;
                        OnUnavailable(EventArgs.Empty);
                    }
                    if(_available == false && _quantity > 0)
                    {
                        _available = true;
                        OnAvailable(EventArgs.Empty);
                    }
                    
                }
            }
        }
        /// <summary>
        /// Gets the purchase price of the product. Always save this together with price so an individual profit can be calculated afterwards.
        /// </summary>
        public double Purchaseprice { get => _purchaseprice;}
        /// <summary>
        /// Gets the availability of the product. A product becomes unavailable is quantity = 0.
        /// </summary>
        public bool IsAvailable { get => _available; }
        /// <summary>
        /// Gets or sets the activated property of the product. All products will be added to the productpool on application startup. This property can be used to filter the pool or disable/enable a product without removing it.
        /// </summary>
        public bool IsActive
        {
            get => _active;
            set
            {
                if (_active != value)
                {
                    _active = value;
                    _da.ChangeProductActivation(this);
                    OnPropertyChanged("IsActive");
                }
            }
        }
        /// <summary>
        /// Indicates if the product data has been changed when the application was in offline state. Can be used for synchronising the database entry to the offline data.
        /// </summary>
        public bool IsSynchronized { get => _synchronized; set => _synchronized = value; }
        public int Warninglevel {
            get { return _warninglevel; }
            set
            {
                _warninglevel = value;
                if (_quantity <= _warninglevel)
                { _refilled = false; }
                else { _refilled = true; }
            }
        }

        public bool Used { get => _used; set => _used = value; }

        #endregion


        #region Methods
        public override string ToString()
        {
            return _name + " x " +  _quantity.ToString();
        }
        /// <summary>
        /// Add products to current quantity.
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns>updated quantity</returns>
        public int Add(int quantity)
        {
            _quantity += quantity;
            if (_quantity <= 0) { _available = false; }
            else { _available = true; }
            return _quantity;
        }
        /// <summary>
        /// Subtract products from current quantity.
        /// </summary>
        /// <param name="quantity"></param>
        /// <returns>updated quantity</returns>
        public int Substract(int quantity)
        {
            if (quantity <= _quantity)
            {
                _quantity -= quantity;
                if (_quantity <= 0) { _available = false; }
                else { _available = true; }
                return _quantity;
            }
            _quantity = 0;
            _available = false;
            return _quantity;
        }
        #endregion

        #region Events
        public event EventHandler Unavailable;
        protected virtual void OnUnavailable(EventArgs e)
        {
            EventHandler handler = Unavailable;
            handler?.Invoke(this, e);
        }
        public event EventHandler Available;
        protected virtual void OnAvailable(EventArgs e)
        {
            EventHandler handler = Available;
            handler?.Invoke(this, e);
        }
        public event EventHandler QuantityChanged;
        protected virtual void OnQuantityChanged(EventArgs e)
        {
            EventHandler handler = QuantityChanged;
            handler?.Invoke(this, e);
        }
        public event EventHandler Refilled;
        protected virtual void OnRefilled(EventArgs e)
        {
            EventHandler handler = Refilled;
            handler?.Invoke(this, e);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion


        #region Helpers

        #endregion
    }

    /// <summary>
    /// Category to which a product belongs.
    /// </summary>
    public enum ProductCategory
    {
        Water, Frisdrank, Sportdrank, Bier, Bubbels, Wijn, Fruitsap, Warme_Dranken, Spirits, Snacks, Diverse, None
    }
    /// <summary>
    /// Typing enum for different comparisons
    /// </summary>
    public enum ComparerType
    {
        NAME_ASC, NAME_DESC, ID_ASC, ID_DESC,
    }
    /// <summary>
    /// Comparer Class for Products
    /// </summary>
    public class ProductComparer : IComparer<Product>
    {
        private ComparerType _type;
        public ProductComparer(ComparerType type)
        {
            _type = type;
        }
        public int Compare(Product x, Product y)
        {
            if (x == null || y == null)
            {
                return 0;
            }
            switch (_type)
            {
                case ComparerType.NAME_ASC:
                    return x.Name.CompareTo(y.Name);
                case ComparerType.NAME_DESC:
                    return y.Name.CompareTo(x.Name);
                case ComparerType.ID_ASC:
                    return x.Id.CompareTo(y.Id);
                case ComparerType.ID_DESC:
                    return y.Id.CompareTo(x.Id);
                default:
                    return x.Name.CompareTo(y.Name);
            }
        }
    }
}
