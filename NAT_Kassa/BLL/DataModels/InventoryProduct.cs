﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class InventoryProduct : Product
    {
        #region Fields
        //add further fields here
        private int _packedquantity;
        private int _targetQ;
        private int _minQ;
        private int _orderQ;

        #endregion

        #region Constructor
        public InventoryProduct(string name, double price, double purchaseprice, ProductCategory category, int id, int quantity, bool active, bool synchronized, int packedQ, int targetQ, int minQ, int orderQ) : base(name,price,purchaseprice,category,id,quantity,active,synchronized)   
        {
            //Base constructor is called before this code block
            //Do further instructions here
            _packedquantity = packedQ;
            _targetQ = targetQ;
            _minQ = minQ;
            _orderQ = orderQ;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the quantity of single units in 1 package.
        /// </summary>
        public int PackedQuantity { get => _packedquantity; set => _packedquantity = value; }
        /// <summary>
        /// Gets or sets the standard target quantity of packages to have in the inventory after order.
        /// </summary>
        public int TargetQ { get => _targetQ; set => _targetQ = value; }
        /// <summary>
        /// Gets or sets the minimum quantity of packages that should be in inventory at all time.
        /// </summary>
        public int MinQ { get => _minQ; set => _minQ = value; }
        /// <summary>
        /// Gets or sets the standard quantity of packages to order.
        /// </summary>
        public int OrderQ { get => _orderQ; set => _orderQ = value; }

        public bool IsLow
        {
            get
            {
                if (Quantity <= _minQ * _packedquantity) { return true; }
                return false;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Calculates OrderQ (in packages) based on set targetQ and minQ. Order Q can also be set directly by using property. 
        /// </summary>
        /// <returns></returns>
        public int CalculateCurrentOrderQ()
        {
            if (Quantity <= _minQ)
            {
                _orderQ = _targetQ - Quantity / _packedquantity;
                return _orderQ;
            }
            return 0;

        }


        #endregion
    }
    public class InventoryTransaction
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Remark { get; set; }
        public string DeliveryNote { get; set; }
        public int Type { get; set; }
        public string NamedType { get; set; }
        public string UserName { get; set; }
        public override string ToString()
        {
            if (Type == 5 || Type == 2)
            {
                return Id.ToString() + ": " + NamedType + " | " + DeliveryNote + " | " + Timestamp.ToString("HH:mm:ss");
            }
            return Id.ToString() + ": " + NamedType + " | " + Remark + " | " + Timestamp.ToString("HH:mm:ss");

        }
    }

}
