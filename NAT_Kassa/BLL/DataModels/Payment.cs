﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class Payment
    {
        #region Fields
        private int _id;
        private PaymentMethod _method;
        private double _amount;
        private DateTime _transactiontimestamp;
        #endregion

        #region Constructors
        public Payment(int id, PaymentMethod method, double amount)
        {
            _id = id;
            _method = method;
            _amount = amount;
            _transactiontimestamp = System.DateTime.Now;
        }
        
        #endregion

        #region Properties
        /// <summary>
        /// Get payment Id. Seeded by database.
        /// </summary>
        public int Id { get => _id; }
        /// <summary>
        /// Gets the payment method enum.
        /// </summary>
        public PaymentMethod Method { get => _method; }
        public double Amount { get => _amount; }
        /// <summary>
        /// Gets or sets the timestamp of the payment.
        /// </summary>
        public DateTime TransactionTimestamp { get => _transactiontimestamp; set => _transactiontimestamp = value; }
        public double CreditAfter { get; set; }
        #endregion

        #region Methods
        public void SetId(int id)
        {
            _id = id;
        }

        public override string ToString()
        {
            if (_method == PaymentMethod.MemberCard)
            {
                return _id.ToString() + ": NFC Card €" + _amount.ToString() + " => Saldo €" + CreditAfter.ToString();
            }
            return _id.ToString() + ": " + _method.ToString() + " €" + _amount.ToString();
        }
        #endregion
    }
    
    public enum PaymentMethod
    {
        None, Cash, Bancontact, Payconiq, Voucher, MemberCard
    }

}
