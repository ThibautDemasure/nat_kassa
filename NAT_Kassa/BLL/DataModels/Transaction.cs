﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class Transaction
    {
        #region Fields
        private int _transactionid;
        private string _description = "";
        private List<Product> _products = new List<Product>();
        private double _amount;
        private double _pamount;
        private TransactionState _state = TransactionState.Open;
        private List<Payment> _payments = new List<Payment>();
        private User _openedby;
        private User _closedby;
        private DateTime _dtclosed;
        private DateTime _dtopened;
        private int _Synced = 1;
        #endregion

        #region Constructors
        public Transaction(int id, User cashier)
        {
            _openedby = cashier;
            _transactionid = id;
            _dtopened = System.DateTime.Now;
        }
        public Transaction(int id, User cashier, string description)
        {
            _openedby = cashier;
            _transactionid = id;
            _description = description;
            _dtopened = System.DateTime.Now;
        }
        #endregion

        #region Properties
        public int Id { get => _transactionid; }
        public string Description { get => _description;
            set
            {
                if (value != _description)
                {
                    _description = value;
                    DescriptionChanged(EventArgs.Empty);
                }
               
            }
        }
        public List<Product> Products { get => _products; set => _products = value; }
        public int Count {
        get {
          int val = 0;
          foreach (Product product in _products) {
            val += product.Quantity;
          }
          return val;
          }
        }
        /// <summary>
        /// Sums the product prices.
        /// </summary>
        public double Amount
        {
            get
            {
                _amount = 0;
                foreach (Product p in _products)
                {
                    _amount += (p.Price * p.Quantity);
                }
                return _amount;
            }
        }
        /// <summary>
        /// Sums the product prices.
        /// </summary>
        public double AmountPurchase
        {
            get
            {
                _pamount = 0;
                foreach (Product p in _products)
                {
                    _pamount += (p.Purchaseprice * p.Quantity);
                }
                return _pamount;
            }
        }
        /// <summary>
        /// Sums the payments.
        /// </summary>
        public double AmountPaid
        {
            get
            {
                double ap = 0;
                foreach (Payment p in _payments)
                {
                    ap += p.Amount;
                }
                return ap;
            }
        }
        /// <summary>
        /// product prices - payments
        /// </summary>
        public double AmountOpen
        {
            get
            {
                return Amount - AmountPaid;
            }
        }
        public TransactionState State { get => _state; set => _state = value; }
        public User Openedby { get => _openedby; }
        public User Closedby { get => _closedby; }
        public List<Payment> Payments { get => _payments; }
        public DateTime ClosedTimestamp { get => _dtclosed; set => _dtclosed = value; }
        public DateTime OpenedTimestamp { get => _dtopened; set => _dtopened = value; }
        public int Synced { get => _Synced; set => _Synced = value; }
        #endregion

        #region Events
        public event EventHandler OnDescriptionChanged;
        protected virtual void DescriptionChanged(EventArgs e)
        {
            EventHandler handler = OnDescriptionChanged;
            handler?.Invoke(this, e);
        }
        public event EventHandler OnAmountChanged;
        protected virtual void AmountChanged(EventArgs e)
        {
            EventHandler handler = OnAmountChanged;
            handler?.Invoke(this, e);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Adds a product to the transaction. Quantity is embedded in the product.Quantity property.
        /// Use product.Add or constructor to change quantity.
        /// A new product is created interally so the reference to the original productpool is definitely removed. 
        /// </summary>
        /// <param name="product">The product that needs to be added</param>
        public void AddProduct(Product product)
        {
            Product p = this.HasProduct(product.Name, product.Purchaseprice, product.Price);
            if (p != null)
            {
                p.Add(product.Quantity);
                UpdateState();
                AmountChanged(EventArgs.Empty);
                return;
            }
            _products.Add(new Product(product.Name,product.Price,product.Purchaseprice,product.Category,product.Id,product.Quantity,product.IsActive,product.IsSynchronized));
            UpdateState();
            AmountChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Removes a product completely out of the transaction.
        /// </summary>
        /// <param name="product">Product to remove</param>
        public void RemoveProduct(Product product)
        {
            Product p = this.HasProduct(product.Name, product.Purchaseprice, product.Price);
            if (p != null)
            {
                _products.Remove(p);
                UpdateState();
                AmountChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Substracts a quantity of a product in the transaction. Quantity is embedded in product.Count.
        /// If result equals 0 then product is removed from transaction.
        /// </summary>
        /// <param name="product">Product to remove</param>
        public void SubstractProduct(Product product)
        {
            Product p = this.HasProduct(product.Name, product.Purchaseprice, product.Price);
            if (p != null)
            {
                p.Substract(product.Quantity);
                if (p.Quantity == 0)
                {
                    _products.Remove(p);
                }
                UpdateState();
                AmountChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Adds a payment to the transaction.
        /// </summary>
        /// <param name="payment">Payment to add</param>
        public void Pay(Payment payment)
        {
            _payments.Add(payment);
            UpdateState();
            AmountChanged(EventArgs.Empty);
        }
        public void Pay(int id, PaymentMethod method, double amount)
        {
            Payment payment = new Payment(id, method, amount);
            _payments.Add(payment);
            UpdateState();
            AmountChanged(EventArgs.Empty);
        }

        public void Refund(Payment payment)
        {
            foreach (Payment p in _payments)
            {
                if(p.Id == payment.Id)
                {
                    _payments.Remove(p);
                    break;
                }
            }
            UpdateState();
            AmountChanged(EventArgs.Empty);
        }
        public void Refund(int paymentid)
        {
            foreach (Payment p in _payments)
            {
                if (p.Id == paymentid)
                {
                    _payments.Remove(p);
                    break;
                }
            }
            UpdateState();
            AmountChanged(EventArgs.Empty);
        }


        /// <summary>
        /// Closes a transaction when paid fully. The closing user determines the organization that will be credited with the payments.
        /// </summary>
        /// <param name="cashier">User that closes the transaction.</param>
        public void CloseTransaction (User cashier, bool forceclose = false)
        {
            if (_state == TransactionState.PaidFull || forceclose)
            {
                _closedby = cashier;
                _state = TransactionState.Closed;
                _dtclosed = System.DateTime.Now;
            }

        }

        public void SetId(int Id)
        {
            _transactionid = Id;
        }

        public override string ToString()
        {
            return _transactionid.ToString() + ": " + _closedby.Username;
        }
        #endregion

        #region Helpers

        private Product HasProduct(string productname, double purchaseprice, double price)
        {
            if (_products.Count != 0)
            {
                foreach (Product p in _products)
                {
                    if (p.Name == productname && p.Purchaseprice == purchaseprice && p.Price == price)
                    {
                        return p;
                    }
                }
            }
            return null;
        }
            
        private void UpdateState()
        {
           if(AmountPaid > 0 && AmountPaid < Amount )
            {
                _state = TransactionState.PaidPartial;
                return;
            }
           if(AmountPaid > 0 && AmountPaid >= Amount)
            {
                _state = TransactionState.PaidFull;
                return;
            }
            _state = TransactionState.Open;
        }
        #endregion

    }
    public enum TransactionState
    {
        Open, PaidPartial, PaidFull, Closed, Booked
    }
}
