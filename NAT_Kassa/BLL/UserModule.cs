﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Collections.ObjectModel;
namespace NAT_Kassa.BLL
{
    public class UserModule
    {
        #region Fields
        private List<User> _userpool;
        private User _currentuser;
        private List<string> _notifications = new List<string>();
        private ModuleState _modstate = ModuleState.Normal;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        #endregion

        #region Constructors
        public UserModule()
        {
            _userpool = _da.LoadUsers();
            ObservableCollection<User> users = new ObservableCollection<User>();
            _userpool.Sort(new UserComparer(ComparerType.NAME_ASC));
            foreach (User u in _userpool)
            {
                users.Add(u);
            }
            _userpool.Sort(new UserComparer(ComparerType.ID_ASC));
            Users = users;
            _notifications.Add("Users loaded from datasource.");
            _da.UpdateZeroUser += _da_UpdateZeroUser;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets the current loged on user. Returns a dummy user if no user is loged on -> UL = None
        /// </summary>
        public User CurrentUser { get { if (_currentuser != null) { return _currentuser; } return new User("No User"); } }

        public bool IsLoggedOn
        {
            get
            {
                return (_currentuser != null);
            }
        }

        /// <summary>
        /// Gets the module notifications.
        /// </summary>
        public List<string> Notifications { get => _notifications; }

        public ObservableCollection<User> Users { get; set; }

        /// <summary>
        /// Gets all the registered users.
        /// </summary>
        public List<User> Userpool { get => _userpool; }
        /// <summary>
        /// Get or set the module state.
        /// ModuleState makes it possible to have different actions (add, delete, restore) for the same touch action handler.
        /// </summary>
        public ModuleState ModState { get => _modstate; set => _modstate = value; }
        #endregion

        #region Methods

        private void _da_UpdateZeroUser(object sender, EventArgs e)
        {
            foreach (User user in _userpool)
            {
                if (user.Id == 0)
                {
                    user.Id = _da.ZeroUserId;
                }
            }
            if (_currentuser.Id == 0) { _currentuser.Id = _da.ZeroUserId; }
        }

        /// <summary>
        /// Logs a user on to the app. Checks if user is registered before login.
        /// </summary>
        /// <param name="userid">Username or email</param>
        /// <param name="password">Hashed password</param>
        /// <returns>True if action succeeded, otherwise false</returns>
        public bool Login(string userid, string password)
        {
            //check if user is in userpool
            User u = GetUser(userid);
            if (u != null && u.Password == password)
            {
                if (_currentuser != null)
                {
                    //Users.Add(_currentuser);
                    //ObservableCollection<User> samestart = (ObservableCollection<User>)Users.Where(p => p.Name.StartsWith(_currentuser.Name.Substring(0)));
                    InsertUserAlphabetically(_currentuser);
                }
                _currentuser = u;
                Users.Remove(_currentuser);
                _notifications.Add("Login success: " + _currentuser.Username);
                return true;
            }
            _notifications.Add("User or password incorrect. Please retry or register a new user");
            return false;
        }
        
        /// <summary>
        /// Registers and logs on a user, if no corresponding user exists.
        /// </summary>
        /// <param name="name">Voornaam</param>
        /// <param name="surname">Achternaam</param>
        /// <param name="email"></param>
        /// <param name="pw">Encrypted or hashed password</param>
        /// <param name="organizationid">OPTIONAL: Tribes, Flac, Nautice or None, standard = None</param>
        /// <param name="userlevel">OPTIONAL: Guest, Normal, Admin, standard = Normal</param>
        /// <param name="address">OPTIONAL</param>
        /// <returns></returns>
        public bool Register (string name, string surname, string email, string pw, OrganizationID organizationid = OrganizationID.None, Userlevel userlevel = Userlevel.Guest, string address = "")
        {
            User newu = new User(name, surname, address, email, organizationid, userlevel, pw);
            if (IsUser(newu.Username, newu.Email))
            {
                _notifications.Add("Username or Email already exists. Login instead or use different email or username.");
                return false;
            }
            _userpool.Add(newu);
            //Users.Add(newu);
            if (_currentuser != null)
            {
                InsertUserAlphabetically(_currentuser);
            }
            _currentuser = newu;
            _notifications.Add("User Registered and Logged on: " + _currentuser.Username);
            _da.AddUser(newu);
            return true;
        }
        
        public bool EditUser(User changeduser)
        {
            foreach (User u in _userpool)
            {
                if (u.Email == changeduser.Email && u.Id != changeduser.Id)
                {
                    _notifications.Add("Email already exists for other user. Current user not changed.");
                    return false;
                }
                if (u.Username == changeduser.Username && u.Id != changeduser.Id)
                {
                    _notifications.Add("Username already exists for other user. Current user not changed.");
                    return false;
                }
            }
            if (SetUser(changeduser) != null)
            {
                _da.ChangeUser(changeduser);
                _currentuser = changeduser;
                _notifications.Add("User with Id " + changeduser.Id + " is changed.");
                return true;
            }
            else
            {
                _notifications.Add("User not found. Please retry user edit. If problem keeps occuring, contact system admin.");
                return false;
            }
        }

        /// <summary>
        /// Delete a user entry from the system. Only possible with Admin userlevel.
        /// </summary>
        /// <param name="userid">The user that will be deleted. Specify username or email.</param>
        public void DeleteUser (string userid)
        {
            if (_currentuser.Userlevel == Userlevel.Admin || _currentuser.Userlevel == Userlevel.Super)
            {
                User u = GetUser(userid);
                if (u != null)
                {
                    _userpool.Remove(u);
                    _notifications.Add("User " + u.Username + " deleted by Admin" + _currentuser.Username);
                    _da.DeleteUser(u);
                    return;
                }
            }
            _notifications.Add("User not deleted. No such user exists or no Admin rights.");
        }

        public void DeactivateUser(User selecteduser)
        {
            if (_currentuser != null && _currentuser.Userlevel >= Userlevel.Guest)
            {
                selecteduser.IsActive = false;
                _notifications.Add("User " + selecteduser.Username + " deactivated");
                return;
            }
            _notifications.Add("User not deactivated");
        }
        public void ActivateUser(User selecteduser)
        {
            if (_currentuser != null && _currentuser.Userlevel >= Userlevel.Admin)
            {
                selecteduser.IsActive = true;
                _notifications.Add("User " + selecteduser.Username + " activated");
                return;
            }
            _notifications.Add("User not activated");
        }

        /// <summary>
        /// Log off the current user.
        /// </summary>
        public void Logout()
        {
            if (_currentuser != null)
            {
                InsertUserAlphabetically(_currentuser);
            }
            _currentuser = null;
            _notifications.Add("Logged off");
        }

        /// <summary>
        /// Performs all the neccesary action for a safe shutdown of the module. Call on application shutdown.
        /// </summary>
        public void Shutdown()
        {

        }

        /// <summary>
        /// Hashes an inputstring. Used to store passwords.
        /// </summary>
        /// <param name="password">String to hash.</param>
        /// <returns></returns>
        public string HashPassword (string password)
        {
            SHA256 hasher = SHA256.Create();
            byte[] pwbytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(password));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < pwbytes.Length; i++)
            {
                builder.Append(pwbytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Checks if userpool has a corresponding username or email. Specify either email or username.
        /// </summary>
        /// <param name="userid">username or email</param>
        /// <returns></returns>
        private bool IsUser (string userid)
        {
            foreach (User u in _userpool)
            {
                if (u.Username == userid || u.Email == userid )
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if userpool has a corresponding username or email. Specify both username and email.
        /// </summary>
        /// <param name="username">Name Surname</param>
        /// <param name="email">Email address</param>
        /// <returns></returns>
        private bool IsUser(string username, string email)
        {
            foreach (User u in _userpool)
            {
                if (u.Username == username || u.Email == email)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets a user from userpool if exists otherwise returns null.
        /// </summary>
        /// <param name="userid">username or email</param>
        /// <returns></returns>
        private User GetUser (string userid)
        {
            foreach (User u in _userpool)
            {
                if (u.Username == userid || u.Email == userid)
                {
                    return u;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets a user in the userpool.
        /// </summary>
        /// <param name="userid">id of user to set</param>
        /// <returns></returns>
        private User SetUser(User changeduser)
        {
            int indextochange = -1;
            foreach (User u in _userpool)
            {
                if (u.Id == changeduser.Id)
                {
                    indextochange = _userpool.IndexOf(u);
                }
            }
            if (indextochange != -1)
            {
                _userpool[indextochange] = changeduser;
                return _userpool[indextochange];
            }
            return null;
        }

        private void InsertUserAlphabetically(User selecteduser)
        {
            UserComparer cmp = new UserComparer(ComparerType.NAME_ASC);
            foreach (User item in Users)
            {
                if (cmp.Compare(selecteduser, item) < 0)
                {
                    Users.Insert(Users.IndexOf(item), selecteduser);
                    return;
                }
            }
            Users.Add(selecteduser);
        }
        #endregion
    }

    public enum ModuleState
    {
        Normal, Deleting, Recycling, Restoring, Saving, Loading, Adding, Locked, Renewing
    }
}
