﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class ProductsModule
    {
        #region Fields
        private List<Product> _productpool;
        private List<string> _notifications = new List<string>();
        private ModuleState _modstate = ModuleState.Normal;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        private ComparerType _currentsort;
        #endregion

        #region Constructors
        public ProductsModule()
        {
            _productpool = _da.LoadProducts();
            Sort(ComparerType.NAME_ASC);
            _notifications.Add("Products loaded from datasource.");
            _da.UpdateZeroProduct += _da_UpdateZeroProduct;
        }

        private void _da_UpdateZeroProduct(object sender, EventArgs e)
        {
            foreach (Product prod in _productpool)
            {
                if (prod.Id == 0)
                {
                    prod.Id = _da.ZeroProductId;
                }
            }
            
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the module notifications.
        /// </summary>
        public List<string> Notifications { get => _notifications; }

        /// <summary>
        /// Gets all the available products.
        /// </summary>
        public List<Product> Productpool { get => _productpool; }

        /// <summary>
        /// Get or set the module state.
        /// ModuleState makes it possible to have different actions (add, delete, restore) for the same touch action handler.
        /// </summary>
        public ModuleState ModState { get => _modstate; set => _modstate = value; }
        public ComparerType CurrentSortMethod { get => _currentsort; set => _currentsort = value; }

        #endregion

        #region Methods

        public void UpdateCurrentQuantity(InventoryProduct product)
        {
            if (product != null)
            {
                foreach (Product item in _productpool)
                {
                    if (item.Id == product.Id)
                    {
                        item.Quantity = product.Quantity;
                        _notifications.Add("Product " + item.Name + " updated. Current Quantity = " + item.Quantity.ToString());

                        return;
                    }
                }
                _notifications.Add("Product not found.");
                return;
            }
            _notifications.Add("Product is null");
            return;
        }
        /// <summary>
        /// Sort products according to different comparisons
        /// </summary>
        /// <param name="type">type of comparison, specifies result of sort</param>
        public void Sort(ComparerType type)
        {
            ProductComparer cmpr = new ProductComparer(type);
            _productpool.Sort(cmpr);
            _currentsort = type;
        }

        /// <summary>
        /// Filters the productlist by the specified category.
        /// </summary>
        /// <param name="cat">Category to filter content</param>
        /// <returns>A filtered list of type Product containing the matches</returns>
        public List<Product> FilteredProducts(ProductCategory cat)
        {
            List<Product> products = new List<Product>();
            foreach (Product p in _productpool)
            {
                if (p.Category == cat)
                {
                    products.Add(p);
                }
            }
            return products;
        }

        /// <summary>
        /// Add a new product by name, price and category if product doesn't exist.
        /// </summary>
        /// <param name="name">Unique name of the product</param>
        /// <param name="price">Price in euro</param>
        /// <param name="cat">Category: Sodas, Beers, Snacks, ...</param>
        /// <returns></returns>
        public bool AddProduct (string name, double price, double purchaseprice, int packQ, int InitQ, ProductCategory cat = ProductCategory.Diverse)
        {
            Product newp = new Product(name, price,purchaseprice,cat);
            newp.Quantity = packQ * InitQ;
            newp.Warninglevel = packQ * 2;
            
            foreach (Product p in _productpool)
            {
                if (p.Name == newp.Name)
                {
                    _notifications.Add("A product with this name already exists.");
                    return false;
                }
            }
            _da.Addproduct(newp);
            _notifications.Add("New product: " + newp.ToString() + " defined.");
            _productpool.Add(newp);
            return true;
        }

        /// <summary>
        /// Add a new product by using the product data model. 
        /// </summary>
        /// <param name="product">Product to add</param>
        /// <returns></returns>
        public bool AddProduct(Product product)
        {
            Product newp = product;
            foreach (Product p in _productpool)
            {
                if (p.Name == newp.Name)
                {
                    _notifications.Add("A product with this name already exists.");
                    return false;
                }
            }
            _productpool.Add(newp);
            _notifications.Add("Product: " + newp.Name + " added.");
            _da.Addproduct(newp);
            return true;
        }

        /// <summary>
        /// Deletes a product by name.
        /// </summary>
        /// <param name="name">Unique name of the product to delete</param>
        /// <returns>Successfull or not</returns>
        public bool DeleteProduct (string name)
        {
            if (_modstate == ModuleState.Deleting)
            {
                foreach (Product p in _productpool)
                {
                    if (p.Name == name)
                    {
                        _productpool.Remove(p);
                        _notifications.Add("Product: " + name + " is deleted");
                        _da.DeleteProduct(p);
                        return true;
                    }
                }
                _notifications.Add("No product with name " + name + " exists");
                return false;
            }
            _notifications.Add("ProductModule not in deletion state.");
            return false;
        }
        public bool DeactivateProduct(string name)
        {
            if (_modstate == ModuleState.Deleting)
            {
                foreach (Product p in _productpool)
                {
                    if (p.Name == name)
                    {
                        p.IsActive = false;
                        _da.ChangeProductActivation(p);
                        _notifications.Add("Product: " + name + " is deactivated");
                        return true;
                    }
                }
                _notifications.Add("No product with name " + name + " exists");
                return false;
            }
            _notifications.Add("ProductModule not in deletion state.");
            return false;
        }

        public bool EditProduct (Product prod, ProductCategory oldcat)
        {
            foreach (Product p in _productpool)
            {
                if (p.Name == prod.Name && p.Id != prod.Id)
                {
                    _notifications.Add("Product name already exists for other product. Current product not changed.");
                    return false;
                }
            }
            if (SetProduct(prod) != null)
            {
                _da.ChangeProduct(prod, oldcat);
                _notifications.Add("Product with Id " + prod.Id + " is changed.");
                return true;
            }
            else
            {
                _notifications.Add("Product not found. Please retry. If problem keeps occuring, contact system admin.");
                return false;
            }
        }

        public bool SelProduct(Product product)
        {
            if (product != null)
            {
                foreach (Product item in _productpool)
                {
                    if (item.Id == product.Id)
                    {
                        item.Quantity = _da.SelProduct(product);
                        _notifications.Add("Product " + item.Name + " sold. Current Quantity = " + item.Quantity.ToString());

                        return true;
                    }
                }
                _notifications.Add("Product not found.");
                return false;
            }
            _notifications.Add("Product is null");
            return false;
        }
        public bool ReturnProduct(Product product)
        {
            if (product != null)
            {
                foreach (Product item in _productpool)
                {
                    if (item.Id == product.Id)
                    {
                        item.Quantity = _da.BuyProduct(product);
                        _notifications.Add("Product " + item.Name + " returned. Current Quantity = " + item.Quantity.ToString());
                        return true;
                    }
                }
                _notifications.Add("Product not found.");
                return false;
            }
            _notifications.Add("Product is null");
            return false;
        }

        /// <summary>
        /// Performs all the neccesary action for a safe shutdown of the module. Call on application shutdown.
        /// </summary>
        public void Shutdown()
        {

        }

        #endregion

        #region Helpers
        private Product SetProduct(Product changedprod)
        {
            int indextochange = -1;
            foreach (Product p in _productpool)
            {
                if (p.Id == changedprod.Id)
                {
                    indextochange = _productpool.IndexOf(p);
                }
            }
            if (indextochange != -1)
            {
                _productpool[indextochange] = changedprod;
                return _productpool[indextochange];
            }
            return null;
        }

        #endregion
    }
}