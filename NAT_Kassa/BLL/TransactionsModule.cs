﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class TransactionsModule
    {
        #region Fields
        private List<Transaction> _transactionpool = new List<Transaction>();
        private List<Transaction> _recyclebin = new List<Transaction>();
        private List<string> _notifications = new List<string>();
        private ModuleState _modstate = ModuleState.Normal;
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        #endregion

        #region Constructors
        public TransactionsModule()
        {
            _transactionpool = _da.LoadTransactions();
            _notifications.Add("Transactions loaded from datasource.");
            _da.UpdateTransactionIds += _da_UpdateTransactionIds;
        }

        private void _da_UpdateTransactionIds(object sender, EventArgs e)
        {
            _transactionpool = _da.LoadTransactions();
            _notifications.Add("Transactions updated from datasource.");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets all the active transactions.
        /// </summary>
        public List<Transaction> Transactionpool { get => _transactionpool; }

        /// <summary>
        /// Gets the module notifications.
        /// </summary>
        public List<string> Notifications { get => _notifications; }

        /// <summary>
        /// Gets all the removed transactions.
        /// </summary>
        public List<Transaction> Recyclebin { get => _recyclebin; }

        /// <summary>
        /// Get or set the module state.
        /// ModuleState makes it possible to have different actions (add, delete, restore) for the same touch action handler.
        /// </summary>
        public ModuleState ModState { get => _modstate; set => _modstate = value; }

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new transaction to the transactionpool.
        /// </summary>
        /// <param name="cashier">Current User of the app.</param>
        /// <param name="description">Optional description for the transaction</param>
        public bool AddTransaction(User cashier, string description = "")
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. No transaction added. Try again after status is ONLINE.");
                return false;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. No transaction added. Try again after status is ONLINE.");
                    return false;
                }
                if (cashier.Userlevel != Userlevel.None)
                {
                    Transaction newT = new Transaction(0, cashier, description);
                    _da.AddTransaction(newT);
                    _transactionpool.Add(newT);
                    _notifications.Add("New transaction opened by " + cashier.Username + ". Transaction: " + newT.Id);
                    return true;
                }
                _notifications.Add("Userlevel does not allow to create new transactions.");
                return false;
            }
        }

        /// <summary>
        /// Splits products of a master bill into a new seperate bill.
        /// </summary>
        /// <param name="transactionid">Id of the master bill</param>
        /// <param name="products">Products to split off</param>
        /// <param name="cashier">Current user</param>
        /// <param name="description">Optional: description for new bill</param>
        public void SplitTransaction(int transactionid, List<Product> products, User cashier, string description = "")
        {
            foreach (Transaction t in _transactionpool)
            {
                if (t.Id == transactionid)
                {
                    description = "Split from: " + transactionid.ToString();
                    Transaction newT = new Transaction(0, cashier, description);
                    foreach (Product p in products)
                    {
                        t.SubstractProduct(p);
                        newT.AddProduct(p);
                    }
                    _da.AddTransaction(newT);
                    _transactionpool.Add(newT);
                    _da.ChangeTransaction(t);
                    _notifications.Add("Split succesfull. New Transaction created with id: " + newT.Id);
                    break;
                }
            }
        }

        /// <summary>
        /// Moves a transaction to the recycle bin and deletes from file.
        /// </summary>
        /// <param name="transactionid">Id of the transaction to remove</param>
        /// <returns>completed</returns>
        public bool DeleteTransaction(int transactionid)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Deletion not done. Try again after status is ONLINE.");
                return false;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Deletion not done. Try again after status is ONLINE.");
                    return false;
                }
                foreach (Transaction t in _transactionpool)
                {
                    if (t.Id == transactionid)
                    {
                        _recyclebin.Add(t);
                        _transactionpool.Remove(t);
                        _da.DeleteTransaction(t);
                        _notifications.Add("Transaction: " + t.Id + " is removed to bin.");
                        return true;
                    }
                }
                _notifications.Add("Transaction not found.");
                return false;
            }
        }

        /// <summary>
        /// Restores a transaction out of the recycle bin and adds to file again.
        /// </summary>
        /// <param name="transactionid">Id of the transaction to restore</param>
        /// <returns></returns>
        public bool RestoreTransaction(int transactionid)
        {
            foreach (Transaction t in _recyclebin)
            {
                if (t.Id == transactionid)
                {
                    _transactionpool.Add(t);
                    _recyclebin.Remove(t);
                    _da.AddTransaction(t);
                    _notifications.Add("Transaction: " + t.Id + " is restored.");
                    return true;
                }
            }
            _notifications.Add("No matching transaction found, Restore not possible.");
            return false;
        }

        /// <summary>
        /// Use to pay a transaction. Amount and method can be chosen freely.
        /// </summary>
        /// <param name="transactionid">Id of the transaction to pay</param>
        /// <param name="amount">Amount to pay</param>
        /// <param name="method">Payment method</param>
        public void PayTransaction (int transactionid, double amount, PaymentMethod method)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Payment not done. Try again after status is ONLINE.");
                return;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Payment not done. Try again after status is ONLINE.");
                    return;
                }
                foreach (Transaction t in _transactionpool)
                {
                    if (t.Id == transactionid)
                    {
                        t.Pay(0, method, amount);
                        _da.ChangeTransaction(t);
                        _notifications.Add("Transaction " + t.Id + " paid. Amount: €" + amount + " (" + method.ToString() + ")");
                        break;
                    }
                }
            }
        }

        public void DeletePayment (int transactionid, int paymentid)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Payment not deleted. Try again after status is ONLINE.");
                return;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Payment not deleted. Try again after status is ONLINE.");
                    return;
                }
                foreach (Transaction t in _transactionpool)
                {
                    if (t.Id == transactionid)
                    {
                        t.Refund(paymentid);
                        _da.ChangeTransaction(t);
                        _notifications.Add("Payment " + paymentid + " removed from transaction " + transactionid + ".");
                        break;
                    }
                }
            }

        }
        /// <summary>
        /// Delete a payment from a transaction
        /// </summary>
        /// <param name="transactionid">Id of the transaction to delete from</param>
        /// <param name="payment">Payment to delete</param>
        public void DeletePayment(int transactionid, Payment payment)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Payment not deleted. Try again after status is ONLINE.");
                return;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Payment not deleted. Try again after status is ONLINE.");
                    return;
                }
                if (payment != null)
                {
                    foreach (Transaction t in _transactionpool)
                    {
                        if (t.Id == transactionid)
                        {
                            int id = payment.Id;
                            t.Refund(payment);
                            _da.ChangeTransaction(t);
                            _notifications.Add("Payment " + id + " removed from transaction " + transactionid + ".");
                            return;
                        }
                    }
                    _notifications.Add("No transaction with id: " + transactionid + " found.");
                }
                _notifications.Add("No payment selected in details view");
            }
        }

        /// <summary>
        /// OBSOLETE! All action are registered directly in datasource.
        /// Use to update the stored data when the products or payments of a transaction are changed. Multiple changes can be made before using this to push the updates all at once.
        /// </summary>
        /// <param name="transactionid">Id of the transaction to change</param>
        public void UpdateProducts(int transactionid)
        {
            foreach (Transaction t in _transactionpool)
            {
                if (t.Id == transactionid)
                {
                    _da.ChangeTransaction(t);
                    _notifications.Add("Changes to transaction " + t.Id + " stored in database/XML");
                    break;
                }
            }
        }

        /// <summary>
        /// Closes a transaction when paid fully. The closing user determines the organization that will be credited with the payments.
        /// </summary>
        /// <param name="transactionid">Id of thransaction to close</param>
        /// <param name="cashier">Current loged on user</param>
        public bool CloseTransaction(int transactionid, User cashier, bool forceclose = false)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Checkout not done. Try again after status is ONLINE.");
                return false;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Checkout not done. Try again after status is ONLINE.");
                    return false;
                }
                foreach (Transaction t in _transactionpool)
                {
                    if (t.Id == transactionid)
                    {
                        t.CloseTransaction(cashier, forceclose);
                        if (t.State == TransactionState.Closed)
                        {
                            _da.ChangeTransaction(t);
                            _recyclebin.Add(t);
                            InventoryTransaction it = new InventoryTransaction()
                            {
                                Id = 0,
                                DeliveryNote = "",
                                NamedType = "Issue",
                                Type = 99,
                                Timestamp = t.ClosedTimestamp,
                                Remark = "Transaction " + t.Id.ToString(),
                                UserName = cashier.Username
                            };
                            foreach (Product p in t.Products)
                            {
                                if (p.Id > 0)
                                {
                                    _da.IssueTransactionProduct(p, it);
                                } 
                            }
                            _transactionpool.Remove(t);
                            _notifications.Add("Transaction " + t.Id + " closed.");
                            return true;
                        }
                        _notifications.Add("Unable to close transaction " + t.Id + ". Not paid fully or no products added.");
                        return false;
                    }
                }
                _notifications.Add("No transaction with id: " + transactionid + " found.");
                return false;
            }
        }

        public void AddSingleProduct(int transactionid, Product product)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Product not added. Try again after status is ONLINE.");
                return;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Product not added. Try again after status is ONLINE.");
                    return;
                }
                foreach (Transaction t in _transactionpool)
                {
                    if (t.Id == transactionid)
                    {
                        t.AddProduct(product);
                        _notifications.Add("Added " + product.Name + " x " + product.Quantity + " to transaction " + t.Id);
                        _da.ChangeTransaction(t);
                        return;
                    }
                }
                _notifications.Add("No transaction with id: " + transactionid + " found.");
            }
        }

        public void DeleteSingleProduct(int transactionid, Product product)
        {
            if (_da.Locked)
            {
                _notifications.Add("Synchronizing the database. Product deletion not done. Try again after status is ONLINE.");
                return;
            }
            lock (DAL.DataAdapter._syncLock)
            {
                if (_da.Locked)
                {
                    _notifications.Add("Synchronizing the database. Product deletion not done. Try again after status is ONLINE.");
                    return;
                }
                if (product != null)
                {
                    foreach (Transaction t in _transactionpool)
                    {
                        if (t.Id == transactionid)
                        {
                            int count = product.Quantity;
                            t.SubstractProduct(product);
                            _notifications.Add("Subtracted " + product.Name + " x " + count + " from transaction " + t.Id);
                            _da.ChangeTransaction(t);
                            return;
                        }
                    }
                    _notifications.Add("No transaction with id: " + transactionid + " found.");
                }
                _notifications.Add("No product selected in details view");
            }
        }

        /// <summary>
        /// Performs all the neccesary action for a safe shutdown of the module. Call on application shutdown.
        /// </summary>
        public void Shutdown()
        {

        }
        #endregion
        #region Helpers

        #endregion
    }
}
