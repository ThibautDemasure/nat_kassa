﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa.BLL
{
    public class InventoryModule
    {
        #region Fields
        private List<InventoryProduct> _inventory = new List<InventoryProduct>();
        private List<InventoryProduct> _loadedinventory = new List<InventoryProduct>();
        private List<InventoryTransaction> _inventorytransactions = new List<InventoryTransaction>();
        private Dictionary<string, int> _typeDictionary = new Dictionary<string, int>();
        private DAL.DataAdapter _da = DAL.DataAdapter.Instance;
        private ModuleState _modstate = ModuleState.Normal;
        private List<string> _notifications = new List<string>();

        #endregion

        #region Constructors
        public InventoryModule()
        {
            _inventory = _da.LoadInventory();
            List<string> types = _da.LoadInventoryTransactionTypes();
            _typeDictionary.Add("New",0);
            for (int i = 0; i < types.Count; i++)
            {
                _typeDictionary.Add(types[i], i + 1);
            }
            _inventorytransactions = _da.LoadInventoryTransactions();
            
            foreach (InventoryTransaction item in _inventorytransactions)
            {
                item.Type = _typeDictionary[item.NamedType];
            }
            _notifications.Add("Inventory loaded from datasource.");
        }
        #endregion

        #region Properties

        public ModuleState ModState { get => _modstate; set => _modstate = value; }
        public List<string> Notifications { get => _notifications; }
        public int TotalQuantity
        {
            get
            {
                int total = 0;
                foreach (InventoryProduct item in _inventory)
                {
                    total += item.Quantity;
                }
                return total;
            }
        }
        public List<InventoryProduct> Inventory { get => _inventory; }
        public List<InventoryProduct> LoadInventory
        {
            get
            {
                _loadedinventory = _da.LoadInventory();
                return _loadedinventory;
            }
        }

        public List<InventoryProduct> SelectTransaction(int Id)
        {
            return _da.LoadInventoryTransaction(Id);
        }

        public List<InventoryTransaction> Inventorytransactions { get => _inventorytransactions; }
        public List<InventoryTransaction> FilteredTransactions(string filter)
        {
            List<InventoryTransaction> filtered = new List<InventoryTransaction>();
            foreach (InventoryTransaction item in _inventorytransactions)
            {
                if (item.NamedType == filter)
                {
                    filtered.Add(item);
                }
            }
            return filtered;
        }

        public Dictionary<string, int> TypeDictionary { get => _typeDictionary; }

        #endregion

        #region Methods

       
        /// <summary>
        /// Update quantity of all products in the inventory by inputting a complete list of inventory products
        /// </summary>
        /// <param name="products">list of inventory products where the OrderQ has been set</param>
        public void ReceiveOrderProduct(InventoryTransaction transaction, InventoryProduct product)
        {
            if (product.Used && transaction.Type == 2 && _da.ReceiveOrderProduct(product,transaction))
            {
                foreach (InventoryProduct item in _inventory)
                {
                    if (item.Id == product.Id)
                    {
                        item.Add(product.Quantity);
                        item.Used = item.IsLow;
                        break;
                    }
                }
                _notifications.Add(product.Name + " order received and added to inventory");
            }
        }
        public void IssueTransactionProduct(InventoryTransaction transaction)
        {
            
            for (int i = 0; i < _loadedinventory.Count; i++)
            {
                if (_loadedinventory[i].Used && _da.IssueTransactionProduct(_loadedinventory[i],transaction))
                {
                    _notifications.Add(_loadedinventory[i].Name + " order issued");
                }
            }
 
        }
        public void PurchaseOrderProduct(InventoryTransaction transaction, InventoryProduct product)
        {
                if (product.Used && _da.PurchaseOrderProduct(product, transaction))
                {
                    _notifications.Add(product.Name + " purchase order created");
                }
        }
        public void UpdateCurrentQuantity(InventoryTransaction transaction)
        {

            for (int i = 0; i < _loadedinventory.Count; i++)
            {
                if (_loadedinventory[i].Used && _da.UpdateCurrentQuantity(_loadedinventory[i],transaction))
                {
                    _loadedinventory[i].Used = _loadedinventory[i].IsLow;
                    _notifications.Add(_loadedinventory[i].Name + " x " + _loadedinventory[i].Quantity.ToString() + " current quantity counted and inventory changed");
                }
            }

        }

        public void UpdateDefaultOrderParameters()
        {
            for (int i = 0; i < _loadedinventory.Count; i++)
            {
                if (_loadedinventory[i].Used && _da.UpdateOrderParameters(_loadedinventory[i]))
                {
                    _loadedinventory[i].Used = _loadedinventory[i].IsLow;
                    _notifications.Add(_loadedinventory[i].Name + " updated order parameters");
                }
                
            }  
            
        }
        
        public void Shutdown()
        {

        }


        #endregion
    }
}
