﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NAT_Kassa
{
    public class CashDrawer
    {
        //fields
        private System.Timers.Timer _bgworker = new System.Timers.Timer(2000);
        private string _sPath = "";
        private int _openedvalue;
        private string _errormessage = "";
        private int _exitcode;

        //constructors
        public CashDrawer()
        {
            _bgworker.Elapsed += _bgworker_Elapsed;
            _sPath = Properties.Settings.Default.sCashDrawerSDK;
            _openedvalue = Properties.Settings.Default.iCashDrawerClosedState;
            _exitcode = 99;
        }

        //properties

        public int Exitcode { get => _exitcode; }
        public string Errormessage { get => _errormessage; }

        //events
        public event EventHandler Closed;
        protected virtual void OnClosed(EventArgs e)
        {
            EventHandler handler = Closed;
            handler?.Invoke(this, e);
        }
        //register tick
        public event EventHandler Tick;
        protected virtual void OnTick(EventArgs e)
        {
            EventHandler handler = Tick;
            handler?.Invoke(this, e);
        }

        //methods
        public void Open()
        {
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.StartInfo.FileName = @"C:\Program Files (x86)\FEC\CashDrawerSDK\fec_cashdrawer_module.exe"; //FEC CashDrawer module program filename
                myProcess.StartInfo.Arguments = @"openCashDrawer"; //call FEC CashDrawer module program parameter, 
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.Start();
                myProcess.WaitForExit();
                _exitcode = myProcess.ExitCode;
                myProcess.Close();
                _bgworker.Start();
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void SetOpenedValue (int val)
        {
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.StartInfo.FileName = @"C:\Program Files (x86)\FEC\CashDrawerSDK\fec_cashdrawer_module.exe"; //FEC CashDrawer module program filename
                myProcess.StartInfo.Arguments = "setCashDrawerOpenStatusValue " + val.ToString(); //call FEC CashDrawer module program parameter, 
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.Start();
                myProcess.WaitForExit();
                _exitcode = myProcess.ExitCode;
                myProcess.Close();
                _openedvalue = val;
                Properties.Settings.Default.iCashDrawerClosedState = val;
                Properties.Settings.Default.Save();
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void CheckClosed()
        {
            try
            {
                Process myProcess = new Process();
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcess.StartInfo.FileName = @"C:\Program Files (x86)\FEC\CashDrawerSDK\fec_cashdrawer_module.exe"; //FEC CashDrawer module program filename
                myProcess.StartInfo.Arguments = @"detectCashDrawerClose"; //call FEC CashDrawer module program parameter, 
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.RedirectStandardInput = true;
                myProcess.Start();
                myProcess.WaitForExit();
                int result = myProcess.ExitCode;
                _exitcode = result;
                myProcess.Close();

                if (_exitcode == _openedvalue)
                {
                    _bgworker.Stop();
                    _bgworker.Interval = 2000;
                    OnClosed(EventArgs.Empty);
                }
                else
                {
                    _bgworker.Interval = 500;
                    OnTick(EventArgs.Empty); 
                }
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        private void _bgworker_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CheckClosed();
        }
      

        public void Shutdown()
        {
            _bgworker.Stop();
            _bgworker.Close();
        }

    }
    public class DrawerAction
    {
        public DateTime Timestamp { get; set; }
        public string Message { get; set; }
        public string Action { get; set; }
        public string User { get; set; }
        public string Valid { get; set; }
        public double OnHandAmount { get; set; }
        public double CountedAmount { get; set; }
        public double WithdrawAmount { get; set; }
        public double DepositAmount { get; set; }
        public double NewAmount { get; set; }
        public override string ToString()
        {
            return Action + ": " + User + ". Vorig bedrag " + OnHandAmount.ToString("F2") + " ";
        }
    }
}

